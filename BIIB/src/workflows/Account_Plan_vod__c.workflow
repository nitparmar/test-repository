<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approvedstatus</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pendingstatus</fullName>
        <field>Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Pending status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejectedstatus</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
