<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BIIB_Send_letter_of_consent_email_to_Patient</fullName>
        <description>BIIB Send letter of consent email to Patient</description>
        <protected>false</protected>
        <recipients>
            <field>BIIB_Patient_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EventReminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>BIIB_Four_Dosage</fullName>
        <description>Update dosage field to 4</description>
        <field>BIIB_Dosage__c</field>
        <formula>4</formula>
        <name>BIIB_Four_Dosage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Result_Currently_Ineligible</fullName>
        <description>Update result field to Currently Ineligible</description>
        <field>BIIB_Result__c</field>
        <literalValue>Currently Ineligible</literalValue>
        <name>BIIB_Result_Currently Ineligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Result_Eligible</fullName>
        <description>Update result to eligible</description>
        <field>BIIB_Result__c</field>
        <literalValue>Eligible</literalValue>
        <name>BIIB_Result_Eligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Result_Permanently_neligible</fullName>
        <description>Update the result with permanently ineligible</description>
        <field>BIIB_Result__c</field>
        <literalValue>Permanently Ineligible</literalValue>
        <name>BIIB_Result_Permanently_neligible</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Two_Dosage</fullName>
        <description>Update dosage field with 2</description>
        <field>BIIB_Dosage__c</field>
        <formula>2</formula>
        <name>BIIB_Two_Dosage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Closed_Date</fullName>
        <field>BIIB_Closed_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>BIIB Update Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Closed_Date_Time</fullName>
        <field>BIIB_Closed_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Update Closed Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Patient_Email</fullName>
        <description>Update Patient email on Work Object</description>
        <field>BIIB_Patient_Email__c</field>
        <formula>BIIB_Case__r.Contact.Email</formula>
        <name>BIIB_Update_Patient_Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Result_Flag</fullName>
        <field>BIIB_Result_Flag__c</field>
        <literalValue>1</literalValue>
        <name>BIIB_Update_Result_Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Route_Date_Field</fullName>
        <field>BIIB_Route_Date__c</field>
        <formula>NOW()</formula>
        <name>BIIB_Update_Route_Date_Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Routed_First_date</fullName>
        <field>BIIB_Routed_First_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>BIIB Update Routed First Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Shipment_Work_Status</fullName>
        <description>US-0658: This is used to update the Work Status Field to &quot;Done&quot; when the rule criteria are met.</description>
        <field>BIIB_Status__c</field>
        <literalValue>Done</literalValue>
        <name>BIIB Update Shipment Work Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Work_Status</fullName>
        <description>User Story US-0112. Updates work status to &apos;Completed&apos; when recordtype of work is Shipment and Shipment status is Received</description>
        <field>BIIB_Status__c</field>
        <literalValue>Done</literalValue>
        <name>Update Work Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Work_Status_Done</fullName>
        <field>BIIB_Status__c</field>
        <literalValue>Done</literalValue>
        <name>BIIB_Update_Work_Status_Done</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Zero_Dosage</fullName>
        <description>Update Dosage field with Zero</description>
        <field>BIIB_Dosage__c</field>
        <formula>0</formula>
        <name>BIIB_Zero_Dosage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>BIIB Update Closed Date</fullName>
        <actions>
            <name>BIIB_Update_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Status__c</field>
            <operation>equals</operation>
            <value>Done</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Closed_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>US-0080 : This rule will populate the Closed Date Time  field using a Workflow Field update when the status is Completed or Closed-Failed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB Update Routed First Date</fullName>
        <actions>
            <name>BIIB_Update_Routed_First_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>US-0080: This rule checks if the Assign To Field is being changed and the original value is Null. This rule is run when the user changes the AssignTo on WorkItem from a Generic User to a Proper user for the first time.</description>
        <formula>ISCHANGED(OwnerId)&amp;&amp; NOT(ISBLANK(PRIORVALUE (OwnerId))) &amp;&amp; ISBLANK( BIIB_Routed_First_Date_Time__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB Update Shipment Work Status</fullName>
        <actions>
            <name>BIIB_Update_Shipment_Work_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Shipment_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Shipment_Status__c</field>
            <operation>equals</operation>
            <value>Shipment Confirmed</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Shipment</value>
        </criteriaItems>
        <description>US-0658: This rule is used to update the Status to &quot;Done&quot; for the Work Record of type Shipment, when Shipment Date is not null and Shipment Status = &quot;Shipment Confirmed&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Four_Dosage</fullName>
        <actions>
            <name>BIIB_Four_Dosage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Federal_Payer__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Open_in_Start_Assistance__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Consent_on_file__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Barrier_Present__c</field>
            <operation>equals</operation>
            <value>Alt Funding/Insurance Counseling &gt;30 Days,Step Edit Required (Naïve Pt),PA Denied (MD Appealing)</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Titrating_first_month_PEN__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Four Dosage</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Result_Currently Ineligible</fullName>
        <actions>
            <name>BIIB_Result_Currently_Ineligible</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Open_in_Start_Assistance__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Consent_on_file__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Barrier_Present__c</field>
            <operation>equals</operation>
            <value>Alt Funding/Insurance Counseling &gt;30 Days,None of the above</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Titrating_first_month_PEN__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>Update result to currently ineligible</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Result_Eligible</fullName>
        <actions>
            <name>BIIB_Result_Eligible</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Dosage__c</field>
            <operation>equals</operation>
            <value>2,4</value>
        </criteriaItems>
        <description>Update result to eligible</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Result_Permanently_Ineligible</fullName>
        <actions>
            <name>BIIB_Result_Permanently_neligible</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Federal_Payer__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Update result to permanently ineligible</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Send_Consent_To_Patient_Email</fullName>
        <actions>
            <name>BIIB_Send_letter_of_consent_email_to_Patient</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Patient_Email__c</field>
            <operation>notEqual</operation>
            <value>NULL</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Two_Dosage</fullName>
        <actions>
            <name>BIIB_Two_Dosage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Federal_Payer__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Open_in_Start_Assistance__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Consent_on_file__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Barrier_Present__c</field>
            <operation>equals</operation>
            <value>Step Edit Required (Naïve Pt),PA Denied (MD Appealing)</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Titrating_first_month_PEN__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>Zero Dosage</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Update_Patient_Email</fullName>
        <actions>
            <name>BIIB_Update_Patient_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update patient email field with corresponding email on Work Object</description>
        <formula>AND(
ISPICKVAL(BIIB_Work_Type__c, &quot;Get Consent&quot;),
ISPICKVAL( BIIB_Work_Sub_Type__c, &quot;eMail&quot;),    ISPICKVAL(BIIB_Case__r.Account.BIIB_Consent__c, &quot;Yes&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Update_Result_Flag</fullName>
        <actions>
            <name>BIIB_Update_Result_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Result__c</field>
            <operation>equals</operation>
            <value>Wrong Number</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Update_Route_Date</fullName>
        <actions>
            <name>BIIB_Update_Route_Date_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>US-0295: This rule checks if the Assign To Field is being changed and the original value is not Null. This rule is run when the user changes the AssignTo on WorkItem from a Generic User to a Proper user</description>
        <formula>ISCHANGED(BIIB_Assigned_to__c) &amp;&amp; NOT(ISNULL(PRIORVALUE (BIIB_Assigned_to__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Update_Shipment_Work_Status</fullName>
        <actions>
            <name>BIIB_Update_Work_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Work__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Shipment</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Shipment_Status__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <description>User Story US-0112. Workflow to update the Work Status field to &apos;Completed&apos; when the recordtype is BIIB_Shipment and Shipment Status  is Received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Update_Work_Status_Done</fullName>
        <actions>
            <name>BIIB_Update_Work_Status_Done</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Result__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB_Zero_Dosage</fullName>
        <actions>
            <name>BIIB_Zero_Dosage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3 OR 4</booleanFilter>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Federal_Payer__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Open_in_Start_Assistance__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Consent_on_file__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <criteriaItems>
            <field>BIIB_Work__c.BIIB_Barrier_Present__c</field>
            <operation>equals</operation>
            <value>None of the above</value>
        </criteriaItems>
        <description>Zero Dosage</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
