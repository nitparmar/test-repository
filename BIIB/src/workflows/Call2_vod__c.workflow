<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BIIB_Update_Call_Duration</fullName>
        <description>Field update to update call duration whenever call datetime or call end time changes</description>
        <field>Duration_vod__c</field>
        <formula>(BIIB_End_DateTime__c - Call_Datetime_vod__c)*24*60.0</formula>
        <name>Update Call Duration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BIIB_Update_Call_End_DateTime</fullName>
        <description>Field update to update Call End DateTime field whenever call duration changes</description>
        <field>BIIB_End_DateTime__c</field>
        <formula>Call_Datetime_vod__c+(Duration_vod__c/(60*24))</formula>
        <name>Update Call End DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BIIB Update Call Duration</fullName>
        <actions>
            <name>BIIB_Update_Call_Duration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>BIIB Update Call Duration workflow rule updates the &apos;Duration&apos; on call object whenever call datetime or end time changes</description>
        <formula>OR(AND(ISCHANGED(BIIB_End_DateTime__c),IF( ISNULL(Duration_vod__c),true, if(Duration_vod__c != (BIIB_End_DateTime__c - Call_Datetime_vod__c)/(1000*60.0), true, false))),AND(ISCHANGED(Call_Datetime_vod__c),IF( ISNULL(Duration_vod__c),true, if(Duration_vod__c != (BIIB_End_DateTime__c - Call_Datetime_vod__c)/(1000*60.0), true, false))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BIIB Update Call End DateTime</fullName>
        <actions>
            <name>BIIB_Update_Call_End_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>BIIB Update Call End DateTime workflow rule updates the &apos;End DateTime&apos; on call object whenever call duration changes</description>
        <formula>AND(ISCHANGED(Duration_vod__c),IF( ISNULL(BIIB_End_DateTime__c),true, if(Duration_vod__c != (BIIB_End_DateTime__c - Call_Datetime_vod__c)/(1000*60.0), true, false)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
