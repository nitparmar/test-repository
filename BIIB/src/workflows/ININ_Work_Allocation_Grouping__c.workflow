<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BIIB_Field_Update_Copy_Record</fullName>
        <field>BIIB_Record_Id__c</field>
        <formula>Id</formula>
        <name>BIIB_Field_Update_Copy_Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BIIB_Copy_Record_ID_for_Service_Console_PopUp</fullName>
        <actions>
            <name>BIIB_Field_Update_Copy_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ININ_Work_Allocation_Grouping__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
