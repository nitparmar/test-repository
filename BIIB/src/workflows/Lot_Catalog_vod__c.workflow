<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LotCatalogExternalId_vod</fullName>
        <field>Lot_Catalog_External_Id_vod__c</field>
        <formula>Name</formula>
        <name>Lot Catalog External Id_vod</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lot Catalog External Id_vod</fullName>
        <actions>
            <name>LotCatalogExternalId_vod</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to update the External Id on Lot Catalog for Data Loading processes.</description>
        <formula>IF(ISNULL(Name), false, true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
