<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BIIB_Update_Primary_flag_to_false</fullName>
        <description>User story US-0128: This field updates Primary flag to false for end dated Patient Plans</description>
        <field>BIIB_Primary__c</field>
        <literalValue>0</literalValue>
        <name>Update Primary flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BIIB_Update_Primary_flag_for_End_Dated_Plan</fullName>
        <active>true</active>
        <criteriaItems>
            <field>BIIB_Patient_Plan__c.BIIB_Primary__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>User Story US-0128: This workflow updates the Primary flag to false for End Dated Patient Plans</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BIIB_Update_Primary_flag_to_false</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>BIIB_Patient_Plan__c.BIIB_End_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
