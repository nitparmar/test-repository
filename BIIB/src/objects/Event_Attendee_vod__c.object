<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This is a child object of Medical Event. One row represents a entity (Account, Contact, User) who has been invited to attended to the Medical Event. The Attendee_vod field is updated by a trigger to insure it always matches the name field of the account, contact, or user on create/update.</description>
    <enableActivities>false</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>Account_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Account acting as an Attendee.</description>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Event Attendees(Account)</relationshipLabel>
        <relationshipName>Event_Attendee_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Attendee_Name_vod__c</fullName>
        <description>This field is a formula that displays the Attendee Name from the Attendee field and acts as a Link to the Event Attendee record.</description>
        <externalId>false</externalId>
        <formula>HYPERLINK(&quot;/&quot; &amp;  Id,  IF(LEN(Attendee_vod__c) &lt; 1,&quot;Attendee Link&quot;,Attendee_vod__c)  ,&quot;_parent&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Attendee Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Attendee_Type_vod__c</fullName>
        <description>This field defines what type of Attendee has been selected for the Event.  This formula can be modified to meet the naming conventions of a Customer&apos;s organization.</description>
        <externalId>false</externalId>
        <formula>IF(LEN( Account_vod__c) &gt; 0, &quot;Account&quot;,  IF(LEN( Contact_vod__c) &gt; 0, &quot;Contact&quot;,  &quot;User&quot;) )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Attendee Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Attendee_vod__c</fullName>
        <description>The text of the Person that is selected either in the Account, Contact, or User Lookup.  This is populated by a Trigger on Save.</description>
        <externalId>false</externalId>
        <label>Attendee</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Contact attending the event.</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Event Attendees(Contact)</relationshipLabel>
        <relationshipName>Event_Attendee_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Expense_Attendee_Type_vod__c</fullName>
        <description>Indicates the type of attendee. The values of this picklist should match the values available in the third-party expense system, such as Concur.</description>
        <externalId>false</externalId>
        <label>Expense Attendee Type</label>
        <picklist>
            <picklistValues>
                <fullName>BUSGUEST</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Expense_Post_Status_vod__c</fullName>
        <description>Holds a Success or Fail status based on the attempt to post this attendee into Concur. If the attendee was not yet posted, the status will be null.</description>
        <externalId>false</externalId>
        <label>Expense Post Status</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Incurred_Expense_vod__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Attendees with the Incurred Expense checkbox checked will be posted as Attendees to Concur.</description>
        <externalId>false</externalId>
        <label>Incurred Expense</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Medical_Event_vod__c</fullName>
        <description>The Medical Event that the Account, Contact, or User is attending.</description>
        <externalId>false</externalId>
        <label>Medical Event</label>
        <referenceTo>Medical_Event_vod__c</referenceTo>
        <relationshipLabel>Event Attendees</relationshipLabel>
        <relationshipName>Event_Attendee_vod</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Signature_Datetime_vod__c</fullName>
        <description>Stores the Datetime of when the Signature was captured</description>
        <externalId>false</externalId>
        <label>Signature Datetime</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Signature_vod__c</fullName>
        <description>Used to store the Signature</description>
        <externalId>false</externalId>
        <label>Signature</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Start_Date_vod__c</fullName>
        <description>Start Date for the Medical Event</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Status_vod__c</fullName>
        <description>Status of the person invited to the medical event.</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Invited</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Accepted</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Attended</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rejected</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Signed_vod</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cleared_Signature_vod</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>User_vod__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The User attending the event.</description>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Event_Attendee_vod</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Event Attendee</label>
    <nameField>
        <displayFormat>A{000000000}</displayFormat>
        <label>Attendee Id</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Event Attendees</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Attendee_Name_vod__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Attendee_Type_vod__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status_vod__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LAST_UPDATE</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Attendee_Selection_vod</fullName>
        <active>true</active>
        <description>This rule validates that either an Account, Contact, or User was selected as an Attendee.</description>
        <errorConditionFormula>AND(LEN( Account_vod__c ) &lt; 1,LEN( Contact_vod__c ) &lt; 1,LEN( User_vod__c ) &lt; 1)</errorConditionFormula>
        <errorMessage>An Account, Contact, or User must be selected to save a record.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Multiple_Attendees_vod</fullName>
        <active>true</active>
        <description>This validation Rule will confirm that only one Account or Contact or User is selected as the Attendee.</description>
        <errorConditionFormula>IF( LEN( Account_vod__c ) &gt; 0 , IF( OR(LEN( Contact_vod__c ) &gt; 0 ,LEN( User_vod__c ) &gt; 0),true,false) , IF( LEN( Contact_vod__c ) &gt; 0 , IF( OR(LEN( Account_vod__c ) &gt; 0 ,LEN( User_vod__c ) &gt; 0),true,false) , IF( LEN( User_vod__c ) &gt; 0 , IF( OR(LEN( Account_vod__c ) &gt; 0 ,LEN( Contact_vod__c ) &gt; 0),true,false) , false)))</errorConditionFormula>
        <errorMessage>Please select only one Account, Contact, or User.</errorMessage>
    </validationRules>
</CustomObject>
