trigger Sample_Receipt_vod on Sample_Receipt_vod__c (after update) {
    RecordType  recType  = [ Select Id from RecordType where SobjectType = 'Sample_Transaction_vod__c'  and  Name ='Receipt_vod' ];
    List <Sample_Transaction_vod__c> transList = new    List <Sample_Transaction_vod__c> ();
    List <Sample_Transaction_vod__c> insTransList = new     List <Sample_Transaction_vod__c> ();
    List <Sample_Receipt_vod__c> recpList = new     List <Sample_Receipt_vod__c> ();
    List <Sample_Lot_vod__c> updSampLotList = new   List <Sample_Lot_vod__c> ();
    Set <Id> updSampLotSet = new   Set<Id> ();
    
    VOD_SAMPLE_RECEIPTS.setReceipt (true);
    
    for (Integer i = 0; i < Trigger.new.size (); i++) {
        Sample_Receipt_vod__c sr = Trigger.new[i];
        Sample_Transaction_vod__c updtran = 
               new Sample_Transaction_vod__c (ID = sr.Ref_Transaction_Id_vod__c,
                                              Receipt_Comments_vod__c = sr.Receipt_Comments_vod__c,
                                              Received_vod__c = true,
                                              Confirmed_Quantity_vod__c = sr.Confirmed_Quantity_vod__c);
                                              
        Sample_Transaction_vod__c insTran = new Sample_Transaction_vod__c ( 
                                                        RecordTypeId = recType.Id,
                                                        Comments_vod__c =   sr.Comments_vod__c, 
                                                        Confirmed_Quantity_vod__c   =   sr.Confirmed_Quantity_vod__c, 
                                                        Lot_Name_vod__c =   sr.Lot_Name_vod__c, 
                                                        Lot_vod__c  =   sr.Lot_vod__c, 
                                                        Quantity_vod__c =   sr.Quantity_vod__c, 
                                                        Receipt_Comments_vod__c =   sr.Receipt_Comments_vod__c, 
                                                        Sample_vod__c   =   sr.Sample_vod__c, 
                                                        Shipment_Id_vod__c  =   sr.Shipment_Id_vod__c, 
                                                        Status_vod__c = 'Submitted_vod',
                                                        Submitted_Date_vod__c = System.today(),
                                                        Transferred_Date_vod__c =   sr.Transferred_Date_vod__c, 
                                                        Transferred_From_Name_vod__c    =   sr.Transferred_From_Name_vod__c, 
                                                        Transferred_From_vod__c =   sr.Transferred_From_vod__c, 
                                                        U_M_vod__c  =   sr.U_M_vod__c );
                                                        
        if (insTran.Shipment_Id_vod__c != null)
            insTran.Group_Transaction_Id_vod__c =  insTran.Shipment_Id_vod__c + '(RT)';                                                                   
        
      
        // add the sample lot Ids in the set
        if(!updSampLotSet.contains(sr.Lot_vod__c)) {
            updSampLotSet.add(sr.Lot_vod__c);
            Sample_Lot_vod__c updSampLot = new Sample_Lot_vod__c ( Id = sr.Lot_vod__c,
                                                                    Active_vod__c = true);                                                                      
            updSampLotList.add(updSampLot);
        }                                                   
                                                        
    
        if (sr.Ref_Transaction_Id_vod__c != null) {
            transList.add (updtran);
            insTran.Ref_Transaction_Id_vod__c   =   sr.Ref_Transaction_Id_vod__c;   
        }
        insTransList.add (insTran);
        
        recpList.add (sr);
    
    }
    
    if (updSampLotList.size () > 0) {
        update updSampLotList;        
    }
    
    if (transList.size () > 0) {
        update transList;
    }
     
    if (insTransList.size() > 0) {
        insert insTransList;
    }    
}