/**
*@author Rancy Fernandes
*@date 08/04/2014
@description: Trigger on Patient Plan
       1. User story US-0128. To allow only one primary Patient Plan for a Patient
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rancy      08/04/2014  OriginalVersion
*/
trigger BIIB_Patient_Plan_trigger on BIIB_Patient_Plan__c (before Insert, before update) {
    
    /* Before Insert */
    if(Trigger.isInsert && Trigger.isBefore){
        system.debug('new map' + Trigger.new);
        BIIB_Patient_Plan_Handler_Class.OnBeforeInsert(Trigger.new);
    }
    
    /* Before Update */
    else if(Trigger.isUpdate && Trigger.isBefore){
        BIIB_Patient_Plan_Handler_Class.OnBeforeUpdate (Trigger.OldMap , Trigger.newMap);
    }
}