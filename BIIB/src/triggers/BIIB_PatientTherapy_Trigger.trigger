trigger BIIB_PatientTherapy_Trigger on BIIB_Patient_Therapy__c (after update) {
    
    Set<Id> set_PatientTherapy = new Set<Id>();
    List<BIIB_Work__c> lstWorkUpdate = new List<BIIB_Work__c>();
    List<Case> lstCase= new List<Case>();
    
    for(BIIB_Patient_Therapy__c objTherapy:Trigger.new){
        if(objTherapy.BIIB_Status__c=='Never Start'){
              set_PatientTherapy.add(objTherapy.Id);  
        }        
    }
    
    if(!(set_PatientTherapy.isempty())){
        List<Case> lst_Case =[Select Id,Status,(Select Id,BIIB_Status__c From Work_Items__r Where BIIB_Status__c!='Done') From Case Where BIIB_Patient_Therapy__c In : set_PatientTherapy and Status!='Closed'];
        
        if(!(lst_Case.isempty())){
               for(Case objCase : lst_Case){
                   objCase.Status='Closed';
                   lstCase.add(objCase);
                   
                   for(BIIB_Work__c objWork : objCase.Work_Items__r){
                       objWork.BIIB_Status__c='Done';
                       objWork.BIIB_Result__c='Never Start';
                       objWork.BIIB_Reason_Code__c='N/A';
                       lstWorkUpdate.add(objWork);
                   }
               }
        }
        
        if(!(lstWorkUpdate.isempty())){
            update lstWorkUpdate;
        }
        if(!(lstCase.isempty())){
            update lstCase;
        }
    }
}