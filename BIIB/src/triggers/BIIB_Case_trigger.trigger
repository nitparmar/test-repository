/*  
*@author Rancy Fernandes
*@date 08/04/2014
@description
       1. User story: US-0115. To allow multiple Open Cases for recordtypes which are specified in the custom setting BIIB_Case_RecordTypes__c where Multiple is true before Insert.
       2. User story: US-0126. To change the record type from Adverse Event to Closed AE when the Adverse Event record type case is closed before insert and before update.

------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rancy      08/04/2014  OriginalVersion

*/
trigger BIIB_Case_trigger on Case (before insert, after insert, before update) {
   
    //Before Insert 
    if(Trigger.isInsert && Trigger.isBefore){
        system.debug('new map' + Trigger.new);
        BIIB_Case_Handler_Class.OnBeforeInsert(Trigger.new);
    }
    //Before Update 
    if(Trigger.isUpdate && Trigger.isBefore){
        system.debug('new map' + Trigger.new);
        BIIB_Case_Handler_Class.OnBeforeUpdate(Trigger.new);
    }
}