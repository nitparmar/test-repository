trigger VEEVA_SENT_EMAIL_BEFORE_INSERT on Sent_Email_vod__c (before insert) {
    VEEVA_ApprovedDocumentAccessChecker checker = new VEEVA_ApprovedDocumentAccessChecker();
    Set<Id> docIds = new Set<Id>();
    
    for (Sent_Email_vod__c se : trigger.new){
        docIds.add(se.Approved_Email_Template_vod__c);      
    }
    
    Map<Id, Boolean> accessMap = checker.userHasAccessToApprovedDocuments(docIds);
    
    for (Sent_Email_vod__c se : trigger.new){
        Id templateID = se.Approved_Email_Template_vod__c; 
        if(accessMap.get(templateID) == false){
            se.Failure_Msg_vod__c= 'Failed to send because template with the ID '+se.Approved_Email_Template_vod__c + ' could not be accessed';
            se.Approved_Email_Template_vod__c = null;
            se.Status_vod__c = 'Failed_vod';
        }     
    }  
   
}