trigger VOD_CALL2_SAMPLE_BEFORE_TRIGGER on Call2_Sample_vod__c (before delete, before insert, before update) {
        
     
    //do not call this in case of Delete! 
    if(trigger.isDelete == false)
    	VEEVA_CSL.Main(trigger.new);
   

    VOD_ERROR_MSG_BUNDLE bnd = new VOD_ERROR_MSG_BUNDLE ();
    String NO_DEL_SUB = bnd.getErrorMsg('NO_DEL_SUB');
    String NO_UPD_SUB = bnd.getErrorMsg('NO_UPD_SUB');
        
     List <String> parentCall = new List <String> ();

    Set<String> allIds =  new Set<String> ();
    Map<String, List<Product_Group_vod__c>> productGroupMapping = new Map<String, List<Product_Group_vod__c>>();
    Map<String, List<String>> acctSamples = new Map<String, List<String>>();
    Call2_Sample_vod__c [] cRow = null;
            
    if (Trigger.isDelete) 
        cRow = Trigger.old;
    else
        cRow = Trigger.new;
            
            
    for (Integer i = 0; i < cRow.size (); i++ ) {
        parentCall.add (cRow[i].Call2_vod__c);          
    }
            
    Map <Id, Call2_vod__c> calls =  VOD_CALL2_CHILD_COMMON.getCallMap (parentCall);
    for (Integer k = 0; k < cRow.size(); k++) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            if (cRow[k].Attendee_Type_vod__c != null && cRow[k].Attendee_Type_vod__c.length() > 0 &&  
                cRow[k].Entity_Reference_Id_vod__c != null && cRow[k].Entity_Reference_Id_vod__c.length() > 0) {
                if ('Person_Account_vod' == cRow[k].Attendee_Type_vod__c  || 'Group_Account_vod' == cRow[k].Attendee_Type_vod__c ) {
                    cRow[k].Account_vod__c = cRow[k].Entity_Reference_Id_vod__c;    
                    cRow[k].Entity_Reference_Id_vod__c = null;                  
                }
            }
    
        }
        if ((Trigger.isInsert || Trigger.isUpdate) && (cRow[k].Override_Lock_vod__c == true)) {
            cRow[k].Override_Lock_vod__c = false;
            continue;
        }
        if (VOD_CALL2_CHILD_COMMON.isLocked (cRow[k].Call2_vod__c, calls)) {
            if (Trigger.isDelete) {
                cRow[k].Call2_vod__c.addError(NO_DEL_SUB, false);
            }
            else {
                if (VEEVA_SAMPLE_CANCEL.isSampleCancel == false)
                    cRow[k].Call2_vod__c.addError(NO_UPD_SUB, false);
            }
        }
        Call2_vod__c myCall = calls.get(cRow[k].Call2_vod__c);
        
        String ownerId = null;
        if (myCall != null)
            ownerId = myCall.OwnerId;
       
      
      String AccountProd = VOD_CALL2_CHILD_COMMON.getLimitId (cRow[k].Account_vod__c,cRow[k].Product_vod__c, '');
      String AccountProdUser = VOD_CALL2_CHILD_COMMON.getLimitId (cRow[k].Account_vod__c,cRow[k].Product_vod__c, ownerId);
      String UserProd = VOD_CALL2_CHILD_COMMON.getLimitId ('',cRow[k].Product_vod__c, ownerId);
      
      allIds.add(AccountProd);
      allIds.add(AccountProdUser);
      allIds.add(UserProd);   
   
      // add for product group mapping
      List<String> samples = acctSamples.get(cRow[k].Account_vod__c);
      if (samples == null) {
          samples = new List<String>();
          acctSamples.put(cRow[k].Account_vod__c, samples);
      }
      samples.add(cRow[k].Product_vod__c);
      
      if (!productGroupMapping.containsKey(cRow[k].Product_vod__c)) {
          productGroupMapping.put(cRow[k].Product_vod__c, new List<Product_Group_vod__c>());
      }
    }
    
    for (Product_Group_vod__c prodGrpObj :
            [Select Id,Start_Date_vod__c,End_Date_vod__c,Product_vod__c,Product_Catalog_vod__c
                From Product_Group_vod__c
                Where Product_vod__c In :productGroupMapping.keySet()]) {
        List<Product_Group_vod__c> pgs = productGroupMapping.get(prodGrpObj.Product_vod__c);
        pgs.add(prodGrpObj);
    }
    
    // generate group ids for sample product groups
    for (String acctId : acctSamples.keySet()) {
        List<String> sampleIds = acctSamples.get(acctId);
        for (String sampleId : sampleIds) {
            List<Product_Group_vod__c> prodGroups = productGroupMapping.get(sampleId);
            for (Product_Group_vod__c prodGroup : prodGroups) {
                allIds.add(VOD_CALL2_CHILD_COMMON.getLimitId(acctId, prodGroup.Product_Catalog_vod__c, ''));
            }
        }
    }    
    Map<String, List<Sample_Limit_vod__c>> mapSLbyExtId = new   Map<String, List<Sample_Limit_vod__c>>();
    Map<String,Sample_Limit_vod__c> updateMap = new  Map<String,Sample_Limit_vod__c> ();
    List<Sample_Limit_Transaction_vod__c> transactions = new  List<Sample_Limit_Transaction_vod__c> ();
    for (Sample_Limit_vod__c limitObj : 
             [Select  Name,External_Id_vod__c,Account_vod__c, Account_vod__r.Formatted_Name_vod__c, 
                      Disbursed_Quantity_vod__c, End_Date_vod__c, Enforce_Limit_vod__c, Group_Id_vod__c, 
                      Id, Limit_Per_Call_vod__c, Limit_Quantity_vod__c, Product_vod__c, Product_vod__r.Name, 
                      Remaining_Quantity_vod__c, Sample_Limit_Type_vod__c, Start_Date_vod__c, User_vod__c, 
                      User_vod__r.Username 

                FROM Sample_Limit_vod__c
                Where Group_Id_vod__c in :allIds]) {
                    
        String acctId = limitObj.Account_vod__c;
        if (acctId != null && !limitObj.Group_Id_vod__c.startsWith(acctId.substring(0,15))) {
            continue;
        }
                    
        List<Sample_Limit_vod__c> thisLimit =  mapSLbyExtId.get(limitObj.Group_Id_vod__c);
        if (thisLimit == null) {
            thisLimit =  new List<Sample_Limit_vod__c> ();
        }  
        thisLimit.add(limitObj);
        mapSLbyExtId.put (limitObj.Group_Id_vod__c, thisLimit);
      } 
      
      if (mapSLbyExtId.size() > 0) {
           for (Integer k = 0; k < cRow.size(); k++) {

             if (cRow[k].Limit_Applied_vod__c == true || cRow[k].Apply_Limit_vod__c == false || Trigger.isDelete == true)
                continue;

             cRow[k].Apply_Limit_vod__c = false;
             cRow[k].Limit_Applied_vod__c = true;
             
             Call2_vod__c myCall = calls.get(cRow[k].Call2_vod__c);
             String ownerId = null;
             Date CallDate = null;
            
             if (myCall != null) {
                ownerId = myCall.OwnerId;
                CallDate = myCall.Call_Date_vod__c;
                
             }
             System.Debug ('cRow=' +  cRow[k]);
             String AccountProd = VOD_CALL2_CHILD_COMMON.getLimitId (cRow[k].Account_vod__c,cRow[k].Product_vod__c, '');
             String AccountProdUser = VOD_CALL2_CHILD_COMMON.getLimitId (cRow[k].Account_vod__c,cRow[k].Product_vod__c, ownerId);
             String UserProd = VOD_CALL2_CHILD_COMMON.getLimitId ('',cRow[k].Product_vod__c, ownerId);
              
              List<Sample_Limit_vod__c>  AccountProdLst =  mapSLbyExtId.get(AccountProd);         
              List<Sample_Limit_vod__c>  AccountProdUserLst =  mapSLbyExtId.get(AccountProdUser);
              List<Sample_Limit_vod__c>  UserProdList =  mapSLbyExtId.get(UserProd);
            
              if (AccountProdLst != null && AccountProdLst.size() > 0) {
                    for (Sample_Limit_vod__c myLimit : AccountProdLst) {
                        if (CallDate >= myLimit.Start_Date_vod__c  && CallDate <= myLimit.End_Date_vod__c) {
                             
                            Sample_Limit_vod__c checkLimit = updateMap.get(myLimit.Id);
                            if (checkLimit != null) {
                                System.debug ('CheckLimit=' + checkLimit);
                                Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], checkLimit, myCall);
                                transactions.add(slT);
                                if (checkLimit.Limit_Per_Call_vod__c == false) {
	                                if (!VEEVA_SAMPLE_CANCEL.isSampleCancel) {
	                                    checkLimit.Disbursed_Quantity_vod__c += cRow[k].Quantity_vod__c;
	                                }
	                                else {
	                                    checkLimit.Disbursed_Quantity_vod__c -= cRow[k].Quantity_vod__c;
	                                }
                                }
                                checkLimit.Disable_Txn_Create_vod__c = true;
                                updateMap.put(checkLimit.Id,checkLimit);
                            } else {
                                System.debug ('myLimit=' + myLimit);
                                if (myLimit.Disbursed_Quantity_vod__c == null)
                                    myLimit.Disbursed_Quantity_vod__c = 0;
                                Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], myLimit, myCall);
                                transactions.add(slT);
                                if (myLimit.Limit_Per_Call_vod__c == false) {
	                                if (!VEEVA_SAMPLE_CANCEL.isSampleCancel) {
	                                   myLimit.Disbursed_Quantity_vod__c +=cRow[k].Quantity_vod__c;
	                                }
	                                else {
	                                    myLimit.Disbursed_Quantity_vod__c -=cRow[k].Quantity_vod__c;
	                                }
                                }
                                myLimit.Disable_Txn_Create_vod__c = true;
                                updateMap.put(myLimit.Id,myLimit);
                            }
                            
                        }
                    }           
              }
              if (AccountProdUserLst != null && AccountProdUserLst.size() > 0) {
                for (Sample_Limit_vod__c myLimit : AccountProdUserLst) {
                        if (CallDate >= myLimit.Start_Date_vod__c  && CallDate <= myLimit.End_Date_vod__c) {
                                Sample_Limit_vod__c checkLimit = updateMap.get(myLimit.Id);
                            if (checkLimit != null) {
                                System.debug ('CheckLimit=' + checkLimit);
                                Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], checkLimit, myCall);
                                transactions.add(slT);
                                if (checkLimit.Limit_Per_Call_vod__c == false) {
	                                if (!VEEVA_SAMPLE_CANCEL.isSampleCancel) {
	                                    checkLimit.Disbursed_Quantity_vod__c += cRow[k].Quantity_vod__c;
	                                }
	                                else {
	                                    checkLimit.Disbursed_Quantity_vod__c -= cRow[k].Quantity_vod__c;
	                                }
                                }
                                checkLimit.Disable_Txn_Create_vod__c = true;
                                updateMap.put(checkLimit.Id,checkLimit);
                            } else {
                                    System.debug ('myLimit=' + myLimit);
                                if (myLimit.Disbursed_Quantity_vod__c == null)
                                myLimit.Disbursed_Quantity_vod__c = 0;
                                
                                Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], myLimit, myCall);
                                transactions.add(slT);
                                if (myLimit.Limit_Per_Call_vod__c == false) {
	                                if (!VEEVA_SAMPLE_CANCEL.isSampleCancel) {
	                                    myLimit.Disbursed_Quantity_vod__c +=cRow[k].Quantity_vod__c;
	                                }
	                                else {
	                                    myLimit.Disbursed_Quantity_vod__c -=cRow[k].Quantity_vod__c;
	                                }
                                }
                                myLimit.Disable_Txn_Create_vod__c = true;
                                updateMap.put(myLimit.Id,myLimit);
                            }
                        }
                    }           
              }
              if (UserProdList != null && UserProdList.size() > 0) {
                for (Sample_Limit_vod__c myLimit : UserProdList) {
                        if (CallDate >= myLimit.Start_Date_vod__c  && CallDate <= myLimit.End_Date_vod__c) {
                                Sample_Limit_vod__c checkLimit = updateMap.get(myLimit.Id);
                            if (checkLimit != null) {
                                System.debug ('CheckLimit=' + checkLimit);
                                Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], checkLimit, myCall);
                                transactions.add(slT);
                                if (checkLimit.Limit_Per_Call_vod__c == false) {
	                                if (!VEEVA_SAMPLE_CANCEL.isSampleCancel) {
	                                    checkLimit.Disbursed_Quantity_vod__c += cRow[k].Quantity_vod__c;
	                                }
	                                else {
	                                    checkLimit.Disbursed_Quantity_vod__c -= cRow[k].Quantity_vod__c;
	                                }
                                }
                                checkLimit.Disable_Txn_Create_vod__c = true;
                                updateMap.put(checkLimit.Id,checkLimit);
                            } else {
                                    System.debug ('myLimit=' + myLimit);
                                if (myLimit.Disbursed_Quantity_vod__c == null)
                                    myLimit.Disbursed_Quantity_vod__c = 0;
                                    
                                Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], myLimit, myCall);
                                transactions.add(slT);
                                if (myLimit.Limit_Per_Call_vod__c == false) {
	                                if (!VEEVA_SAMPLE_CANCEL.isSampleCancel) {
	                                    myLimit.Disbursed_Quantity_vod__c +=cRow[k].Quantity_vod__c;
	                                }
	                                else {
	                                    myLimit.Disbursed_Quantity_vod__c -=cRow[k].Quantity_vod__c;
	                                }
                                }
                                myLimit.Disable_Txn_Create_vod__c = true;
                                updateMap.put(myLimit.Id,myLimit);
                            }
                        }
                    }           
              }

              // now determine which sample group limits need to be updated
              List<Product_Group_vod__c> prodGroups = productGroupMapping.get(cRow[k].Product_vod__c);
              if (prodGroups != null) {
                  for (Product_Group_vod__c prodGroup : prodGroups) {
                      // verify what active groups the product belongs to
                      if (CallDate >= prodGroup.Start_Date_vod__c && CallDate <= prodGroup.End_Date_vod__c) {
                          List<Sample_Limit_vod__c> acctGroupList = mapSLbyExtId.get(VOD_CALL2_CHILD_COMMON.getLimitId(cRow[k].Account_vod__c, prodGroup.Product_Catalog_vod__c, '')); 
                          if (acctGroupList != null) {
                              for (Sample_Limit_vod__c myLimit : acctGroupList) {
                                if (CallDate >= myLimit.Start_Date_vod__c && CallDate <= myLimit.End_Date_vod__c) {
                                    Sample_Limit_vod__c checkLimit = updateMap.get(myLimit.Id);
                                    if (checkLimit != null) {
                                        System.debug ('CheckLimit=' + checkLimit);
                                        Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], checkLimit, myCall);
                                        transactions.add(slT);
                                        if (checkLimit.Limit_Per_Call_vod__c == false) {
                                        	checkLimit.Disbursed_Quantity_vod__c += cRow[k].Quantity_vod__c;
                                        }
                                        checkLimit.Disable_Txn_Create_vod__c = true;
                                        updateMap.put(checkLimit.Id,checkLimit);
                                    } else {
                                        System.debug ('myLimit=' + myLimit);
                                        Sample_Limit_Transaction_vod__c slT = VOD_CALL2_CHILD_COMMON.createTransactionRecord ( cRow[k], myLimit, myCall);
                                        transactions.add(slT);
                                        if (myLimit.Disbursed_Quantity_vod__c == null)
                                            myLimit.Disbursed_Quantity_vod__c = 0; 
                                        if (myLimit.Limit_Per_Call_vod__c == false) {
                                        	myLimit.Disbursed_Quantity_vod__c +=cRow[k].Quantity_vod__c;
                                        }
                                        myLimit.Disable_Txn_Create_vod__c = true;
                                        updateMap.put(myLimit.Id,myLimit);
                                    }   
                                }
                             }
                         }
                      }
                  }
               }
           }
           
           if (transactions.size() > 0) {
            insert transactions;
           }
           if (updateMap.size() > 0) {
                update  updateMap.values();
           }
        
      }
}