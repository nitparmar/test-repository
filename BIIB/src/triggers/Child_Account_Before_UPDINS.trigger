trigger Child_Account_Before_UPDINS on Child_Account_vod__c (before insert, before update) {
    for (Child_Account_vod__c item: Trigger.new) {        
        item.External_ID_vod__c = item.Parent_Account_vod__c + '__' + item.Child_Account_vod__c; 
    }

}