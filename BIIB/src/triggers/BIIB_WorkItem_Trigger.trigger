/*  Name: BIIB_WorkItem_Trigger
    Author: USI Offshore 
    Created Date:14-Apr-14
    Reason: 
       1. To allow multiple Open workitem for recordtypes which are specified in the custom setting BIIB_Case_RecordTypes__c where Multiple is true
*/
trigger BIIB_WorkItem_Trigger on BIIB_Work__c(before Insert,before update, after insert, after update) 
{
    //Before Insert 
    if(Trigger.isInsert && Trigger.isBefore)
    {
        BIIB_Work_Handler_Class.OnBeforeInsert(Trigger.new);
    } 
    if(Trigger.isUpdate && Trigger.isBefore)
    {
        BIIB_Work_Handler_Class.OnBeforeUpdate(Trigger.new);
    } 
    if(Trigger.isInsert&& Trigger.isAfter)
    {
        BIIB_Work_Handler_Class.OnAfterInsert(Trigger.new); 
    }
    if(Trigger.isUpdate&& Trigger.isAfter)
    {
        BIIB_Work_Handler_Class.OnAfterUpdate(Trigger.new,Trigger.old); 
    }
  
}