trigger VEEVA_DCR_FIELD_TYPE_BEFORE_UPSERT on DCR_Field_Type_vod__c (before insert, before update) {
    Map<String,Profile> profileById = new Map<String, Profile>();
    for (DCR_Field_Type_vod__c record : Trigger.new) {
        String profileId = record.Profile_ID_vod__c;
        // Set profile name from provided ID
        if (String.isNotBlank(profileId)) {
            // check cache
        	Profile profile = profileById.get(profileId);
            if (profile == null) {
               profile = [SELECT Id,Name FROM Profile WHERE Id=:profileId];
            }
            if (profile != null) {
                // cache
                profileById.put(profileId, profile);
                // use
                record.Profile_Name_vod__c = profile.Name;
            }
        }
        // set unique key
        List<String> keyComponents = new String[] {record.Country_vod__c,
            record.Profile_Name_vod__c,record.Object_API_Name_vod__c,
            record.Record_Type_vod__c,record.Field_API_Name_vod__c};
        record.Unique_Key_vod__c = String.join(keyComponents,':');        
    }
}