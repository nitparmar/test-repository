/*  @Name: BIIB_Update_Cases
    @Author: Divya Galani 
    @Created Date:16-Apr-14
    @Description:PRE Functionality: US-0072
       1. Invokes Class BIIB_TemplateToObjectUpdates which in turn updates all Work Item Records related to a particular WI Template.
       2. This trigger is fired whenever an Work Item Template is updated by the administrator
       Modification Log
       -----------------------------------------------------------------------------------------------
       Developer				Date				Description
       Divya Galani				16-Apr-14			Original Version
       -----------------------------------------------------------------------------------------------
*/

trigger BIIB_Update_WorkItems on BIIB_Work_Item_Template__c (after update) 
{
    // Invoke Handler Class and pass Trigger.new and Trigger.oldMap to the class. The class will handle the further functionality
    if(Trigger.isUpdate && Trigger.isAfter)
    {
        BIIB_TemplateToObjectUpdates clsTemplateToObject = new BIIB_TemplateToObjectUpdates();
        
        //Frank Neezen: commented as this function does not exist and is breaking the build.
        //clsTemplateToObject.BIIB_UpdateWorkItems_FromWITemplate(Trigger.new,Trigger.oldMap);
    }
}