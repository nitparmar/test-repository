global class BIIB_Medicare_OutReach_Batch implements Database.Batchable<sObject>, Database.Stateful
{
    global final string query;
    
    global BIIB_Medicare_OutReach_Batch(String query)
    {
        this.query=query; // Get the query from the controller as a string      
    }  
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //query all the patient Account with age greater than 65
        //return Database.getQueryLocator('SELECT Id FROM Account WHERE RecordTypeId=\''+String.escapeSingleQuotes('012110000000BBl')+'\' AND IsPersonAccount=true AND BIIB_Age__c > 65');
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC,List<Account> scope)
    {
        Set<Id> setPatientId = new Set<Id>();
        List<Case> lstCaseToInsert = new List<Case>();
        Database.SaveResult[] lstDatabaseResult;
        List<BIIB_Error_Log__c> lstErrorLog = new List<BIIB_Error_Log__c>();
        
        //fetch all the patients ids into a set.
        for(Account s : scope)
        {
            setPatientId.add(String.ValueOf(s.get('Id')));
        }
        
        system.debug('-------setPatientId--------'+setPatientId);
        
        //query all the patient plan which will be expired in next 6 days
        List<BIIB_Patient_Plan__c> lstPatientPlan = new List<BIIB_Patient_Plan__c>();
        lstPatientPlan = [SELECT Id, BIIB_Patient_Lookup__c, BIIB_Patient_Lookup__r.PersonContactId FROM BIIB_Patient_Plan__c WHERE BIIB_Patient_Lookup__c IN : setPatientId AND BIIB_End_Date__c = NEXT_N_DAYS:6];//AND End_Date__c > TODAY
        
        system.debug('-----lstPatientPlan--------'+lstPatientPlan);
        
        //Create case record for each patient plan record which is going to expire in next 6 days
        if(lstPatientPlan.size() > 0)
        {
            //create service request of type "Medicare Outreach" for each record in patient plan and assign the ownership to queue "MCOS Queue"
            for(BIIB_Patient_Plan__c objPP : lstPatientPlan)
            {
                Case objCase = new Case(RecordTypeId='012110000000GQ2',AccountId=objPP.BIIB_Patient_Lookup__c,ContactId=objPP.BIIB_Patient_Lookup__r.PersonContactId, OwnerId='00G11000000G8co');
                lstCaseToInsert.add(objCase);
            }
        }
        
        if(lstCaseToInsert.size() > 0)
            lstDatabaseResult = Database.insert(lstCaseToInsert,false);
        
        //loop through the save result and log the errors into the system         
        for(Database.SaveResult objSR : lstDatabaseResult)
        {
            for(Database.Error err :  objSR.getErrors())
            {
                BIIB_Error_Log__c objEL = new BIIB_Error_Log__c(Component_Name__c='BIIB_Medicare_OutReach_Batch', Error_Detail__c=err.getMessage(), Error_Type__c = 'Batch Failure', Failure_Record_Id__c = objSR.getId(), BIIB_Timestamp__c = system.now());
                lstErrorLog.add(objEL);
            }
        }
        
        //Insert Erros into the error log
        if(lstErrorLog.size() > 0)
        {
            insert lstErrorLog;
        }
    }
   
    global void finish(Database.BatchableContext BC)
    {
        AsyncApexJob a = [SELECT id, ApexClassId, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedBy.Email 
                          FROM AsyncApexJob WHERE id = :BC.getJobId()];
    
        String emailMessage = 'Your batch job ' + 'Medicare Outreach Scan' +' has finished.  It executed ' + a.totalJobItems + 
                              ' batches.  Of which, ' + a.jobitemsprocessed + ' processed without any exceptions thrown and ' + 
                              a.numberOfErrors +  ' batches threw unhandled exceptions.';            

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String[] toAddresses = new String[] {a.createdBy.email};

        mail.setToAddresses(toAddresses);
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSenderDisplayName('Batch Job Summary');
        mail.setSubject('Batch job completed');
        mail.setPlainTextBody(emailMessage);
        mail.setHtmlBody(emailMessage);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}