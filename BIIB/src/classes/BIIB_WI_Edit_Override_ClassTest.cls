/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/24/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_WI_Edit_Override_Class .
 * 
 */
@isTest
private class BIIB_WI_Edit_Override_ClassTest {

        public static User tempUser = [Select id from User limit 1];
        
        static testMethod void myUnitTest() {

        Case tempCase = BIIB_testClassHelper.createCase();
        insert tempCase;
        
        BIIB_Work__c tempWork = BIIB_testClassHelper.createWork(tempUser , tempCase);
        insert tempWork;
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Record',tempWork.id);
        BIIB_WI_Edit_Override_Class obj = new BIIB_WI_Edit_Override_Class(new ApexPages.StandardController(tempWork));
        obj.editRedirectToWork();
        Test.stopTest();
    }
}