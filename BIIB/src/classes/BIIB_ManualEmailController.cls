public class BIIB_ManualEmailController
{
    public String strEmailAddress {get;set;}
    public String strEmailAddress2 {get;set;}
    public String strSelTemplateId {get;set;}
    public String strTemplateBody {get;set;}
    public boolean isModifyClicked {get;set;}
    public blob attachFile {get;set;}
    
    String strSelTemplateClone = '';
    String strTemplateBodyClone = '';
    List<EmailTemplate> lstEmailTemplate = new List<EmailTemplate>();
    List<String> lstUserEmail = new List<String>();
    
    public BIIB_ManualEmailController(ApexPages.StandardController stdController)
    {
        isModifyClicked = false;
        strTemplateBody = '';
        lstEmailTemplate = [SELECT Name, Id FROM EmailTemplate];
        for(User objUser : [SELECT Name, Email FROM User WHERE IsActive = true])
        {
            lstUserEmail.add(objUser.Email);
        }
        lstUserEmail.sort();
    }
    
    public List<SelectOption> getEmailAddress() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','----None----'));
        for(String strEmail : lstUserEmail)
        {
            options.add(new SelectOption(strEmail, strEmail));
        }
        return options;
    }
    
    public List<SelectOption> getTemplateId() {
        List<SelectOption> options = new List<SelectOption>();
        
        if(!isModifyClicked)
        {
            options.add(new SelectOption('','----None----'));
            for(EmailTemplate objEmailTemp : lstEmailTemplate)
                options.add(new SelectOption(objEmailTemp.Id,objEmailTemp.Name));
        }
        else
        {
            options.add(new SelectOption('Custom','Custom'));
        }
        return options;
    }
    
    public void modifyAction()
    {
        isModifyClicked = true;
        strSelTemplateClone = strSelTemplateId;
        strTemplateBodyClone = strTemplateBody;
    }
    
    public void cancel()
    {
        if(isModifyClicked)
        {
            isModifyClicked = false;
            strTemplateBody = strTemplateBodyClone;
            strSelTemplateId = strSelTemplateClone;
        }
        system.debug('----------strSelTemplateId----------'+strSelTemplateId);
    }
    
    public void parseTemplate()
    {
        if(strSelTemplateId == null || strSelTemplateId == '')
        {
            strTemplateBody = '';
        }
            
        if(strSelTemplateId != '' && strSelTemplateId != null)
        {
            // Here we will build the single email message
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{'kammehta@deloitte.com'};
            mail.setToAddresses(toAddresses);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setSenderDisplayName('MMPT');
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setTemplateId(strSelTemplateId);
            
            Savepoint sp = Database.setSavepoint();
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            Database.rollback(sp);
            
            strTemplateBody = mail.getPlainTextBody();
            String mailTextBody = mail.getPlainTextBody();
            String mailHtmlBody = mail.getHTMLBody();
            String mailSubject = mail.getSubject();
            system.debug('------mailTextBody-----'+mailTextBody);
            system.debug('------mailHtmlBody-----'+mailHtmlBody);
            system.debug('------mailSubject-----'+mailSubject);
        }
    }
}