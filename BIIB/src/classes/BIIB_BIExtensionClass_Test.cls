/**
*@author Aditya Ghosh
*@date 22/04/2014
@test class for BIIB_BIExtensionClass
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Aditya       22/04/2014  OriginalVersion
*/
@isTest(seeAllData = true)
public class BIIB_BIExtensionClass_Test{

    public static testmethod void testFunction(){
        test.startTest();
            Case tempCase = BIIB_testClassHelper.createCase();
            insert tempCase;
            ApexPages.StandardController sc = new ApexPages.standardController(tempCase);
            BIIB_BIExtensionClass testInstance = new BIIB_BIExtensionClass(sc);
            testInstance.BindData(5);
            Pagereference returnPageRef;
            returnPageRef = testInstance.editBI();
            returnPageRef = testInstance.newBI();
            returnPageRef = testInstance.nextBtnClick();
            returnPageRef = testInstance.previousBtnClick();
            
            Integer returnInt;
            returnInt = testInstance.getPageNumber();
            returnInt = testInstance.getPageSize();
            returnInt = testInstance.getTotalPageNumber();
            
            Boolean returnBoolean;
            returnBoolean = testInstance.getPreviousButtonEnabled();
            returnBoolean = testInstance.getNextButtonDisabled();
        test.stopTest();
    }
}