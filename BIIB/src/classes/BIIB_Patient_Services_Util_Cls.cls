global without sharing class BIIB_Patient_Services_Util_Cls {
	
	/**
     * Method to return map of developer name and recordtype id for object
     * @param sObjectName API name of the object
     * @return Map<String,Id> map of developer name and recordtype id
    **/
    
    public static Map<String,sObject> getMapOfRecordTypeId(String sObjectName){
        Map<String,sObject> mapOfRecordTypeId = new Map<String,sObject>();
            List<RecordType> lstRecordTypeList = [Select 
	            										Id,
	            										Name, 
	            										DeveloperName 
            									From 
            											RecordType 
            									Where 
            											SobjectType =:sObjectName
            									And
            											IsActive = true 
            									LIMIT 
            											100];
            if(!(lstRecordTypeList.isempty())){
                for(RecordType objRecordType:lstRecordTypeList){
                    mapOfRecordTypeId.put(objRecordType.DeveloperName, objRecordType);
                }
            }
            return mapOfRecordTypeId;
        }
     
     
     /**
     * Method to return map of developer name and recordtype id for object
     * @param sObjectName API name of the object
     * @return Map<String,Id> map of developer name and recordtype id
    **/
    
    public static Map<Id,sObject> getMapOfRecordTypeDeveloperName(String sObjectName){
        Map<Id,sObject> mapOfRecordTypeDeveloperName = new Map<Id,sObject>();
            List<RecordType> lstRecordTypeList = [Select 
	            										Id,
	            										Name, 
	            										DeveloperName 
            									From 
            											RecordType 
            									Where 
            											SobjectType =:sObjectName
            									And
            											IsActive = true 
            									LIMIT 
            											100];
            if(!(lstRecordTypeList.isempty())){
                for(RecordType objRecordType:lstRecordTypeList){
                    mapOfRecordTypeDeveloperName.put(objRecordType.Id, objRecordType);
                }
            }
            return mapOfRecordTypeDeveloperName;
        }
        
}