@isTest(seealldata=true)
public with sharing class BIIB_Test_Work_Handler_Class 
{ 
  
      public static set<String> setWorkRecordTypes = new set<String>{'BIIB_Work_Item_1','BIIB_Work_Item_2','BIIB_Shipment'};
      public static List<RecordType> case_rectype = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SObjectType='Case' AND Name  in ('12 Week Questionnaire' , 'Adverse Event' , 'Benefit Investigation' , 'Financial Assistance' , 'Medicare Outreach' , 'Patient Check-In' , 'Support')  ORDER BY Name];
      
      static testMethod void  BIIB_Multiple_Work_Check()
      {
    try
    {
        User objUser = [SELECT Id FROM User WHERE username = 'genericuser@deloitte.com']; 
      Map<String, Id> mapWorkRecordTypes = new Map<String, Id>();
      List <RecordType> lstWorkRecordTypes = [SELECT DeveloperName, Id from RecordType where DeveloperName IN : setWorkRecordTypes];
           system.debug('---lstWorkRecordTypes----'+lstWorkRecordTypes);
           
           
            for(RecordType objRecordType : lstWorkRecordTypes)
            {
                if(!mapWorkRecordTypes.containsKey(objRecordType.DeveloperName))
                {
                    mapWorkRecordTypes.put(objRecordType.DeveloperName,objRecordType.Id);
                }
                else
                {
                    mapWorkRecordTypes.put(objRecordType.DeveloperName,objRecordType.Id);
                }
            }
           system.debug('---mapWorkRecordTypes----'+mapWorkRecordTypes);
        
        test.startTest();
        /*
        BIIB_Work_Item_RecordType__c c = new BIIB_Work_Item_RecordType__c();
          c.Name = 'BIIB_Work_Item_1';
          c.BIIB_Multiple_Allowed__c = true;
          insert c;
          system.debug('----customsetting'+c);
       
      c = new BIIB_Work_Item_RecordType__c();
          c.Name = 'BIIB_Work_Item_2';
          c.BIIB_Multiple_Allowed__c = false;
          insert c;
          system.debug('----customsetting'+c);
           
       c = new BIIB_Work_Item_RecordType__c();
          c.Name = 'BIIB_Shipment';
          c.BIIB_Multiple_Allowed__c = false;
          insert c;
          system.debug('----customsetting'+c); 
  */
            //Creating Account Test data
      Account objAccount = BIIB_testClassHelper.createAccountPatient('Steve','Waugh');
      insert objAccount;
          
      //Creating Case Test data--Case1--BI
      Case objCaseBI = new Case(
                    BIIB_Is_Pre_Created__c = true, 
                    BIIB_Comments__c = 'testcase1', 
                    AccountId = objAccount.Id,
                    RecordTypeId = case_rectype[2].Id,
                    Status = 'New'
                    );                         
          insert   objCaseBI;
          system.debug('***objCase*****' +objCaseBI.Id);
          
      //Creating Case Test data--Case2--FA
      Case objCaseFA = new Case(
                    BIIB_Is_Pre_Created__c = true, 
                    BIIB_Comments__c = 'testcase2', 
                    AccountId = objAccount.Id,
                    RecordTypeId = case_rectype[3].Id,
                    Status = 'New'
                    );                         
          insert   objCaseFA;
          system.debug('***objCase*****' +objCaseFA.Id);
          
      //Creating Work Item Test data
           BIIB_Work__c objWork1 = new BIIB_Work__c(Name = 'Work1',
                           BIIB_Account__c = objAccount.Id,
                           BIIB_Assigned_to__c = objUser.Id,
                           BIIB_ININ_ID__c = '666666',
                           BIIB_Preferred_Language__c = 'English',
                           BIIB_Preferred_Time__c = '8PM',
                           BIIB_Priority__c = '85',
                           BIIB_Region__c = 'UCAN',
                           BIIB_Status__c = 'New',
                           BIIB_Work_Group__c = 'Acquisition',
                           RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_1'),
                           BIIB_Case__c = objCaseBI.Id
                           
                           );
      System.debug('-----------before objWork1 insert-----------------');                     
      insert objWork1;
      
      //Creating Work Item Test data
           BIIB_Work__c objWork2 = new BIIB_Work__c(Name = 'Work2',
                           BIIB_Account__c = objAccount.Id,
                           BIIB_Assigned_to__c = objUser.Id,
                           BIIB_ININ_ID__c = '666666',
                           BIIB_Preferred_Language__c = 'Spanish',
                           BIIB_Preferred_Time__c = '8PM',
                           BIIB_Priority__c = '85',
                           BIIB_Region__c = 'EMEA',
                           BIIB_Status__c = 'New',
                           BIIB_Work_Group__c = 'Acquisition',
                           RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_2'),
                           BIIB_Case__c = objCaseBI.Id
                           
                           );
        System.debug('-----------before objWork2 insert-----------------');                      
      insert   objWork2;
      /*
      BIIB_Work__c objWork3 = new BIIB_Work__c(Name = 'Work1',
                           BIIB_Account__c = objAccount.Id,
                           BIIB_Assigned_to__c = objUser.Id,
                           BIIB_ININ_ID__c = '666666',
                           BIIB_Preferred_Language__c = 'English',
                           BIIB_Preferred_Time__c = '8PM',
                           BIIB_Priority__c = '85',
                           BIIB_Region__c = 'UCAN',
                           BIIB_Status__c = 'New',
                           BIIB_Work_Group__c = 'Acquisition',
                           RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_1'),
                           BIIB_Case__c = objCaseFA.Id
                           
                           );
      System.debug('-----------before objWork3 insert-----------------');                       
      insert   objWork3;
      
      //Creating Work Item Test data
           BIIB_Work__c objWork4 = new BIIB_Work__c(Name = 'Work2',
                           BIIB_Account__c = objAccount.Id,
                           BIIB_Assigned_to__c = objUser.Id,
                           BIIB_ININ_ID__c = '666666',
                           BIIB_Preferred_Language__c = 'Spanish',
                           BIIB_Preferred_Time__c = '8PM',
                           BIIB_Priority__c = '85',
                           BIIB_Region__c = 'EMEA',
                           BIIB_Status__c = 'New',
                           BIIB_Work_Group__c = 'Acquisition',
                           RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_2'),
                           BIIB_Case__c = objCaseFA.Id
                           
                           );
       System.debug('-----------before objWork4 insert-----------------');                       
      insert   objWork4;
      */
      //Creating Work Item Test data
           BIIB_Work__c objWork5 = new BIIB_Work__c(Name = 'Work3',
                           BIIB_Account__c = objAccount.Id,
                           BIIB_Assigned_to__c = objUser.Id,
                           BIIB_ININ_ID__c = '666666',
                           BIIB_Preferred_Language__c = 'Spanish',
                           BIIB_Preferred_Time__c = '8PM',
                           BIIB_Priority__c = '85',
                           BIIB_Region__c = 'EMEA',
                           BIIB_Status__c = 'New',
                           BIIB_Work_Group__c = 'Acquisition',
                           RecordTypeId = mapWorkRecordTypes.get('BIIB_Shipment'),
                           BIIB_Case__c = objCaseBI.Id,
                           BIIB_Shipment_Status__c = 'Received'
                           
                           );
      System.debug('-----------before objWork5 insert-----------------');                       
      insert   objWork5;
    
          
    
    /*  List<BIIB_Work__c> lstWorkObject = new List<BIIB_Work__c>();
      lstWorkObject.add(0, objWork1);
      lstWorkObject.add(1, objWork2);
      lstWorkObject.add(2, objWork3);
      lstWorkObject.add(3, objWork4);
      lstWorkObject.add(4, objWork5);
      
      insert lstWorkObject;
      */
                
          
         test.stopTest();
      }
    catch(Exception e)
    {
      Boolean expectedExceptionThrown =  e.getMessage().contains('There is already open Work with the same type on the same Service Request') ? true : false;
      System.AssertEquals(expectedExceptionThrown, true);
    }
  }
  
  
    static testMethod void  BIIB_2nd_IF_Block()
      {
    try
    {
        User objUser = [SELECT Id FROM User WHERE username = 'genericuser@deloitte.com']; 
      Map<String, Id> mapWorkRecordTypes = new Map<String, Id>();
      List <RecordType> lstWorkRecordTypes = [SELECT DeveloperName, Id from RecordType where DeveloperName IN : setWorkRecordTypes];
           system.debug('---lstWorkRecordTypes----'+lstWorkRecordTypes);
           
           
            for(RecordType objRecordType : lstWorkRecordTypes)
            {
                if(!mapWorkRecordTypes.containsKey(objRecordType.DeveloperName))
                {
                    mapWorkRecordTypes.put(objRecordType.DeveloperName,objRecordType.Id);
                }
                else
                {
                    mapWorkRecordTypes.put(objRecordType.DeveloperName,objRecordType.Id);
                }
            }
           system.debug('---mapWorkRecordTypes----'+mapWorkRecordTypes);
        
        test.startTest();
        /*
        BIIB_Work_Item_RecordType__c c = new BIIB_Work_Item_RecordType__c();
          c.Name = 'BIIB_Work_Item_1';
          c.BIIB_Multiple_Allowed__c = true;
          insert c;
          system.debug('----customsetting'+c);
       
      c = new BIIB_Work_Item_RecordType__c();
          c.Name = 'BIIB_Work_Item_2';
          c.BIIB_Multiple_Allowed__c = false;
          insert c;
          system.debug('----customsetting'+c); 
            */
  
            //Creating Account Test data
      Account objAccount = BIIB_testClassHelper.createAccountPatient('Steve','Waugh');
      insert objAccount;
          
      //Creating Case Test data--Case1--BI
      Case objCaseBI = new Case(
                    BIIB_Is_Pre_Created__c = true, 
                    BIIB_Comments__c = 'testcase1', 
                    AccountId = objAccount.Id,
                    RecordTypeId = case_rectype[2].Id,
                    Status = 'New'
                    );                         
          insert   objCaseBI;
          system.debug('***objCase*****' +objCaseBI.Id);          
          
      //Creating Work Item Test data
           BIIB_Work__c objWork1 = new BIIB_Work__c(Name = 'Work1',
                           BIIB_Account__c = objAccount.Id,
                           BIIB_Assigned_to__c = objUser.Id,
                           BIIB_ININ_ID__c = '666666',
                           BIIB_Preferred_Language__c = 'English',
                           BIIB_Preferred_Time__c = '8PM',
                           BIIB_Priority__c = '85',
                           BIIB_Region__c = 'UCAN',
                           BIIB_Status__c = 'New',
                           BIIB_Work_Group__c = 'Acquisition',
                           BIIB_Case__c = objCaseBI.Id
                           );
                           
      insert objWork1;
      
   /*   List<BIIB_Work__c> lstWorkObject = new List<BIIB_Work__c>();
      lstWorkObject.add(0, objWork1);
      insert lstWorkObject;
     */ 
      objWork1.BIIB_Status__c = 'In Progress';
      update objWork1;
          
         test.stopTest();
      }
    catch(Exception e)
    {
      
    }
  }
}