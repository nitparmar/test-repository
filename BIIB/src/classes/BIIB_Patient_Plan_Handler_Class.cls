/**
*@author Rancy Fernandes
*@date 08/04/2014
@description: Handler class on Patient Plan trigger
       1. User story US-0128. To allow only one primary Patient Plan for a Patient
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rancy      08/04/2014  OriginalVersion
*/

public with sharing class BIIB_Patient_Plan_Handler_Class {

    public static void OnBeforeInsert(List<BIIB_Patient_Plan__c> newppList) {
        if(newppList != null && !newppList.isEmpty()) {
            primaryPlan(newppList);
        }
    }
    
    public static void OnBeforeUpdate(Map<Id,BIIB_Patient_Plan__c> oldppMap, Map<Id,BIIB_Patient_Plan__c> newppMap) {
        if(newppMap != null && !newppMap.isEmpty()) {
            primaryPlan(newppMap.values());
        }
    }
    
    // To allow only one primary Patient Plan for a Patient
    static void primaryPlan(List<BIIB_Patient_Plan__c> triggerppList){
        
        Set<Id> patientIdSet = new Set<Id>();
        Set<Id> patientPlanIdSet = new Set<Id>();
        List<BIIB_Patient_Plan__c> ppList = new List<BIIB_Patient_Plan__c>();
        List<BIIB_Patient_Plan__c> updppList = new List<BIIB_Patient_Plan__c>();
        
        for (BIIB_Patient_Plan__c pp: triggerppList){
            if(pp.BIIB_Primary__c && !pp.BIIB_Active__c){
                pp.addError(System.Label.BIIB_Primary_End_Date_Plans);
            }else if(pp.BIIB_Primary__c){
                    patientIdSet.add(pp.BIIB_Patient_Lookup__c);
                    patientPlanIdSet.add(pp.Id);
                    //system.debug('patientIdSet.............................'+patientIdSet);
                    //system.debug('patientPlanIdSet.............................'+patientPlanIdSet);
            }  
        }
        ppList = [Select Id, Name, BIIB_Primary__c from BIIB_Patient_Plan__c where BIIB_Patient_Lookup__c in :patientIdSet and BIIB_Primary__c = true and Id NOT in :patientPlanIdSet];
        //system.debug('ppList..................................'+ppList);
        for (BIIB_Patient_Plan__c pp : ppList ){
            BIIB_Patient_Plan__c p = new BIIB_Patient_Plan__c();
            p = pp;
            p.BIIB_Primary__c = false;
            updppList.add(p);
        }
        try{
            update updppList;
        }catch(Exception e){
            for(BIIB_Patient_Plan__c pp : updppList){
                pp.addError(e.getMessage());
            }
        }
    }

}