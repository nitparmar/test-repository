/*  
    @author: Divya Galani 
    @date:22-Apr-14
    @description:US-0085: This user story involves creation of Follow Up or Escalate To Field Work Item Records based on the call disposition to HCP/patient.
       1. This Class has multiple methods
       ValidateNoOfAttempts: Invoked From VF Page:BIIB_WorkItem_Disposition.:This method checks if a particular work item record type can be followed up or escalated based on the No of Attempts applicable for the work item recordtype
       CreateFollowUpWorkItem: Invoked From VF Page:BIIB_WorkItem_Disposition.: This method Creates Follow Up Work Item Record whenever Reason Code is selected as "Create Follow Up" on the parent WI and validation criteria have been met.
       CreateFollowUpWorkItemFromPopUp:Invoked From VF page:BIIB_WI_Disposition_Popup:Creates Followup work item.
       CreateEscalateToFieldWI:Invoked From VF page:BIIB_WI_Disposition_Popup:Creates Escalate To Field work item.
       Modification Log
       -----------------------------------------------------------------------------------------------
       Developer                Date                Description
       Divya Galani             22-Apr-14           Original Version
       -----------------------------------------------------------------------------------------------
*/

public class BIIB_WorkItem_Disposition_Class
{
    String strWorkItemId;
    String strCaseId;
    String strRecordTypeId;
    String baseURL;
    String redirectToCase;
    String strNewWorkId;
    String strQueryCondition;
    String strRecTypeId;
    public String strOutput {get;set;}
    public String strUserInputFollowUp {get;set;}
    public String strUserInputEsclToField {get;set;}
    public String strInvokeDispLogic{get;set;}
    public String strWorkItemRowId{get;set;}
    public String strGetParam{get;set;}
    public String strRedirectId{get;set;}
    public String strWIError{get;set;}
    private final BIIB_Work__c objWork;
    private static set<String> setWorkClosedStatus = new Set<String>{'Completed','Closed-Failed'};
    Map<String,Integer> mapRecTypeToNoOfAttempts = new Map<String,Integer>();
    Map<String,Boolean> mapRecTypeToEsclFlag = new Map<String,Boolean>();
    BIIB_Work__c objQueryWork;
    
    public BIIB_WorkItem_Disposition_Class(ApexPages.StandardController controller) 
    {
        
        system.debug('----InsideStandardController----');
        this.objWork = (BIIB_Work__c)controller.getRecord(); 
        strInvokeDispLogic = 'Yes';
        system.debug('---strInvokeDispLogic---'+strInvokeDispLogic);
        strNewWorkId = ApexPages.currentPage().getParameters().get('newid');
        system.debug('---strNewWorkId---'+strNewWorkId);
    }
    /**
    @author Divya Galani
    @description This method navigates to newly created work item.
    @param none
    @return pagereference
    */
    public pagereference redirectToWIPage()
    {
        system.debug('----InsideredirectToWIPage----');
        system.debug('----InsideredirectToWIPagestrNewWorkId----'+strNewWorkId);
        PageReference pgRef = new PageReference('/'+strNewWorkId);
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    /**
    @author Divya Galani
    @description This method navigates to newly created Follow Up work item.
    @param none
    @return pagereference
    */
    public pagereference redirectToFollowUpWI()
    {
        system.debug('----redirectToFollowUpWI----');
        system.debug('----redirectToFollowUpWIstrGetParam----'+strGetParam);
        PageReference pgRef = new PageReference('/'+strGetParam);
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    /**
    @author Divya Galani
    @description This method checks if a particular work item record type can be followed up or escalated based on the No of Attempts applicable for the work item recordtype
    @param none
    @return none
    */
    public void ValidateNoOfAttempts()
    {
        
        system.debug('----InsideValidateNoOfAttempts----');
        system.debug('----strNewWorkId----'+strNewWorkId);
        system.debug('---strInvokeDispLogicInsideMethod---'+strInvokeDispLogic);
        strInvokeDispLogic = 'No';
        
        //Query the Work Item on which agent is currently working
        objQueryWork = [SELECT Id,RecordTypeId,BIIB_Status__c,Name,BIIB_Reason_Code__c,BIIB_Account__c,BIIB_Assigned_to__c,BIIB_Case__c,BIIB_ININ_ID__c,BIIB_Parent_Disposition_Work__c,BIIB_Preferred_Language__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Region__c,BIIB_Required__c,BIIB_Work_Group__c FROM BIIB_Work__c WHERE Id =:strNewWorkId];
        strCaseId = objQueryWork.BIIB_Case__c;
        system.debug('---strCaseId----'+strCaseId);
        
        RecordType objWIRecType= [SELECT Id,Name,DeveloperName from RecordType where Id =: objQueryWork.RecordTypeId];
        
        // Check if this Work Item has Parent Disposition Set.If it is null, it means that this is the first time he is creating this WI
        if(objQueryWork.BIIB_Parent_Disposition_Work__c == NULL)
        {
            system.debug('---ParentDispositionNull---');
            strQueryCondition = objQueryWork.Id;
            strRecTypeId = objQueryWork.RecordTypeId;
        }
        // Check if this Work Item has Parent Disposition Set.If it is not null, it means that this is follow up work item record on which agent is working on.
        if(objQueryWork.BIIB_Parent_Disposition_Work__c != NULL)
        {
            system.debug('---ParentDispositionNotNull---');
            strQueryCondition = objQueryWork.BIIB_Parent_Disposition_Work__c;
            BIIB_Work__c objParentRecord = [SELECT Id,RecordTypeId FROM BIIB_Work__c WHERE Id =:strQueryCondition];
            strRecTypeId = objParentRecord.RecordTypeId;
        }
        List<BIIB_Work__c> lstWorkDetails = [SELECT Id,Name,RecordType.DeveloperName FROM BIIB_Work__c WHERE BIIB_Case__c =: strCaseId AND RecordTypeId =:strRecTypeId]; 
        system.debug('----lstWorkDetails----'+lstWorkDetails);
        
        //Logic to create a Map for <RecordTypeName,NoOfAttempts> by querying custom setting
        for(BIIB_Work_Item_RecordType__c lstWorkRecTypeCustomSetting: [SELECT Name,BIIB_No_Of_Attempts__c FROM BIIB_Work_Item_RecordType__c])
        {
                mapRecTypeToNoOfAttempts.put(lstWorkRecTypeCustomSetting.Name,Integer.valueOf(lstWorkRecTypeCustomSetting.BIIB_No_Of_Attempts__c));
        }
        
        
        // Attempts Reached   
        if(lstWorkDetails.size() == mapRecTypeToNoOfAttempts.get(objWIRecType.DeveloperName) && (objQueryWork.BIIB_Reason_Code__c == System.Label.BIIB_Work_Reason_Code_Values || objQueryWork.BIIB_Reason_Code__c == NULL)) 
        {
            // No of Attempts have reached. Provide a pop-up to the user saying that do you still want to followup or escalate
            strWorkItemRowId = objQueryWork.Id;
            strOutput = 'NoOfAttemptsReached';
        }
        //Attempts Not reached
        if(lstWorkDetails.size() < mapRecTypeToNoOfAttempts.get(objWIRecType.DeveloperName) && objQueryWork.BIIB_Reason_Code__c == System.Label.BIIB_Work_Reason_Code_Values)
        {
            // Create FollowUp Work Item
            strWorkItemRowId = objQueryWork.Id;
            system.debug('---strWorkItemRowId---'+strWorkItemRowId);
            strOutput = 'NoOfAttemptsPending';
        }
        //First Time Creation--Redirect
        if(objQueryWork.BIIB_Reason_Code__c == NULL)
        {
            
            strOutput = 'ReDirectNewWI';
        }
        // Attempts Exceeded
        if(lstWorkDetails.size() > mapRecTypeToNoOfAttempts.get(objWIRecType.DeveloperName) && (objQueryWork.BIIB_Reason_Code__c == System.Label.BIIB_Work_Reason_Code_Values || objQueryWork.BIIB_Reason_Code__c == NULL))
        {
            // Create FollowUp Work Item
            strWorkItemRowId = objQueryWork.Id;
            system.debug('---strWorkItemRowId---'+strWorkItemRowId);
            strOutput = 'NoOfAttemptsReached';
        }
        // Attempts Null--Create Follow Up
        if(mapRecTypeToNoOfAttempts.get(objWIRecType.DeveloperName) == NULL || mapRecTypeToNoOfAttempts.get(objWIRecType.DeveloperName) == 0)
        {
            // Create FollowUp Work Item
            system.debug('---NoOfAttemptsNull---');
            strWorkItemRowId = objQueryWork.Id;
            system.debug('---strWorkItemRowId---'+strWorkItemRowId);
            strOutput = 'NoOfAttemptsPending';
        }
    }// End public void ValidateNoOfAttempts()
    
    /**
    @author Divya Galani
    @description This method creates Follow Up Work Item
    @param none
    @return none
    */
    public void CreateFollowUpWorkItem()
    {
        try
        {
            system.debug('----InsideApexClassMethodCreateFollowUpWorkItem-----');
            system.debug('----strWorkItemRowId-----'+strWorkItemRowId);
            
            // Get Custom Label Values
            List<String> workClosedStatusList = ((String)System.Label.BIIB_Work_Closed_Status_values).split(';');
            system.debug('----workClosedStatusList-----'+workClosedStatusList);
            
            String strStatus1 = workClosedStatusList[0];
            String strStatus2 = workClosedStatusList[1];
            
            Map<String,Boolean> mapWorkTypeToPermission = new Map<String,Boolean>();
            for(BIIB_Work_Item_RecordType__c workRecordtypeList : [SELECT Name, BIIB_Multiple_Allowed__c FROM BIIB_Work_Item_RecordType__c])
            {
                mapWorkTypeToPermission.put(workRecordtypeList.Name,workRecordtypeList.BIIB_Multiple_Allowed__c);
            }
            system.debug('------mapWorkTypeToPermission------'+mapWorkTypeToPermission);
            RecordType objRecTypeName = [SELECT DeveloperName FROM RecordType WHERE Id =:objQueryWork.RecordTypeId];
                
            system.debug('---objRecTypeName--'+objRecTypeName);
            objQueryWork = [SELECT Id,BIIB_Status__c,RecordTypeId,Name,BIIB_Reason_Code__c,BIIB_Account__c,BIIB_Assigned_to__c,BIIB_Case__c,BIIB_ININ_ID__c,BIIB_Parent_Disposition_Work__c,BIIB_Preferred_Language__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Region__c,BIIB_Required__c,BIIB_Work_Group__c FROM BIIB_Work__c WHERE Id =:strWorkItemRowId];

            if(mapWorkTypeToPermission!=null && !mapWorkTypeToPermission.isEmpty()){
                if(mapWorkTypeToPermission.containsKey(objRecTypeName.DeveloperName)){    
                    if(Boolean.valueOf(mapWorkTypeToPermission.get(objRecTypeName.DeveloperName)) == false && objQueryWork.BIIB_Reason_Code__c == System.Label.BIIB_Work_Reason_Code_Values)
                    {
                        // Close Previous Work Item                     
                        if(objQueryWork.BIIB_Status__c != strStatus1 || objQueryWork.BIIB_Status__c != strStatus2)
                        {
                            system.debug('--inrectype2condition---');
                            strWIError = 'CloseOriginalWI';
                        }
                    }
                }
            }
            if(objQueryWork.BIIB_Reason_Code__c == System.Label.BIIB_Work_Reason_Code_Values)
            {
                BIIB_Work__c objFollowUp = new BIIB_Work__c(
                                                    Name = objQueryWork.Name,
                                                    BIIB_Account__c = objQueryWork.BIIB_Account__c,
                                                    BIIB_Assigned_to__c = objQueryWork.BIIB_Assigned_to__c,
                                                    BIIB_Case__c = objQueryWork.BIIB_Case__c,
                                                    BIIB_ININ_ID__c = objQueryWork.BIIB_ININ_ID__c,
                                                    BIIB_Preferred_Language__c = objQueryWork.BIIB_Preferred_Language__c,
                                                    BIIB_Preferred_Time__c = objQueryWork.BIIB_Preferred_Time__c,
                                                    BIIB_Priority__c = objQueryWork.BIIB_Priority__c,
                                                    BIIB_Region__c = objQueryWork.BIIB_Region__c,
                                                    BIIB_Required__c = objQueryWork.BIIB_Required__c,
                                                    BIIB_Status__c = System.Label.BIIB_Work_New_Status,
                                                    BIIB_Work_Group__c = objQueryWork.BIIB_Work_Group__c,
                                                    //BIIB_Work_Item_Template__c = objQueryWork.BIIB_Work_Item_Template__c,
                                                    RecordTypeId = objQueryWork.RecordTypeId,
                                                    BIIB_Parent_Disposition_Work__c = objQueryWork.Id,
                                                    Description__c = 'This is a Follow Up Work'
                                                    );
                insert objFollowUp;
                system.debug('-----objFollowUp----'+objFollowUp.Id);
                strGetParam = objFollowUp.id;
                strRedirectId = objFollowUp.id;
                system.debug('----strGetParam----'+strGetParam);
                system.debug('----strRedirectId----'+strRedirectId);
                strWIError = 'CreateNewFollowUpWI';
            }

            if(objQueryWork.BIIB_Reason_Code__c == '' || objQueryWork.BIIB_Reason_Code__c == NULL)
            {
                strWIError = 'Navigate';
            }
        }// End Try
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }// End Catch
    }// End public void CreateFollowUpWorkItem()
    
    /**
    @author Divya Galani
    @description This method creates Follow Up Work Item from PopUp VF page
    @param none
    @return none
    */
    public void CreateFollowUpWorkItemFromPopUp()
    {
        try
        {
            system.debug('----InsideApexClassMethodCreateFollowUpWorkItemFromPopUp-----');
            system.debug('----strWorkItemRowId-----'+strWorkItemRowId);
            objQueryWork = [SELECT Id,BIIB_Status__c,RecordTypeId,Name,BIIB_Reason_Code__c,BIIB_Account__c,BIIB_Assigned_to__c,BIIB_Case__c,BIIB_ININ_ID__c,BIIB_Parent_Disposition_Work__c,BIIB_Preferred_Language__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Region__c,BIIB_Required__c,BIIB_Work_Group__c FROM BIIB_Work__c WHERE Id =:strWorkItemRowId];

            BIIB_Work__c objFollowUp = new BIIB_Work__c(
                                                Name = objQueryWork.Name,
                                                BIIB_Account__c = objQueryWork.BIIB_Account__c,
                                                BIIB_Assigned_to__c = objQueryWork.BIIB_Assigned_to__c,
                                                BIIB_Case__c = objQueryWork.BIIB_Case__c,
                                                BIIB_ININ_ID__c = objQueryWork.BIIB_ININ_ID__c,
                                                BIIB_Preferred_Language__c = objQueryWork.BIIB_Preferred_Language__c,
                                                BIIB_Preferred_Time__c = objQueryWork.BIIB_Preferred_Time__c,
                                                BIIB_Priority__c = objQueryWork.BIIB_Priority__c,
                                                BIIB_Region__c = objQueryWork.BIIB_Region__c,
                                                BIIB_Required__c = objQueryWork.BIIB_Required__c,
                                                BIIB_Status__c = System.Label.BIIB_Work_New_Status,
                                                BIIB_Work_Group__c = objQueryWork.BIIB_Work_Group__c,
                                                //BIIB_Work_Item_Template__c = objQueryWork.BIIB_Work_Item_Template__c,
                                                RecordTypeId = objQueryWork.RecordTypeId,
                                                BIIB_Parent_Disposition_Work__c = objQueryWork.Id,
                                                Description__c = 'This is a Follow Up Work'
                                            );
            insert objFollowUp;
            system.debug('-----objFollowUp----'+objFollowUp.Id);
            strGetParam = objFollowUp.id;
            strRedirectId = objFollowUp.id;
            system.debug('----strGetParam----'+strGetParam);
            system.debug('----strRedirectId----'+strRedirectId);

        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }// End public void CreateFollowUpWorkItemFromPopUp()
    
    /**
    @author Divya Galani
    @description This method creates Escalate To Field Work Item
    @param none
    @return none
    */
    public void CreateEscalateToFieldWI()
    {   
        try
        {
            system.debug('----InsideApexClassMethodCreateEscalateToFieldWI-----');
            system.debug('----strWorkItemRowId-----'+strWorkItemRowId);
            objQueryWork = [SELECT Id,RecordTypeId,Name,BIIB_Reason_Code__c,BIIB_Account__c,BIIB_Assigned_to__c,BIIB_Case__c,BIIB_ININ_ID__c,BIIB_Parent_Disposition_Work__c,BIIB_Preferred_Language__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Region__c,BIIB_Required__c,BIIB_Work_Group__c FROM BIIB_Work__c WHERE Id =:strWorkItemRowId];
            RecordType objEslRecTypeId = [SELECT Id from RecordType where DeveloperName = 'BIIB_Escalate_To_Field'];
            BIIB_Work__c objEsclToField = new BIIB_Work__c(
                                                Name = objQueryWork.Name,
                                                BIIB_Account__c = objQueryWork.BIIB_Account__c,
                                                BIIB_Assigned_to__c = objQueryWork.BIIB_Assigned_to__c,
                                                BIIB_Case__c = objQueryWork.BIIB_Case__c,
                                                BIIB_ININ_ID__c = objQueryWork.BIIB_ININ_ID__c,
                                                BIIB_Preferred_Language__c = objQueryWork.BIIB_Preferred_Language__c,
                                                BIIB_Preferred_Time__c = objQueryWork.BIIB_Preferred_Time__c,
                                                BIIB_Priority__c = objQueryWork.BIIB_Priority__c,
                                                BIIB_Region__c = objQueryWork.BIIB_Region__c,
                                                BIIB_Required__c = objQueryWork.BIIB_Required__c,
                                                BIIB_Status__c = System.Label.BIIB_Work_New_Status,
                                                BIIB_Work_Group__c = objQueryWork.BIIB_Work_Group__c,
                                                //BIIB_Work_Item_Template__c = objQueryWork.BIIB_Work_Item_Template__c,
                                                RecordTypeId = objEslRecTypeId.Id,
                                                BIIB_Parent_Disposition_Work__c = objQueryWork.Id,
                                                Description__c = 'This is an Escalated To Field Work'
                                                );
            insert objEsclToField;
            strGetParam = objEsclToField.id;
            system.debug('----strGetParam----'+strGetParam);
        }// End Try
            
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }// End Catch
    }// End public void CreateEscalateToFieldWI() 
    
}