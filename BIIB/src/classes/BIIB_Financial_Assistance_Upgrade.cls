public class BIIB_Financial_Assistance_Upgrade 
{

    public BIIB_FACApplication__c objFACApp {get;set;}
    String strPatientId = '';
    String strCaseId = '';
    String strWorkItemName = '';
    
    
    
    public BIIB_Financial_Assistance_Upgrade(ApexPages.StandardController controller)
    {
        objFACApp = new BIIB_FACApplication__c();
        Case objCase = (Case)controller.getRecord();
        strPatientId = objCase.AccountId;
        strCaseId = objCase.Id;
    }
    
    public PageReference FACActivities()
    {
        if(strPatientId != null && strPatientId != '')
        {
            Set<String> setCriteria = new Set<String>();
            List<String> lstFASeq = new List<String>();
            objFACApp.BIIB_Patient__c = strPatientId;
            insert objFACApp;
            
            objFACApp = [SELECT BIIB_Is_Copay_Null__c, BIIB_Is_Coupon_Null__c, BIIB_Is_Insurance_Counselling_Null__c FROM BIIB_FACApplication__c WHERE Id=:objFACApp.Id];
            
            //List<BIIB_FA_Calculator__c> lstFASequence = [SELECT Name, BIIB_FA_API_Name__c, BIIB_Sequence__c FROM BIIB_FA_Calculator__c order by  BIIB_Sequence__c];
           
            for(BIIB_FAEligibilityCriteria__c obj : [SELECT BIIB_CriteriaCondition__c,BIIB_Sequence__c, BIIB_CriteriaType__c FROM BIIB_FAEligibilityCriteria__c order by BIIB_Sequence__c])
            {
                if(!setCriteria.contains(obj.BIIB_CriteriaType__c))
                {
                    setCriteria.add(obj.BIIB_CriteriaType__c);
                    lstFASeq.add(obj.BIIB_CriteriaType__c);
                }
            }
            String strEligibleFA = '';
            String strFACAppId = '';
            List<BIIB_FACProgram__c> lstFACProg = new List<BIIB_FACProgram__c>();
            List<SelectOption> options = new List<SelectOption>();
            List<BIIB_FACApplication__c> lstFACApplication = new List<BIIB_FACApplication__c>();
            //Schema.DescribeFieldResult fieldResult = BIIB_FAEligibilityCriteria__c.BIIB_CriteriaType__c.getDescribe();
            //List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            strFACAppId = objFACApp.Id;
            String strFACQuery = 'SELECT Id FROM BIIB_FACApplication__c WHERE Id =:strFACAppId AND ';
            System.debug('********CriteriaList**********'+lstFASeq);
            
            
            for(String f : lstFASeq)
            {
                List<BIIB_FAEligibilityCriteria__c> CriteriaQuerylist = new List<BIIB_FAEligibilityCriteria__c> ();
                CriteriaQuerylist = [SELECT BIIB_CriteriaCondition__c,BIIB_Sequence__c FROM BIIB_FAEligibilityCriteria__c  WHERE BIIB_CriteriaType__c = :f order by BIIB_Sequence__c];
                System.debug('********CriteriaQuery**********' +CriteriaQuerylist);
                Integer intCount = 0;
                //Check Value from Account entity
                for(BIIB_FAEligibilityCriteria__c fae: CriteriaQuerylist)
                {
                    if(f == 'Copay' && objFACApp.BIIB_Is_Copay_Null__c)
                        break;
                        
                    if(f == 'Coupon' && objFACApp.BIIB_Is_Coupon_Null__c)
                        break;
                    
                    if(f == 'Insurance Counseling' && objFACApp.BIIB_Is_Insurance_Counselling_Null__c)
                        break;
                    
                    if(!CriteriaQuerylist.isEmpty() )
                    {
                        String strFACQueryLocal = '';   
                        strFACQueryLocal = strFACQuery + String.valueOf(fae.BIIB_CriteriaCondition__c);
                        lstFACApplication = new List<BIIB_FACApplication__c>();
                        System.debug('********FAC QUERY**********' +strFACQueryLocal);
                        lstFACApplication = Database.query(strFACQueryLocal);
                        if(lstFACApplication.isEmpty())
                        {
                            intCount++;
                        }
                    }
                }
                system.debug('---------intCount----------'+intCount);
                if(intCount == CriteriaQuerylist.size())
                {
                    strEligibleFA = f;
                    break; 
                }
            }
            
            //Create FAC Program records
            for(String f : lstFASeq)
            {
                BIIB_FACProgram__c objFACP;
                if(strEligibleFA != f)
                {
                    objFACP = new BIIB_FACProgram__c(BIIB_Program_Option__c=f, BIIB_Outcome__c='Ineligible', BIIB_Account_FACProgram__c=strPatientId);
                    lstFACProg.add(objFACP);
                }
                else if(strEligibleFA == f)
                {
                    objFACP = new BIIB_FACProgram__c(BIIB_Program_Option__c=f, BIIB_Outcome__c='Eligible', BIIB_Account_FACProgram__c=strPatientId);
                    lstFACProg.add(objFACP);
                                        
                    // Below mentioned is the code for Work Item Creation for this case based on the eligibility
    
                    List<BIIB_SR_Work_Item_Mapping__c> lstWorkItems = [SELECT Name,BIIB_Program__c,BIIB_Work_Item__c FROM BIIB_SR_Work_Item_Mapping__c where BIIB_Program__c =:strEligibleFA];
                    
                    for(BIIB_SR_Work_Item_Mapping__c workItem : lstWorkItems)
                    {
                        strWorkItemName = workItem.BIIB_Work_Item__c;
                        BIIB_Work__c insertWorkItem = new BIIB_Work__c();
                        insertWorkItem.Name =strWorkItemName;
                        insertWorkItem.BIIB_Account__c=strPatientId;
                        insertWorkItem.BIIB_Case__c =strCaseId; 
                        insert insertWorkItem;
                                    
                    }// End For
                    break;
                }// End Else
            }// End for(String f : lstFASeq)
            
            if(lstFACProg.size() > 0)
            {
                insert lstFACProg;
            }
        }// End if(strPatientId != null && strPatientId != '')
        PageReference pageRef = new PageReference('/'+strPatientId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
}