/**
*@author Rancy Fernandes
*@date 08/04/2014
@description Class which contains common methods used across various clasess and trigger
    1. Returns the record type ID based on Object and Record Type Label input parameters
    2. Validates whether the Field API Name exists in the SObject
    3. Validates whether the Object API name entered is valid
    4. Validates the selected Operator and field combination is valid
    5. Return the Map of Record Type Id and Record type Developer Name based on the Object passed as the input parameter
    6. Check to ensure first and last characters are either a number or a closing/opening bracket

------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rancy      08/04/2014  OriginalVersion
*/
global with sharing class BIIB_Utility_Class {

    private static boolean run = true;
    
    public static boolean runOnce()
    {
        if(run)
        {
            run=false;
            return true;
        }
        else
        {
            return run;
        }
    }
    
    // Returns the record type ID based on Object and Record Type Label input parameters
    public static ID getRecordTypeId(String ObjectType, String RecordTypeLabel){   
        SObject OBJ;  
        Id rectypeId =null;
        // Describing Schema  
        Schema.SObjectType Res = Schema.getGlobalDescribe().get(ObjectType);  
        if (Res != null){  
            OBJ = Res.newSObject();  
            // Describing Object   
            Schema.DescribeSObjectResult DesRes = OBJ.getSObjectType().getDescribe();   
            if (DesRes != null){  
                Map<string, schema.recordtypeinfo> RecordTypeMap = DesRes.getRecordTypeInfosByName();  
                system.debug('--RecordTypeMap ' + RecordTypeMap );
                if (RecordTypeMap != null){  
                    Schema.RecordTypeInfo RecordTypeRes = RecordTypeMap.get(RecordTypeLabel);  
                    if (RecordTypeRes != null){  
                       rectypeId = RecordTypeRes.getRecordTypeId();  
                    }  
                }  
            }  
        }  
        system.debug('--rectypeId --' + rectypeId);
        return rectypeId;      
    }
    
    // Validates whether the Field API Name exists in the SObject
    public static boolean validatefieldAPIName(String ObjectType, String FieldAPIName){   
        // Describing Schema  
        Schema.SObjectType objSchema = Schema.getGlobalDescribe().get(ObjectType);
        Map<String, Schema.SObjectField> fieldMap = objSchema.getDescribe().fields.getMap();
        system.debug('--fieldMap --' + fieldMap);
        system.debug('--objSchema --' + objSchema);  
        if(fieldMap.containskey(FieldAPIName)){
            return true;
        }else{
            return false;           
        }
    }
    
    // Validates whether the Object API name entered is valid
    public static boolean validateObjectAPIName(String ObjectType){   
        // Describing Schema  
        Map<String, Schema.SObjectType> gdObjectAPINameMap = Schema.getGlobalDescribe(); 
        system.debug('--gdObjectAPIName --' + gdObjectAPINameMap);
        if(gdObjectAPINameMap.containskey(ObjectType)){
            return true;
        }else{
            return false;           
        }
    }
    
    // Validates the selected Operator and field combination is valid
    public static boolean validateFieldOperator(String strObjectAPIName,String strFieldAPIName,String strOperator)    
    {
          boolean result = false;
          // Describing Object 
          Schema.SObjectType Res = Schema.getGlobalDescribe().get(strObjectAPIName);  
          if (Res != null)
          {  
              // Describing Fields
              Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
              if (fieldMap != null)
              {             
                  // Get field Data Type
                  Schema.DisplayType strFieldDataType=fieldMap.get(strFieldAPIName).getDescribe().getType();
                  String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(strFieldAPIName).getDescribe().getType());
                  System.debug('-----------------FieldDataType--------'+strFieldDataType);
                  System.debug('-----------------strFieldDataType1--------'+strFieldDataTypeConverted);
                  for(BIIB_OperatorValidation__c objOperatorValidation: [SELECT Name,BIIB_Operator_Value__c,BIIB_Field_Data_Type__c FROM BIIB_OperatorValidation__c where BIIB_Field_Data_Type__c =:strFieldDataTypeConverted])
                  {
                    if(strOperator == objOperatorValidation.BIIB_Operator_Value__c)
                    {
                      System.debug('-----------------Operator Match--------');
                      result = true;
                      break;
                    }
                    
                    else
                    {
                      System.debug('-----------------Operator MisMatch--------');
                      result = false;
                    }
                    
                  }
                
               }
        }
        
        return result;

    }
    
    
    /**
     * Method to return map of developer name and recordtype id for object
     * @param sObjectName API name of the object
     * @return Map<String,Id> map of developer name and recordtype id
    **/
    
    public static Map<String,sObject> getMapOfRecordTypeId(String sObjectName){
        Map<String,sObject> mapOfRecordTypeId = new Map<String,sObject>();
            List<RecordType> lstRecordTypeList = [Select 
	            										Id,
	            										Name, 
	            										DeveloperName 
            									From 
            											RecordType 
            									Where 
            											SobjectType =:sObjectName
            									And
            											IsActive = true 
            									LIMIT 
            											100];
            if(!(lstRecordTypeList.isempty())){
                for(RecordType objRecordType:lstRecordTypeList){
                    mapOfRecordTypeId.put(objRecordType.DeveloperName, objRecordType);
                }
            }
            return mapOfRecordTypeId;
        }
     
     
     /**
     * Method to return map of developer name and recordtype id for object
     * @param sObjectName API name of the object
     * @return Map<String,Id> map of developer name and recordtype id
    **/
    
    public static Map<Id,sObject> getMapOfRecordTypeDeveloperName(String sObjectName){
        Map<Id,sObject> mapOfRecordTypeDeveloperName = new Map<Id,sObject>();
            List<RecordType> lstRecordTypeList = [Select 
	            										Id,
	            										Name, 
	            										DeveloperName 
            									From 
            											RecordType 
            									Where 
            											SobjectType =:sObjectName
            									And
            											IsActive = true 
            									LIMIT 
            											100];
            if(!(lstRecordTypeList.isempty())){
                for(RecordType objRecordType:lstRecordTypeList){
                    mapOfRecordTypeDeveloperName.put(objRecordType.Id, objRecordType);
                }
            }
            return mapOfRecordTypeDeveloperName;
        }
        
    
    
    // Return the Map of Record Type Id and Record type Developer Name based on the Object passed as the input parameter
    public static Map<Id,String> caseRTDevName (String objectName){
        List<RecordType> rtList = new List<RecordType>();
        Map<Id,String> recordTypeMap = new Map<Id,String>();
        
        String strQuery = 'Select Id, DeveloperName from RecordType  Where SobjectType =\'' + objectName +'\'';
        rtList=database.query(strQuery);
        for(RecordType r : rtList){
            recordTypeMap.put(r.Id,r.DeveloperName);
        }
        return recordTypeMap;
    }
    
    //Check to ensure first and last characters are either a number or a closing/opening bracket
    public static boolean validateFirstLastChars(String strRuleLogic){
        strRuleLogic = strRuleLogic.replace('(','&').toUpperCase();
        strRuleLogic = strRuleLogic.replace(')','*').toUpperCase();
        return true;
        
    }
    
    webservice static String createFollowUpActivity(String str_WorkActivity){
        
        String str_FollowUpActivityId ='ERROR';
        
      try{
          // Query for the existing Work Details
          for(BIIB_Work__c objWork : [Select 
                          Id,
                          BIIB_Account__c,
                          BIIB_Business_Rules_Configuration__c,
                          BIIB_Case__c,
                          BIIB_Parent_Disposition_Work__c,
                          BIIB_Parent_Work__c,
                          BIIB_Work_Configuration__c,
                          BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Priority__c,
                          BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Route_Date__c,
                          BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Record_Type__c,
                          BIIB_Assigned_to__c,
                          BIIB_CanRoute__c,
                          BIIB_Due_Date__c,
                          BIIB_Follow_Up_Activity_Created__c,
                          BIIB_Priority__c,
                          BIIB_Reason_Code__c,
                          BIIB_Region__c,
                          BIIB_Required__c,
                          BIIB_Result__c,
                          BIIB_Work_Sub_Type__c,
                          BIIB_Work_Type__c 
                      From 
                          BIIB_Work__c 
                      Where 
                          Id=:str_WorkActivity]){
          
            // Add a code Savepoint
            
              BIIB_Work__c objNewWork = new BIIB_Work__c();
              
              objNewWork.BIIB_Account__c =objWork.BIIB_Account__c;
              objNewWork.BIIB_CanRoute__c = false;
              objNewWork.BIIB_Case__c = objWork.BIIB_Case__c;
              objNewWork.BIIB_Status__c='New';
              objNewWork.BIIB_Parent_Work__c=objWork.Id;
              objNewWork.Name='Follow-Up Activity'; 
              objNewWork.BIIB_Required__c = false;
              objNewWork.BIIB_Work_Configuration__c   = objWork.BIIB_Work_Configuration__c;  
              objNewWork.BIIB_Priority__c = objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Priority__c;
              objNewWork.BIIB_Business_Rules_Configuration__c = objWork.BIIB_Business_Rules_Configuration__c;
              objNewWork.BIIB_Is_Follow_Up_Activity__c = true;
            
             if(objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Record_Type__c!=null){
             	Map<String,sObject> mMap_RecordType = getMapOfRecordTypeId('BIIB_Work__c');
              	System.debug('Configured FollowUp Activity RecordType--->'+objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Record_Type__c+'Map-->'+mMap_RecordType+'----'+mMap_RecordType.get(objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Record_Type__c));
             	if(mMap_RecordType.get(objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Record_Type__c)!=null){
              		sObject oObject = mMap_RecordType.get(objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Record_Type__c);
              		objNewWork.RecordTypeId = String.valueOf(oObject.get('Id'));
              	}
            }
            
            if(objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Route_Date__c!=null){
              objNewWork.BIIB_Due_Date__c = objNewWork.BIIB_Route_Date__c = System.today().addDays(Integer.valueOf(objWork.BIIB_Work_Configuration__r.BIIB_Follow_Up_Activity_Route_Date__c)+1);
            }
            else{
              objNewWork.BIIB_Due_Date__c = objNewWork.BIIB_Route_Date__c = System.today();
            }
            objNewWork.BIIB_Work_Type__c  = 'Follow-Up Activity';
            //objNewWork.BIIB_Work_Sub_Type__c = objWork.BIIB_Work_Sub_Type__c;
            
            Database.SaveResult db_SaveResult = Database.insert(objNewWork , false);  
              
              if (db_SaveResult.isSuccess()) {
                str_FollowUpActivityId = db_SaveResult.getId();
                objWork.BIIB_Follow_Up_Activity_Created__c = true;
                objWork.BIIB_Result__c='Call Back Request';
                
                update objWork;  
              }
        }
      }
      catch(Exception e){
        System.debug('Exception e-->'+e.getMessage());
      }
      
      return str_FollowUpActivityId;
    }
    // Wrapper class for business day calculations
    public class BusinessDays {
 
    private List<Boolean> businessDay = new Boolean[7];
    private Date knownSunday = date.newInstance(2014, 1, 5);
 
    // Constructor creates businessDay array
    public BusinessDays(String businessHourId){
        BusinessHours bh = [SELECT FridayStartTime,MondayStartTime,SaturdayStartTime,SundayStartTime,
							ThursdayStartTime,TuesdayStartTime,WednesdayStartTime FROM BusinessHours WHERE id =: businessHourId];
        businessDay[0] = (bh.SundayStartTime != null);
        businessDay[1] = (bh.MondayStartTime != null);
        businessDay[2] = (bh.TuesdayStartTime != null);
        businessDay[3] = (bh.WednesdayStartTime != null);
        businessDay[4] = (bh.ThursdayStartTime != null);
        businessDay[5] = (bh.FridayStartTime != null);
        businessDay[6] = (bh.SaturdayStartTime != null);
    }
 
    // Returns back the next available business day
    public Date nextBusinessDay(Date d){
        integer i = Math.mod(this.knownSunday.daysBetween(d),7);
 
        Date returnDate = d;
 
        do {
            returnDate = returnDate.addDays(1);
            i++;
        } while (!businessDay[Math.mod(i, 7)]);
 
        return returnDate;
    }
 
    // returns back date in numberOfDays business days
    public Date addBusinessDays (Date startDate, integer numberOfDays){
        Date returnDate = startDate;
 
        for (integer x = 0; x < numberOfDays; x++)
            returnDate = nextBusinessDay(returnDate);
 
        return returnDate;
    }
}
    
    
}