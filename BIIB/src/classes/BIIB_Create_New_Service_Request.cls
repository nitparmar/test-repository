public class BIIB_Create_New_Service_Request{
    
    public String strCustomerType {get;private set;}
    public Boolean bool_IsProductEnabled {get;private set;}
    
    public List<SelectOption> lstProductOptions {get;set;}
    public List<SelectOption> lstServiceRequestOptions {get;set;}
    public List<WorkItemConfiguration> lstWorkItemConfigurations {get;set;}
    
    public Map<Id,Product_vod__c> mMap_ProductGroupById ;
    public Map<String,Product_vod__c> mMap_ProductGroupByName ; 
    public Map<Id,Id> mMap_Product_PatientTherapy; 
    public Set<String> setProduct;
    public String strAccountId {get;set;}
    public String strSelectedProduct{get;set;}
    public String strSelectedServiceRequest{get;set;}
    public String strValidationError {get;set;}
    public String strAction {get;set;}
    public String strPrimaryHCP {get;set;}
    public String strPrimaryPatient {get;set;}
    public Integer iListIndex {get;set;}
    public Boolean bool_ShowImage {get;set;}
    public Boolean bool_CreationViolated {get;set;}
    
    public Map<String,BIIB_Business_Rules_Configuration__c> mapServiceRequestNames = new Map<String,BIIB_Business_Rules_Configuration__c>();
    public Map<String,Id> map_Affiliations = new Map<String,Id>();
    public Map<Id,BIIB_Affiliation__c> map_AffiliationsAccountDetails {get;set;}
    public Map<Id,Account> map_AccountDetails{get;set;}
                                
    public BIIB_Create_New_Service_Request(){
        
        lstProductOptions = new List<SelectOption>();
        lstServiceRequestOptions= new List<SelectOption>();
        lstWorkItemConfigurations = new List<WorkItemConfiguration>();
        
        mMap_ProductGroupById = new Map<Id,Product_vod__c>();
        mMap_ProductGroupByName = new Map<String,Product_vod__c>();
        map_AffiliationsAccountDetails = new Map<Id,BIIB_Affiliation__c>();
        map_AccountDetails = new Map<Id,Account>();
        mMap_Product_PatientTherapy = new Map<Id,Id>();
        
        setProduct = new Set<String>();
        
        bool_IsProductEnabled = false;
        bool_ShowImage = false;
        bool_CreationViolated = false;
        
        strCustomerType = ApexPages.currentPage().getParameters().get('CustomerType');
        strAccountId = ApexPages.currentPage().getParameters().get('Id');
        
        map_Affiliations.put(strCustomerType,Id.valueOf(strAccountId));
        
        //Query Product Catalog for Therapies 
        
        for(Product_vod__c objProductCatalog : [Select 
                                                        Id,
                                                        Name
                                                  From 
                                                        Product_vod__c
                                                  Where
                                                        Product_Type_vod__c='Product Group']){
        
            mMap_ProductGroupById.put(objProductCatalog.Id,objProductCatalog);
            mMap_ProductGroupByName.put(objProductCatalog.Name,objProductCatalog);
        }
        
        // if the Customer Type is Patient
        if(strCustomerType=='Patient'){
            bool_IsProductEnabled = true;
            Integer iPatientTherapies = 0;
            for(BIIB_Patient_Therapy__c objPatientTherapy : [Select 
                                                            Id,
                                                            BIIB_Therapy__c,
                                                            BIIB_Therapy__r.Name,
                                                            BIIB_Primary__c 
                                                    From 
                                                            BIIB_Patient_Therapy__c 
                                                    Where 
                                                            BIIB_Patient__c=:strAccountId
                                                    and     
                                                            (BIIB_Primary__c = true
                                                    OR     
                                                            BIIB_Active__c=true)]){
                
                System.debug('Patient Therapy--->'+objPatientTherapy);
                mMap_Product_PatientTherapy.put(objPatientTherapy.BIIB_Therapy__c,objPatientTherapy.Id);
                if(objPatientTherapy.BIIB_Primary__c){
                    strSelectedProduct= objPatientTherapy.BIIB_Therapy__r.Name;
                    setProduct.add(strSelectedProduct);
                }                                               
            }
        }
        System.debug('Selected Product-->'+strSelectedProduct);
        // Adding a default value
        
        lstServiceRequestOptions.add(new SelectOption('0','-Select SR-'));
        
        if(strSelectedProduct!=null){
            getProductList();
            getServiceRequests();   
        }
        else{
            lstProductOptions.add(new SelectOption('0','-Select Product-'));
            getProductList();
        }
        
    }
    public List<SelectOption> getProductList(){
        try{
            
            if(lstServiceRequestOptions!=null){
                lstServiceRequestOptions.clear();
                lstServiceRequestOptions.add(new SelectOption('0','-Select SR-'));
            }
            bool_ShowImage = false;
            strValidationError ='';
            
            
            for (BIIB_Business_Rules_Configuration__c objProductList : [Select 
                                                                                Id,
                                                                                BIIB_Product__c
                                                                          From
                                                                                BIIB_Business_Rules_Configuration__c
                                                                          Where 
                                                                                BIIB_Customer_Type__c=:strCustomerType]){
                    if(objProductList.BIIB_Product__c!=null){
                        String[] arrProduct = String.valueOf(objProductList.BIIB_Product__c).split(';');
                            for(String strProduct : arrProduct){
                                if(!(setProduct.contains(strProduct))){
                                    setProduct.add(strProduct);
                                }           
                            }
                    }
            }
            
            if(!(setProduct.isempty())){
                for(String strProduct : setProduct){
                    lstProductOptions.add(new SelectOption(strProduct,strProduct));
                }       
            }
            return lstProductOptions;
        }
        catch(Exception e){
            System.debug('Exception e-->'+e.getMessage());
            return null;
        }       
    }
    
    public List<SelectOption> getServiceRequests(){
        try{
            
            if(lstServiceRequestOptions!=null){
                lstServiceRequestOptions.clear();
                lstServiceRequestOptions.add(new SelectOption('0','-Select SR-'));
            }
            
            if(strSelectedProduct!=null){
                    for (BIIB_Business_Rules_Configuration__c objProductList : [Select 
                                                                                Id,
                                                                                Name,
                                                                                BIIB_Allow_more_than_one_active__c,
                                                                                BIIB_Validation_Error__c,
                                                                                BIIB_SR_Record_Type__c
                                                                          From
                                                                                BIIB_Business_Rules_Configuration__c
                                                                          Where 
                                                                                BIIB_Product__c INCLUDES (:strSelectedProduct)
                                                                          Order By 
                                                                                Name]){
                    if(objProductList.Name!=null){
                        mapServiceRequestNames.put(objProductList.Id,objProductList);
                        lstServiceRequestOptions.add(new SelectOption(objProductList.Id,objProductList.Name));
                    }
                }   
            }
            return lstServiceRequestOptions;
        }
        catch(Exception e){
            System.debug('Exception e-->'+e.getMessage());
            return null;
        }
    }
    
    public List<WorkItemConfiguration> getWorkItemConfiguration(){
        try{
          // Affiliations to Primary Account
          
            for(BIIB_Affiliation__c objAffiliation : [Select 
                                                             Id,
                                                             BIIB_To_Account__c,
                                                             BIIB_From_Account__c,
                                                             BIIB_To_Account_Type__c,
                                                             BIIB_To_Account__r.PersonEmail,
                                                             BIIB_To_Account__r.BIIB_Email_2__c,
                                                             BIIB_To_Account__r.Phone,
                                                             BIIB_To_Account__r.BIIB_Phone_2__c,
                                                             BIIB_To_Account__r.BIIB_Phone_3__c,
                                                             BIIB_To_Account__r.BIIB_Preferred_Language__c 
                                                        From 
                                                            BIIB_Affiliation__c 
                                                        Where 
                                                            BIIB_From_Account__c=:Apexpages.currentPage().getParameters().get('Id')
                                                        and BIIB_Primary__c = true]){
                    //strPrimaryPatient = objAffiliation.BIIB_From_Account__c;
                    //strPrimaryHCP = objAffiliation.BIIB_To_Account__c;
                    
                    map_Affiliations.put(objAffiliation.BIIB_To_Account_Type__c,objAffiliation.BIIB_To_Account__c);
                    map_AffiliationsAccountDetails.put(objAffiliation.BIIB_To_Account__c,objAffiliation); 
                                                    
            }
            
            // Get Details of the Primary Account
            
            for(Account objAccount : [Select 
                               Id,
                               PersonEmail,
                                               BIIB_Email_2__c,
                                               Phone,
                                               BIIB_Phone_2__c,
                                               BIIB_Phone_3__c,
                                               BIIB_Preferred_Language__c
                                     From
                                            Account
                                     Where
                                            Id=:ApexPages.currentPage().getParameters().get('Id')]){
              map_AccountDetails.put(objAccount.Id,objAccount);
          }
            
            if(lstWorkItemConfigurations!=null){
                lstWorkItemConfigurations.clear();
            }
            
            if(strSelectedServiceRequest!=null){
                    Integer iIndex=0;
                    for (BIIB_Work_Configuration__c objWorkItems : [Select 
                                                                                Id,
                                                                                Name,
                                                                                BIIB_Business_Rules_Configuration__c,
                                                                                BIIB_Priority__c,
                                                                                BIIB_Description__c,
                                                                                BIIB_Required__c,
                                                                                BIIB_Route_Date__c,
                                                                                BIIB_Work_Group__c,
                                                                                BIIB_Work_Sub_Type__c,
                                                                                BIIB_Work_Type__c,
                                                                                BIIB_Point_of_Contact__c,
                                                                                BIIB_Related_To__c,
                                                                                BIIB_Entrance_Criteria__c,
                                                                                BIIB_Entrance_Criteria_Message__c,
                                                                                BIIB_Work_Record_Type__c
                                                                          From
                                                                                BIIB_Work_Configuration__c
                                                                          Where 
                                                                                BIIB_Business_Rules_Configuration__c =:strSelectedServiceRequest
                                                                          and
                                                                                Id Not In (Select 
                                                                                					BIIB_Work_Configuration__c 
                                                                                			From 
                                                                                					BIIB_Work_Configuration_Dependencies__c 
                                                                                			Where 
                                                                                					BIIB_Predecessor__r.BIIB_Business_Rules_Configuration__c=:strSelectedServiceRequest)
                                                                          Order By
                                                                                BIIB_Required__c Desc]){
                        
                        
                        
                        
                        //Check for Work Item Related Entrance Criteria
                        
                        if(objWorkItems.BIIB_Entrance_Criteria__c!=null){
                            
                            System.debug('Query--->Select '+
                                                                                'Id '+ 
                                                                       'From '+ 
                                                                                'Account ' +
                                                                       'Where '+ 
                                                                                'Id= '+'\''+ApexPages.currentPage().getParameters().get('Id')+'\'' + 
                                                                       ' And ('+ objWorkItems.BIIB_Entrance_Criteria__c +')');
                            
                            List<Account> lstAccount = Database.query('Select '+
                                                                                'Id '+ 
                                                                       'From '+ 
                                                                                'Account ' +
                                                                       ' Where '+ 
                                                                                'Id= '+'\''+ApexPages.currentPage().getParameters().get('Id')+'\'' + 
                                                                       ' And ('+ objWorkItems.BIIB_Entrance_Criteria__c +')');
                            
                            if(lstAccount.isempty()){
                                System.debug('Donot create.....');
                                lstWorkItemConfigurations.add(new WorkItemConfiguration(iIndex,objWorkItems.BIIB_Required__c,(!objWorkItems.BIIB_Required__c),true,false,objWorkItems));        
                            }
                            
                            else{
                                    if(objWorkItems.BIIB_Point_of_Contact__c!=null && String.valueOf(objWorkItems.BIIB_Point_of_Contact__c).trim()!=''){
                                        if(map_Affiliations.get(objWorkItems.BIIB_Point_of_Contact__c)!=null){
                                            objWorkItems.BIIB_Related_To__c=map_Affiliations.get(objWorkItems.BIIB_Point_of_Contact__c);
                                        }
                                    }
                                    lstWorkItemConfigurations.add(new WorkItemConfiguration(iIndex,objWorkItems.BIIB_Required__c,(!objWorkItems.BIIB_Required__c),false,true,objWorkItems));
                            }
                            
                            
                            
                            //iIndex++;
                        }
                            
                        else{
                                if(objWorkItems.BIIB_Point_of_Contact__c!=null && String.valueOf(objWorkItems.BIIB_Point_of_Contact__c).trim()!=''){
                                        if(map_Affiliations.get(objWorkItems.BIIB_Point_of_Contact__c)!=null){
                                            objWorkItems.BIIB_Related_To__c=map_Affiliations.get(objWorkItems.BIIB_Point_of_Contact__c);
                                        }
                                    }
                        
                            lstWorkItemConfigurations.add(new WorkItemConfiguration(iIndex,objWorkItems.BIIB_Required__c,(!objWorkItems.BIIB_Required__c),false,true,objWorkItems));
                            //iIndex++;
                        }
                        
                        iIndex++;   
                }   
            }
            return lstWorkItemConfigurations;
        }
        catch(Exception e){
            System.debug('Exception e-->'+e.getMessage());
            return null;
        }
    }
    
    public List<WorkItemConfiguration> getAddRemoveConfiguration(){
        try{
            
            if(strAction!=null && iListIndex!=null){
                for(Integer i=0;i<lstWorkItemConfigurations.size();i++){
                    if(lstWorkItemConfigurations[i].iIndex==iListIndex){
                        if(strAction=='Remove'){
                            lstWorkItemConfigurations[i].bConfigureWorkItem = false;
                        }
                        else if(strAction=='Add'){
                            lstWorkItemConfigurations[i].bConfigureWorkItem = true;
                        }
                    }
                }
            }
            return lstWorkItemConfigurations;
        }
        catch(Exception e){
            System.debug('Exception e-->'+e.getMessage());
            return null;
        }
    }
    
    public PageReference SelectProduct(){
        strSelectedProduct = ApexPages.currentPage().getParameters().get('ProductName');
        getServiceRequests();
        return null;
    }
     public PageReference SelectServiceRequest(){
        strSelectedServiceRequest = ApexPages.currentPage().getParameters().get('ServiceRequestTemplate');
        strValidationError=null;
        if(lstWorkItemConfigurations!=null){
                lstWorkItemConfigurations.clear();
            }
            
        // Checking for Validations & Duplicate SR's creation
        if(strSelectedServiceRequest!=null){
            //Preventing duplicate SR Creation if multiple SR's of same type are not allowed
            if(!(mapServiceRequestNames.get(strSelectedServiceRequest).BIIB_Allow_more_than_one_active__c)){
                
                String strSelectedTherapy = mMap_ProductGroupByName.get(strSelectedProduct).Id;
                System.debug('strSelectedTherapy-->'+strSelectedTherapy+'---'+strSelectedServiceRequest+'----'+strAccountId);
                for(Case objCase : [Select 
                                            Id,
                                            CaseNumber,
                                            RecordType.DeveloperName 
                                    From 
                                            Case 
                                    Where 
                                            IsClosed = false
                                    and     
                                            BIIB_Business_Rules_Configuration__c=:strSelectedServiceRequest 
                                    and     
                                            BIIB_Therapy__c=:strSelectedTherapy
                                    and
                                        AccountId=:ApexPages.currentPage().getParameters().get('Id')
                                    Limit 1
                                            ]){
                
                    strValidationError = 'Service Request <a href="/ui/support/servicedesk/ServiceDeskPage#/'+objCase.Id+'" target="_blank">'+objCase.CaseNumber+'</a> is already open.';    
                    bool_CreationViolated = true;
                    bool_ShowImage = true;                          
                }           
            }
            
            // Checking for the validations before creating the SR.
            
            if(!(bool_CreationViolated)){
              for(BIIB_Business_Rules_Validations__c objRuleValidation : [Select 
                                                Id,
                                                BIIB_Active__c,
                                                BIIB_Business_Rules_Configuration__c,
                                                BIIB_Description__c,
                                                BIIB_Validation_Error__c,
                                                BIIB_Validation_Query__c
                                            From
                                                BIIB_Business_Rules_Validations__c
                                            Where
                                                BIIB_Business_Rules_Configuration__c = :strSelectedServiceRequest
                                            And
                                                BIIB_Active__c = true]){
                List<Account> lstAccount = Database.query('Select '+
                                                                        'Id '+ 
                                                               'From '+ 
                                                                        'Account ' +
                                                               ' Where '+ 
                                                                       'Id= '+'\''+ApexPages.currentPage().getParameters().get('Id')+'\'' + 
                                                               ' And ('+ objRuleValidation.BIIB_Validation_Query__c +')');
                
                if(lstAccount.isempty()){
                  strValidationError  = objRuleValidation.BIIB_Validation_Error__c;
                  bool_CreationViolated = true;
                  bool_ShowImage = true;
                  break;      
                }
              }
            }
            
            
            
            /*if(mapServiceRequestNames.get(strSelectedServiceRequest).BIIB_Validation_Error__c!=null){
                strValidationError  = mapServiceRequestNames.get(strSelectedServiceRequest).BIIB_Validation_Error__c;
                bool_ShowImage = true;      
            }*/
            if(!(bool_CreationViolated)){
                getWorkItemConfiguration();
                bool_ShowImage = false;
            }
        }
        return null;
    }
    public PageReference SelectAddRemoveConfiguration(){
        strAction = ApexPages.currentPage().getParameters().get('ServiceAction');
        iListIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('ServiceIndex'));
        getAddRemoveConfiguration();
        return null;
    }
    public PageReference SaveCreateSR_WorkItems(){
        try
            {
                //Create a Map of the Case RecordType
                
                Map<String,sObject> mMap_CaseRecordTypes = new Map<String,sObject>();
                Map<String,sObject> mMap_WorkRecordTypes = new Map<String,sObject>();
                
                /*  Using the utility class function to return the Key - Value pair 
                    Key - RecordType.DeveloperName
                    Value - RecordType 
                */
                 
                mMap_CaseRecordTypes = BIIB_Utility_Class.getMapOfRecordTypeId('Case');
                mMap_WorkRecordTypes = BIIB_Utility_Class.getMapOfRecordTypeId('BIIB_Work__c');
                
                Case objCase = new Case();
                objCase.AccountId = ApexPages.currentPage().getParameters().get('Id');
                objCase.Status = 'New';
                objCase.BIIB_SR_Type__c=mapServiceRequestNames.get(strSelectedServiceRequest).Name;
                objCase.BIIB_Business_Rules_Configuration__c = strSelectedServiceRequest;
                objCase.BIIB_Therapy__c=mMap_ProductGroupByName.get(strSelectedProduct).Id;
                objCase.BIIB_Patient_Therapy__c = mMap_Product_PatientTherapy.get( objCase.BIIB_Therapy__c);
                if(!(mMap_CaseRecordTypes.isempty())){
                    if(mMap_CaseRecordTypes.get(mapServiceRequestNames.get(strSelectedServiceRequest).BIIB_SR_Record_Type__c)!=null){
                        sObject objRecordType = mMap_CaseRecordTypes.get(mapServiceRequestNames.get(strSelectedServiceRequest).BIIB_SR_Record_Type__c);
                        objCase.RecordTypeId = String.valueOf(objRecordType.get('Id'));
                    }
                }
                else{
                    objCase.RecordTypeId='012110000000GVZ';
                }
                insert objCase;
                
                if(!(lstWorkItemConfigurations.isempty())){
                    BIIB_Work__c[] arrayNewWorkItems = new BIIB_Work__c[]{};
                        for(Integer i=0;i<lstWorkItemConfigurations.size();i++){
                            if(/*lstWorkItemConfigurations[i].bRequiredWorkItem || lstWorkItemConfigurations[i].bConfigureWorkItem*/ lstWorkItemConfigurations[i].bCanCreateWorkItem){
                                BIIB_Work__c obj_Work = new BIIB_Work__c();
                                BIIB_Work_Configuration__c objWorkAssignment = lstWorkItemConfigurations[i].objWorkConfiguration;
                                System.debug('Work Assignment -->'+objWorkAssignment);
                                if(objWorkAssignment.BIIB_Point_of_Contact__c!=null && objWorkAssignment.BIIB_Related_To__c!=null){
                                    obj_Work.BIIB_Account__c = objWorkAssignment.BIIB_Related_To__c;
                                    
                                    if(map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c)!=null){
                                      obj_Work.BIIB_Patient_Email__c = map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_To_Account__r.PersonEmail;
                                      obj_Work.BIIB_Email_2__c = map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_To_Account__r.BIIB_Email_2__c;
                                      obj_Work.BIIB_Preferred_Language__c = map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_To_Account__r.BIIB_Preferred_Language__c;
                                      obj_Work.Phone__c = map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_To_Account__r.Phone;
                                      obj_Work.BIIB_Phone_2__c = map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_To_Account__r.BIIB_Phone_2__c;
                                      obj_Work.BIIB_Phone_3__c = map_AffiliationsAccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_To_Account__r.BIIB_Phone_3__c;
                                    }
                                    
                                    // Check the Primary Account
                                    
                                     if(map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c)!=null){
                                      obj_Work.BIIB_Patient_Email__c = map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c).PersonEmail;
                                      obj_Work.BIIB_Email_2__c = map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_Email_2__c;
                                      obj_Work.BIIB_Preferred_Language__c = map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_Preferred_Language__c;
                                      obj_Work.Phone__c = map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c).Phone;
                                      obj_Work.BIIB_Phone_2__c = map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_Phone_2__c;
                                      obj_Work.BIIB_Phone_3__c = map_AccountDetails.get(objWorkAssignment.BIIB_Related_To__c).BIIB_Phone_3__c;
                                    }
                                    
                                }
                                else{
                                    obj_Work.BIIB_Account__c = ApexPages.currentPage().getParameters().get('Id'); 
                                    obj_Work.BIIB_Patient_Email__c = map_AccountDetails.get(ApexPages.currentPage().getParameters().get('Id')).PersonEmail;
                                  obj_Work.BIIB_Email_2__c = map_AccountDetails.get(ApexPages.currentPage().getParameters().get('Id')).BIIB_Email_2__c;
                                  obj_Work.BIIB_Preferred_Language__c = map_AccountDetails.get(ApexPages.currentPage().getParameters().get('Id')).BIIB_Preferred_Language__c;
                                  obj_Work.Phone__c = map_AccountDetails.get(ApexPages.currentPage().getParameters().get('Id')).Phone;
                                  obj_Work.BIIB_Phone_2__c = map_AccountDetails.get(ApexPages.currentPage().getParameters().get('Id')).BIIB_Phone_2__c;
                                  obj_Work.BIIB_Phone_3__c = map_AccountDetails.get(ApexPages.currentPage().getParameters().get('Id')).BIIB_Phone_3__c;
                                }
                                obj_Work.BIIB_CanRoute__c = true;
                                obj_Work.BIIB_Case__c = objCase.Id;
                                obj_Work.BIIB_Status__c='Open';
                                System.debug('RecordType-->'+objWorkAssignment.BIIB_Work_Record_Type__c+'--->'+mMap_WorkRecordTypes.get(objWorkAssignment.BIIB_Work_Record_Type__c));
                                 if(mMap_WorkRecordTypes.get(objWorkAssignment.BIIB_Work_Record_Type__c)!=null){
                                    sObject objRecordType = mMap_WorkRecordTypes.get(objWorkAssignment.BIIB_Work_Record_Type__c);
                                    obj_Work.RecordTypeId = String.valueOf(objRecordType.get('Id'));
                                }
                                obj_Work.Name=objWorkAssignment.BIIB_Description__c;
                                obj_Work.BIIB_Required__c = objWorkAssignment.BIIB_Required__c;
                                obj_Work.BIIB_Work_Configuration__c  = objWorkAssignment.Id;    
                                obj_Work.BIIB_Priority__c = objWorkAssignment.BIIB_Priority__c;
                                obj_Work.BIIB_Business_Rules_Configuration__c = objWorkAssignment.BIIB_Business_Rules_Configuration__c;
                                if(objWorkAssignment.BIIB_Route_Date__c!=null){
                                    obj_Work.BIIB_Due_Date__c = obj_Work.BIIB_Route_Date__c = System.today().addDays(Integer.valueOf(objWorkAssignment.BIIB_Route_Date__c)+1);
                                }
                                else{
                                    obj_Work.BIIB_Due_Date__c = obj_Work.BIIB_Route_Date__c = System.today();
                                }
                                obj_Work.BIIB_Work_Type__c  = objWorkAssignment.BIIB_Work_Type__c;
                                obj_Work.BIIB_Work_Sub_Type__c = objWorkAssignment.BIIB_Work_Sub_Type__c;
                                
                                arrayNewWorkItems.add(obj_Work);
                            }
                        }
                        if((!arrayNewWorkItems.isempty())){
                            insert arrayNewWorkItems;
                        }
                }
        }
        catch(Exception e){
            System.debug('Exception e-->'+e.getMessage());  
        }
        return null;
    }
    
    public class WorkItemConfiguration{
        
        public Boolean bRequiredWorkItem {get;set;}
        public Boolean bConfigureWorkItem {get;set;}
        public Boolean bEntranceCriteria {get;set;}
        public Boolean bCanCreateWorkItem {get;set;}
        public Integer iIndex {get;set;}
        public BIIB_Work_Configuration__c objWorkConfiguration {get;set;}
        
        public WorkItemConfiguration(Integer i_Index , Boolean b_RequiredWorkItem,Boolean b_ConfigureWorkItem,Boolean b_EntranceCriteria,Boolean b_CanCreateWorkItem,BIIB_Work_Configuration__c obj_WorkConfiguration){
                iIndex = i_Index;
                bRequiredWorkItem = b_RequiredWorkItem;
                bConfigureWorkItem = b_ConfigureWorkItem;
                bEntranceCriteria = b_EntranceCriteria;
                bCanCreateWorkItem = b_CanCreateWorkItem;
                objWorkConfiguration = obj_WorkConfiguration;
       }
    }
    
}