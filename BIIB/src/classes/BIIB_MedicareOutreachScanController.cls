public class BIIB_MedicareOutreachScanController
{
   public String returnURL {get;set;}

    public void scanPatients()
    {
        String query  = 'SELECT Id FROM Account WHERE RecordTypeId=\''+String.escapeSingleQuotes('012110000000BBl')+'\' AND IsPersonAccount=true AND BIIB_Age__c > 65';
        BIIB_Medicare_OutReach_Batch obj = new BIIB_Medicare_OutReach_Batch(query); 
        database.executebatch(obj,35);
    }

   public BIIB_MedicareOutreachScanController(ApexPages.StandardSetController sc)
    {        
        returnURL =  Account.SObjectType.getDescribe().getKeyPrefix();
    }

}