public class BIIB_PREGenericClass
{

    Map<String, List<sObject>> preObjListMap = new Map<String, List<sObject>>();
    Map<String, Set<String>> preFieldListForQuery = new Map<String, Set<String>>();
    Set<Id> setEvalCriteriaId = new Set<Id>();
    List<BIIB_Field_Criteria__c> lstfieldCriteria = new List<BIIB_Field_Criteria__c>();
    List<BIIB_Evaluation_Criteria__c> lstevaluationCriteria = new List<BIIB_Evaluation_Criteria__c>();
    List<BIIB_Evaluation_Criteria__c > lstEC = new List<BIIB_Evaluation_Criteria__c >();
    public String strEvalCriteriaFinal {get;set;}
    public String strContinueEvaluation {get;set;}
    public String strProcessFlowRowId {get;set;}
    public String strTempProcessFlowRowId {get;set;}
    public String strInvokePRE {get;set;}
    String strAccountFields = ''; 
    String strAccountId = '';
    String strEvalCriteriaId = '';
    String strObjectName = '';
    String strReplacedOr = '';
    String strReplacedAnd = '';
    String strReplacedTrue = '';
    String strReplacedFalse = '';
    String strProcessFlowId = '';
    
    
    public BIIB_PREGenericClass(ApexPages.StandardController controller) 
    {
        Account objAccount =(Account)controller.getRecord();
        strAccountId = objAccount.Id;
        strObjectName = 'Account';
    }
    
    public void FetchEvalCriteria()
    {
        strContinueEvaluation = 'true';
        // Query All Evaluation Criteria where Record Type is PRE-LIMIT 1
        Set<BIIB_Evaluation_Criteria__c> setAllPREEvalCriteria = new Set<BIIB_Evaluation_Criteria__c>([SELECT Id from BIIB_Evaluation_Criteria__c where RecordType.Name = 'PRE' AND Id NOT IN:setEvalCriteriaId LIMIT 1 ]);
        strEvalCriteriaFinal = '';
        
        // Put all the field criteria related to this evaluation criteria in a map
        Map<String, List<BIIB_Field_Criteria__c>> preFieldCriteria = new Map<String, List<BIIB_Field_Criteria__c>>();  
        if(!setAllPREEvalCriteria.isEmpty())
        {
              
            for(BIIB_Field_Criteria__c objFieldCriteria: [SELECT BIIB_Sequence__c,BIIB_Operator__c, BIIB_Object_API_Name__c,BIIB_Field_API_Name__c,BIIB_Field_Value__c,BIIB_Evaluation_Criteria__c FROM BIIB_Field_Criteria__c WHERE BIIB_Evaluation_Criteria__c NOT IN:setEvalCriteriaId AND BIIB_Evaluation_Criteria__c IN:setAllPREEvalCriteria])
            {           
                // Get All the fields that need to be queried in the SOQL from the field criteria for all evaluation criteria related to PRE. 
                setEvalCriteriaId.add(objFieldCriteria.BIIB_Evaluation_Criteria__c);
                strEvalCriteriaId = objFieldCriteria.BIIB_Evaluation_Criteria__c;
                if(!preFieldListForQuery.containskey(objFieldCriteria.BIIB_Object_API_Name__c))
                {               
                    preFieldListForQuery.put(objFieldCriteria.BIIB_Object_API_Name__c,new Set<String> {objFieldCriteria.BIIB_Field_API_Name__c});               
                }
                else
                {               
                    preFieldListForQuery.get(objFieldCriteria.BIIB_Object_API_Name__c).add(objFieldCriteria.BIIB_Field_API_Name__c);                
                }
    
                // Store Evaluation Criteria  Id and Field Criteria Object in a Map 
                if(!preFieldCriteria.containskey(objFieldCriteria.BIIB_Evaluation_Criteria__c))
                {
                    preFieldCriteria.put(objFieldCriteria.BIIB_Evaluation_Criteria__c, new List<BIIB_Field_Criteria__c> {objFieldCriteria});
                }
                else
                {
                    preFieldCriteria.get(objFieldCriteria.BIIB_Evaluation_Criteria__c).add(objFieldCriteria);
                }       
            } 

            // Get the List Of Child Entities for Account from Custom Setting
            List<BIIB_PRE_Objects__c> lstAccountChildObjects = [SELECT BIIB_Parent_Object_Name__c,BIIB_Child_Object_Name__c,BIIB_Reference_Field__c FROM BIIB_PRE_Objects__c where BIIB_Parent_Object_Name__c =:strObjectName];
            List<sObject> lstObjectsData = new List<sObject>();
            String strOriginalEC = '';

            for(BIIB_PRE_Objects__c objPRE : lstAccountChildObjects)
            {            
                if (preFieldListForQuery.containsKey(objPRE.BIIB_Child_Object_Name__c))
                {
                        List<String> lstFieldName =new List<String>();
                        lstFieldName.addAll(preFieldListForQuery.get(objPRE.BIIB_Child_Object_Name__c));
                        String strFieldsToBeQueried = String.join(lstFieldName, ',');
                        String strQuery='select ';
                        strQuery+= strFieldsToBeQueried+' '+ 'from '; 
                        strQuery += objPRE.BIIB_Child_Object_Name__c +' '+'where ';
                        strQuery += objPRE.BIIB_Reference_Field__c+ '=:strAccountId';
                        lstObjectsData = Database.query(strQuery);
                        strQuery = '';
                        preObjListMap.put(objPRE.BIIB_Child_Object_Name__c, lstObjectsData);           
                        lstObjectsData = null;
                        lstFieldName = null;
                        strFieldsToBeQueried = '';
                }
            } // End for(BIIB_PRE_Objects__c objPRE : lstAccountChildObjects)

            //Loop through the Evaluation Criteria. Validate Field Criteria vs Actual values in the fields on the records

            Map<Double, boolean> preEvaluation = new Map<Double, boolean>();
            if(!preFieldCriteria.isEmpty())
            {
                for(String strEvalCriteria : preFieldCriteria.keyset())
                {
                    for(BIIB_Field_Criteria__c objFC : preFieldCriteria.get(strEvalCriteria))
                    {
                        String strEvulationCriteriaRowId = objFC.BIIB_Evaluation_Criteria__c;
                        Boolean isConditionSatisfied;
                        if(preObjListMap.containskey(objFC.BIIB_Object_API_Name__c))
                        {
                            for(sObject objS : preObjListMap.get(objFC.BIIB_Object_API_Name__c))
                            {
                                if(objFC.BIIB_Operator__c == '=')
                                {                           
                                    isConditionSatisfied = (objFC.BIIB_Field_Value__c  == String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c))) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == '<')
                                {                           
                                    isConditionSatisfied = (objFC.BIIB_Field_Value__c  < String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c))) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == '>')
                                {                           
                                    isConditionSatisfied = (objFC.BIIB_Field_Value__c  > String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c))) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == '<=')
                                {                           
                                    isConditionSatisfied = (objFC.BIIB_Field_Value__c  <= String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c))) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == '>=')
                                {                           
                                    isConditionSatisfied = (objFC.BIIB_Field_Value__c  >= String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c))) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == '!=')
                                {                           
                                    isConditionSatisfied = (objFC.BIIB_Field_Value__c  != String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c))) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == 'CONTAINS')
                                {                           
                                    isConditionSatisfied = (String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c)).contains(objFC.BIIB_Field_Value__c)) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == 'DOES NOT CONTAIN')
                                {
                                    isConditionSatisfied = (String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c)).contains(objFC.BIIB_Field_Value__c)) ? false : true;
                                }
                                else if(objFC.BIIB_Operator__c == 'STARTS WITH')
                                {   
                                    isConditionSatisfied = (String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c)).startsWith(objFC.BIIB_Field_Value__c)) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == 'ENDS WITH')
                                {
                                    isConditionSatisfied = (String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c)).endsWith(objFC.BIIB_Field_Value__c)) ? true : false;
                                }
                                else if(objFC.BIIB_Operator__c == 'LIKE')
                                {
                                    isConditionSatisfied = (String.valueOf(objS.get(objFC.BIIB_Field_API_Name__c)).contains(objFC.BIIB_Field_Value__c)) ? true : false;
                                }
                                preEvaluation.put(objFC.BIIB_Sequence__c,isConditionSatisfied);
                                isConditionSatisfied = false;
                            }// End for(sObject objS : preObjListMap.get(objFC.BIIB_Object_API_Name__c))
                        }// End if(preObjListMap.containskey(objFC.BIIB_Object_API_Name__c))
                    }// for(BIIB_Field_Criteria__c objFC : preFieldCriteria.get(strEvalCriteria))
                    break;
            }// End for(String strEvalCriteria : preFieldCriteria.keyset())
            
                //Fetch the User Entered Criteria from Evaluation Criteria and populate it with the actual result values which are present in Map:preEvaluation
                String strUserProvidedCondition = '';
                // Get User Entered Evaluation Criteria from BRE
                lstEC = Database.query('SELECT BIIB_Criteria__c,BIIB_ProcessFlow__c from BIIB_Evaluation_Criteria__c where Id =:strEvalCriteriaId');
                if(!lstEC .isEmpty())
                {
                    strUserProvidedCondition = lstEC[0].BIIB_Criteria__c;
                    strProcessFlowRowId = lstEC[0].BIIB_ProcessFlow__c;
                }
        
                //Replace Number in the Criteria with the actual boolean value 
                for (Double Key: preEvaluation.keySet())
                {
                    String strSequence = String.valueOf(Key).substring(0,String.valueOf(Key).indexOf('.'));
                    if(strUserProvidedCondition.contains(strSequence))
                    {   
                        boolean strbooleanresult = preEvaluation.get(Key);
                        String strConvertedFromBoolean = String.valueOf(strbooleanresult);
                        String strReplacedCondition = strUserProvidedCondition.replace(strSequence,strConvertedFromBoolean.toUpperCase());   
                        strUserProvidedCondition = strReplacedCondition;
                    }
                }
                if(strUserProvidedCondition.contains('OR'))
                {
                    strReplacedOr = strUserProvidedCondition.replaceAll('OR', '||');
                    strUserProvidedCondition = strReplacedOr ;
                }
                if(strUserProvidedCondition.contains('AND'))
                {
                    strReplacedAnd = strUserProvidedCondition.replaceAll('AND', '&&');
                    strUserProvidedCondition = strReplacedAnd;
                }
        
                if(strUserProvidedCondition.contains('TRUE'))
                {
                    strReplacedTrue = strUserProvidedCondition.replaceAll('TRUE', 'true');
                    strUserProvidedCondition = strReplacedTrue;
                }
        
                if(strUserProvidedCondition.contains('FALSE'))
                {
                    strReplacedFalse = strUserProvidedCondition.replaceAll('FALSE', 'false');
                    strUserProvidedCondition = strReplacedFalse;
                }
                strEvalCriteriaFinal = strUserProvidedCondition;
            }// End if(!preFieldCriteriaMap.isEmpty())
        }// End if(!setAllPREEvalCriteria.isEmpty())
        
        else
        {
            strContinueEvaluation = 'false';
        }// Else of --if(!setAllPREEvalCriteria.isEmpty())
    }// End public void FetchEvalCriteria()
        
        //The Evaluation criteria is sent to the VF page and the VF page sends back the Result. If it is true, then we need to proceed wit SR and Work Item Creation
        public void CreateSRWorkItems()
        {

            String strCaseObjectName = 'Case';
            Schema.SObjectType Res = Schema.getGlobalDescribe().get(strCaseObjectName);
            Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
            
            try
            {
                // Updating Flag on Account so that the PRE functionality is not invoked again whenever the Account page is refreshed This is temporaray as we do not know the trigger points yet
                Account objAccount = new Account(Id=strAccountId,BIIB_Invoke_PRE_Logic__c = true);
                update objAccount;
                 String strGenericUserId;
                //Create SR from SR Templates
                //strProcessFlowId = 'a2T110000004doM';
                List<BIIB_ProcessFlow_SRTemplate__c> lstPFSRTemplateJunction = [SELECT BIIB_SR_Template__c FROM BIIB_ProcessFlow_SRTemplate__c where BIIB_Process_Flow__c =:strProcessFlowRowId];
                List<BIIB_SR_Template__c> lstSRTemplates = new List<BIIB_SR_Template__c>();
                if(!lstPFSRTemplateJunction.isEmpty())
                {
                    lstSRTemplates = [SELECT Id,Name,BIIB_SR_Record_Type__c,BIIB_Dependency__c,BIIB_Number__c,BIIB_Required__c,BIIB_Schedule_Date__c,BIIB_ProcessFlow__c FROM BIIB_SR_Template__c where Id IN(SELECT BIIB_SR_Template__c FROM BIIB_ProcessFlow_SRTemplate__c where BIIB_Process_Flow__c =:strProcessFlowRowId)];
                }
                List<BIIB_SRTemplate_To_SR_Mapping__c> lstSRTemplateToSR = [SELECT BIIB_SR_Field__c,BIIB_SR_Template_Field__c FROM BIIB_SRTemplate_To_SR_Mapping__c];
                List<BIIB_PRE_Work_Mapping__c> lstWITemplateToWI = [SELECT BIIB_Work_Field__c,BIIB_Work_Template_Field__c FROM BIIB_PRE_Work_Mapping__c];
                List<Case> lstInsertedCases = new List<Case>();
                List<BIIB_Work__c> lstInsertedWorkItems = new List<BIIB_Work__c>();
                Map<String,Id> caserecordTypes = new Map<String,Id>();
                Map<String,Id> insertedCasesMap = new Map<String,Id>();
                Map<Id,Id> mapSRTemplateCaseId = new Map<Id,Id>();
                Map<String,Id> insertedWorkItemMap = new Map<String,Id>();
                Set<Id> setSRTemplateRowId = new Set<Id>();
            
                for(RecordType rt : [SELECT Id,Name FROM RecordType WHERE SobjectType = 'Case'])
                {
                    caserecordTypes.put(rt.Name, rt.Id);    
                }
        
            for(BIIB_SR_Template__c objSRTemplateRecords : lstSRTemplates)
            {
                    SObject objSRTemplate = objSRTemplateRecords;
                    Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('Case').getDescribe().getSObjectType();
                    SObject objCase = objectDef.newSObject();
                
                    // Loop through Custom Setting Fields For SR Template to SR and populate on Case
                    for(BIIB_SRTemplate_To_SR_Mapping__c lstMapping: lstSRTemplateToSR)
                    {
                        Schema.DisplayType strFieldDataType=fieldMap.get(lstMapping.BIIB_SR_Field__c).getDescribe().getType();
                        String strFieldDataTypeConverted =  String.valueOf(fieldMap.get(lstMapping.BIIB_SR_Field__c).getDescribe().getType());
                        
                        if(strFieldDataTypeConverted == 'BOOLEAN')
                        {
                            objCase.put(lstMapping.BIIB_SR_Field__c, Boolean.valueOf(objSRTemplate.get(lstMapping.BIIB_SR_Template_Field__c)));
                        }
                        
                        else if(strFieldDataTypeConverted == 'STRING')
                        {
                            objCase.put(lstMapping.BIIB_SR_Field__c, objSRTemplate.get(lstMapping.BIIB_SR_Template_Field__c));
                        }
                        
                        else if(strFieldDataTypeConverted == 'INTEGER')
                        {
                            objCase.put(lstMapping.BIIB_SR_Field__c, Integer.valueOf(objSRTemplate.get(lstMapping.BIIB_SR_Template_Field__c)));
                        }
                        else if(strFieldDataTypeConverted == 'DOUBLE')
                        {
                            objCase.put(lstMapping.BIIB_SR_Field__c, Double.valueOf(objSRTemplate.get(lstMapping.BIIB_SR_Template_Field__c)));
                        }
                        else
                        {
                            objCase.put(lstMapping.BIIB_SR_Field__c, objSRTemplate.get(lstMapping.BIIB_SR_Template_Field__c));
                        }
                        
                    }           
                    objCase.put('AccountId', strAccountId);
                    objCase.put('Status', 'New');
                    objCase.put('BIIB_Due_Date__c',  Date.Today().addDays(objSRTemplateRecords.BIIB_Schedule_Date__c.intValue()));
                    objCase.put('BIIB_Is_Pre_Created__c', true);
            
                    // Get RecordType Form the SR Template and Update on Case
                    if(caserecordTypes.containsKey(objSRTemplateRecords.BIIB_SR_Record_Type__c))
                    {
                        objCase.put('RecordTypeId',  caserecordTypes.get(objSRTemplateRecords.BIIB_SR_Record_Type__c));
                    }
                    Case objCaseObject = (Case)objCase;
                    insert objCaseObject;
                    system.debug('-----NewCaseId---'+objCaseObject.Id);
                    insertedCasesMap.put(objSRTemplateRecords.BIIB_Number__c, objCaseObject.Id);
                    mapSRTemplateCaseId.put(objSRTemplateRecords.Id, objCaseObject.Id);
                    lstInsertedCases.add(objCaseObject);
                }// End for(BIIB_SR_Template__c objSRTemplateRecords : lstSRTemplates)

                // Set Dependency For Cases--Parent-Child Setup
                for(Case objInsertedCases: lstInsertedCases)
                {
                    if(insertedCasesMap.containsKey(String.valueOf(Integer.valueOf(objInsertedCases.BIIB_Dependency__c))))
                    {
                        String strParentCaseId = insertedCasesMap.get(String.valueOf(Integer.valueOf(objInsertedCases.BIIB_Dependency__c)));
                        objInsertedCases.ParentId = strParentCaseId;
                        update objInsertedCases;
                    }
                    
                }// End for(Case objInsertedCases: lstInsertedCases)
                
                //Put all SRTemlateId's in a Set
                setSRTemplateRowId = mapSRTemplateCaseId.keySet();

                //Create Work Items
                
                //Get Owner Id of Generic User
                List<User> lstUser = new List<User>([SELECT Id from User where Username = 'genericuser@deloitte.com']);
                
                for(User objUser :lstUser)
                {
                     strGenericUserId = objUser.Id;
                }
                
                //Select WorkItem TemplateId's from SR-Work Template Junction Object where SRTemplate Id is in (ProcessFlow-SRTemlateJunction where ProcessId is : incomingProcessRowId)
                List<BIIB_SR_Work_Template__c> lstSRWorkTemplateJunction = [SELECT BIIB_Work_Item_Template__c,BIIB_SR_Template__c FROM BIIB_SR_Work_Template__c where BIIB_SR_Template__c IN (SELECT BIIB_SR_Template__c FROM BIIB_ProcessFlow_SRTemplate__c where BIIB_Process_Flow__c =:strProcessFlowId)];
                
                //List<BIIB_Work_Item_Template__c> lstWorkItemTemplates= [SELECT Id,Name,BIIB_Due_Date__c,BIIB_ININ_Id__c,BIIB_Dependency__c,BIIB_Num__c,BIIB_Preferred_Language__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Region__c,BIIB_Work_Group__c,BIIB_SR_Template__c,ProcessFlow__c,BIIB_Required__c FROM BIIB_Work_Item_Template__c where ProcessFlow__c =:strProcessFlowId order by BIIB_Num__c asc];
                List<BIIB_Work_Item_Template__c> lstWorkItemTemplates= [SELECT Id,Name,BIIB_Due_Date__c,BIIB_ININ_Id__c,BIIB_Dependency__c,BIIB_Num__c,BIIB_Preferred_Language__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Region__c,BIIB_Work_Group__c,BIIB_SR_Template__c,BIIB_Required__c,BIIB_ProcessFlow__c FROM BIIB_Work_Item_Template__c where Id IN(SELECT BIIB_Work_Item_Template__c FROM BIIB_SR_Work_Template__c where BIIB_SR_Template__c IN :setSRTemplateRowId) order by BIIB_Num__c asc];
                
                String strCanRouteFlag = 'Yes';
                
                for(BIIB_Work_Item_Template__c objWorkItem : lstWorkItemTemplates)
                {
                    if(mapSRTemplateCaseId.containsKey(objWorkItem.BIIB_SR_Template__c))
                    {
                        String strSRTemplateCaseId = mapSRTemplateCaseId.get(objWorkItem.BIIB_SR_Template__c);
                    }
                    SObject objWorkItemTemplate = objWorkItem;
                    Schema.sObjectType objectDefWI = Schema.getGlobalDescribe().get('BIIB_Work__c').getDescribe().getSObjectType();
                    SObject objWork = objectDefWI.newSObject();
                    for(BIIB_PRE_Work_Mapping__c objWIMapping: lstWITemplateToWI)
                    {
                        objWork.put(objWIMapping.BIIB_Work_Field__c, objWorkItemTemplate.get(objWIMapping.BIIB_Work_Template_Field__c));
                    }
                    if(strCanRouteFlag == 'Yes')
                    {
                        objWork.put('BIIB_CanRoute__c', true);
                        strCanRouteFlag = '';
                    }
                    objWork.put('BIIB_Case__c', mapSRTemplateCaseId.get(objWorkItem.BIIB_SR_Template__c));
                    objWork.put('BIIB_Account__c', strAccountId);
                    objWork.put('BIIB_Due_Date__c',  Date.Today().addDays(objWorkItem.BIIB_Due_Date__c.intValue()));
                    objWork.put('BIIB_Status__c', 'New');
                    objWork.put('BIIB_Assigned_to__c',strGenericUserId);
                        
                    BIIB_Work__c objWorkItemObject = (BIIB_Work__c)objWork;
                    insert objWorkItemObject;
                    system.debug('-----NewWorkItemId---'+objWorkItemObject.Id);
                    lstInsertedWorkItems.add(objWorkItemObject);
                    insertedWorkItemMap.put(objWorkItem.BIIB_Num__c, objWorkItemObject.Id);
                }
            
                // Set Dependency For Work--Parent-Child Setup
                for(BIIB_Work__c objInsertedWorkItems: lstInsertedWorkItems)
                {
                    if(insertedWorkItemMap.containsKey(String.valueOf(Integer.valueOf(objInsertedWorkItems.BIIB_Dependency__c))))
                    {
                        String strParentWorkId = insertedWorkItemMap.get(String.valueOf(Integer.valueOf(objInsertedWorkItems.BIIB_Dependency__c)));
                        objInsertedWorkItems.BIIB_Parent_Work__c = strParentWorkId;
                        update objInsertedWorkItems;
                    }
                }
                
            }//End Try
            
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }
            
        }// End public void BIIB_CreateSRWorkItems()
       
}// End public class BIIB_PREGenericClass