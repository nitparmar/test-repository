@isTest
private class VEEVA_NETWORK_MAPPING_TRIGGER_TEST {
    private static String[] countries = new String[]{'AF','AL','AR','AM','AU','AT','BE','BO','BY','CA','CN',
        'CR','CZ','DK','EC','ER','EE','ET','FI','FR','GA','GB','GD','GU','HK','HU','IS','IN','IE','IL','IT',
        'JP','KE','KP','KW','LV','LR','LI','LT','LU','MG','MY','MX','MZ','NL','NP','NZ','NE','NO','OM','PS',
        'PA','PL','PT','PR','QA','RO','RU','RW','SA','RS','SG','TW','TJ','TH','TR','TN','UA','US','UY','UZ',
        'VE','VN','WF','YE','ZA','ZM'};
    
    static testMethod void testInsertMapping() {
        List<Network_Mapping_vod__c> mappings = new List<Network_Mapping_vod__c>();
        for (Integer i=0; i < countries.size(); i++) {
            Network_Mapping_vod__c mapping = new Network_Mapping_vod__c(Name = 'Test Mapping ' +i, 
                                                                        Country_vod__c = countries[i]);
            // Make second half active
            if (i > (countries.size()/2)) {
                mapping.Active_vod__c = true;
            }
            mappings.add(mapping);
        }
        System.Test.startTest();
        insert mappings;
        System.Test.stopTest();
        
        List<Network_Mapping_vod__c> inserted = [SELECT Id, Name, Active_vod__c, Country_vod__c, 
                                                 Unique_Key_vod__c
                                                 FROM Network_Mapping_vod__c
                                                WHERE Id IN :mappings];
        System.assertEquals(inserted.size(), countries.size());
        // Verify unique key values
        for (Network_Mapping_vod__c mapping : inserted) {
            if (mapping.Active_vod__c) {
                System.assertEquals(mapping.Unique_Key_vod__c,mapping.Country_vod__c);
            } else {
                System.assert(mapping.Unique_Key_vod__c == null);
            }
        }
    }
    
    static testMethod void testUpdateMapping() {
        List<Network_Mapping_vod__c> mappings = new List<Network_Mapping_vod__c>();
        for (Integer i=0; i < countries.size(); i++) {
            Network_Mapping_vod__c mapping = new Network_Mapping_vod__c(Name = 'Test Mapping ' +i, 
                                                                        Country_vod__c = countries[i]);
            // Make second half active
            if (i > (countries.size()/2)) {
                mapping.Active_vod__c = true;
            }
            mappings.add(mapping);
        }
        insert mappings;
        System.Test.startTest();
        for (Network_Mapping_vod__c mapping : mappings) {
            mapping.Active_vod__c = !mapping.Active_vod__c;
        }
        update mappings;
        
        System.Test.stopTest();
        
        List<Network_Mapping_vod__c> inserted = [SELECT Id, Name, Active_vod__c, Country_vod__c, 
                                                 Unique_Key_vod__c
                                                 FROM Network_Mapping_vod__c
                                                WHERE Id IN :mappings];
        System.assertEquals(inserted.size(), countries.size());
        // Verify unique key values
        for (Network_Mapping_vod__c mapping : inserted) {
            if (mapping.Active_vod__c) {
                System.assertEquals(mapping.Unique_Key_vod__c, mapping.Country_vod__c);
            } else {
                System.assert(mapping.Unique_Key_vod__c == null);
            }
        }
    }
}