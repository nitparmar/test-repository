public with sharing class BIIB_Create_New_Literature_Order_Copy {

    

    public List<SelectOption> information_willingIW {get; set;}
    public String selectedOptionIW {get; set;}
    
    public List<SelectOption> ship_To {get; set;}
    public String selectedOptionST {get; set;}
    
    public List<SelectOption> address_Type {get; set;}
    public String selectedOptionAT {get; set;}
    
    public List<SelectOption> shipment_Method {get; set;}
    public String selectedOptionSM {get; set;}
    
    public Boolean blsIsMail {get; set;}
    public Boolean blsIsEmail {get; set;}
    public Boolean blsIsFax {get; set;}
    
    public String strAddLine1 {get; set;}
    public String strAddLine2 {get; set;}
    public String strCity {get; set;}
    public String strState {get; set;}
    public String strZip {get; set;}
    public String strFax {get; set;}
    public String strEmail {get; set;}
    public Account accPatient {get; set;}
    public List<Product_vod__c> prodCatLst {get; set;}
    public Integer intRetRowId {get; set;}
    private List<Contact> careGiverLst;
    private List<BIIB_Information_Willing__c> infWilLst;
    
    public Map<String, List<SelectOption>> productLiteratureMap {get; set;}
    
    public List<Literature_Order_Line_Item_Wrapper> loliwLst {get; set;}
    
    public class Literature_Order_Line_Item_Wrapper{
    
        public BIIB_Literature_Order_Line_Item__c lineItem {get; set;}
        public List<SelectOption> availableTherapy {get; set;}
        public List<SelectOption> availableLiterature {get; set;}
        public String selectedTherapy {get; set;}
        public String selectedLiterature {get; set;}
        public boolean isSelected {get; set;}
        public Integer rowId {get; set;}
    
    }
    
    public BIIB_Create_New_Literature_Order_Copy(ApexPages.StandardController controller)
    {
        String strRecId = ApexPages.CurrentPage().getParameters().get('id');
        accPatient = new Account();
        accPatient = [select id, Name, BIIB_Minor__c, RecordType.DeveloperName, (select id, Name, City_vod__c, State_vod__c, Zip_vod__c,Address_line_2_vod__c, Home_vod__c, Primary_vod__c, Business_vod__c from Address_vod__r), (select id, BIIB_Therapy__r.Name, BIIB_Therapy__c from Patient_Therapies__r where BIIB_Primary__c=true limit 1) from Account where Id=:strRecId];
        
        
        information_willingIW = new List<SelectOption>();
        SelectOption so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Mail').Value__c, BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Mail').Value__c);
        information_willingIW.add(so1);
        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Email').Value__c, BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Email').Value__c);
        information_willingIW.add(so1);
        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Fax').Value__c, BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Fax').Value__c);
        information_willingIW.add(so1);
        
        blsIsMail = false;
        blsIsEmail = false;
        blsIsFax = false;
        
        system.debug('accPatient.Patient_Therapies__r.size().......................'+accPatient.Patient_Therapies__r.size());
        if(accPatient.Patient_Therapies__r.size()>0)
        {
            
            infWilLst = new List<BIIB_Information_Willing__c>();
            infWilLst = [select id, BIIB_Type__c from BIIB_Information_Willing__c where BIIB_Program__c=:accPatient.Patient_Therapies__r[0].BIIB_Therapy__r.Name and BIIB_Patient_Name__c=:strRecId limit 1];
            
            if(infWilLst.size() > 0){
                if(infWilLst[0].BIIB_Type__c == BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Mail').Value__c)
                    blsIsMail = true;
                else if(infWilLst[0].BIIB_Type__c == BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Email').Value__c)
                    blsIsEmail = true;
                else if(infWilLst[0].BIIB_Type__c == BIIB_PicklistValues__c.getValues('BIIB_InformationWilling_Fax').Value__c)
                    blsIsFax = true;
            }
        }    
        
        
        ship_To = new List<SelectOption>();
        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_ShipTo_Patient').Value__c,accPatient.Name);
        ship_To.add(so1);
        careGiverLst = new List<Contact>([select id, Name, MailingStreet, MailingCity, MailingState, MailingPostalCode, OtherStreet, OtherCity, OtherState, OtherPostalCode from Contact where BIIB_Account__c=:accPatient.id and BIIB_Primary__c=true and RecordType.DeveloperName='BIIB_Alternate_Care_Giver' and RecordType.SobjectType='Contact' limit 1]);
        
        if(careGiverLst.size()>0)
            so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c, careGiverLst[0].Name);
        else
            so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c,BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c);
        ship_To.add(so1);
        
        Address_Type = new List<SelectOption>();
        
        shipment_Method = new List<SelectOption>();
        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_ShipmentMethod_USPS').Value__c, BIIB_PicklistValues__c.getValues('BIIB_ShipmentMethod_USPS').Value__c);
        shipment_Method.add(so1);
        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_ShipmentMethod_FEDEX').Value__c, BIIB_PicklistValues__c.getValues('BIIB_ShipmentMethod_FEDEX').Value__c);
        shipment_Method.add(so1);
        
        if(accPatient!=null)
        {
        Boolean bHomeAdded = False;
        Boolean bOfficeAdded = False; 
        Boolean bCustomAdded = False;   
            for(Address_vod__c add : accPatient.Address_vod__r)
                {
                    if(add.Home_vod__c && !bHomeAdded)
                    {    
                        selectedOptionAT = BIIB_PicklistValues__c.getValues('BIIB_AddressType_Home').Value__c;
                        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_AddressType_Home').Value__c, BIIB_PicklistValues__c.getValues('BIIB_AddressType_Home').Value__c);
                        Address_Type.add(so1);
                        bHomeAdded = True;
                     }
                    if(add.Business_vod__c && !bOfficeAdded)
                    {    
                        selectedOptionAT = BIIB_PicklistValues__c.getValues('BIIB_AddressType_Office').Value__c;
                        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_AddressType_Office').Value__c, BIIB_PicklistValues__c.getValues('BIIB_AddressType_Office').Value__c);
                        Address_Type.add(so1);
                        bOfficeAdded = True;
                    }
                    if(!bCustomAdded){
                        so1 = new SelectOption(BIIB_PicklistValues__c.getValues('BIIB_AddressType_Custom').Value__c, BIIB_PicklistValues__c.getValues('BIIB_AddressType_Custom').Value__c);
                        Address_Type.add(so1);
                        bCustomAdded = True;
                    }
                }
            
            if(accPatient.BIIB_Minor__c)
                selectedOptionST = BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c;
            else
                selectedOptionST = BIIB_PicklistValues__c.getValues('BIIB_ShipTo_Patient').Value__c;
            
            
            if(selectedOptionST == BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c)
                selectedOptionAT = BIIB_PicklistValues__c.getValues('BIIB_AddressType_Home').Value__c;
            else if(selectedOptionST == BIIB_PicklistValues__c.getValues('BIIB_ShipTo_Patient').Value__c)
            {
                for(Address_vod__c add : accPatient.Address_vod__r)
                {
                    if(add.Primary_vod__c)
                    {
                        if(add.Home_vod__c)
                            selectedOptionAT = BIIB_PicklistValues__c.getValues('BIIB_AddressType_Home').Value__c;
                           
                        if(add.Business_vod__c)
                            selectedOptionAT = BIIB_PicklistValues__c.getValues('BIIB_AddressType_Office').Value__c;
                    }
                }
            }
            
            
            //getAddress();
            
            prodCatLst = new List<Product_vod__c>([select id, Name, (select id, Name from Literatures__r) from Product_vod__c ]);
            loliwLst = new List<Literature_Order_Line_Item_Wrapper >();
            Literature_Order_Line_Item_Wrapper loliwrp = new Literature_Order_Line_Item_Wrapper();
            
            //system.debug('prodCatLst......................'+[select id, Name, (select id, Name from Literatures__r) from Product_vod__c]);
            //system.debug('prodCatLst......................'+[select id, Name from BIIB_Literature__c]);
            
            List<BIIB_Literature__c> litLst;
            
            if(accPatient.RecordType.DeveloperName == 'BIIB_Patient')
                litLst = new List<BIIB_Literature__c>([select id, Name, BIIB_Product_Catalog__c from BIIB_Literature__c where BIIB_Audience_Type__c LIKE '%Patient%' OR BIIB_Audience_Type__c = 'All']);
            else if(accPatient.RecordType.DeveloperName == 'BIIB_HCP')
                litLst = new List<BIIB_Literature__c>([select id, Name, BIIB_Product_Catalog__c from BIIB_Literature__c where BIIB_Audience_Type__c LIKE '%HCP%' OR BIIB_Audience_Type__c = 'All']);          
            else if(accPatient.RecordType.DeveloperName == 'BIIB_SPP' || accPatient.RecordType.DeveloperName == 'BIIB_HCO' || accPatient.RecordType.DeveloperName == 'BIIB_Infusion_Sites')
                litLst = new List<BIIB_Literature__c>([select id, Name, BIIB_Product_Catalog__c from BIIB_Literature__c where BIIB_Audience_Type__c LIKE '%Site%' OR BIIB_Audience_Type__c = 'All']);
            
            productLiteratureMap = new Map<String, List<SelectOption>>();
            
            for(BIIB_Literature__c lit : litLst)
            {
                if(productLiteratureMap.containsKey(lit.BIIB_Product_Catalog__c))
                {
                    productLiteratureMap.get(lit.BIIB_Product_Catalog__c).add(new SelectOption(lit.id, lit.Name));
                }
                else
                {
                    List<SelectOption> litSO = new List<SelectOption>();
                    litSO .add(new SelectOption(lit.id, lit.Name));
                    productLiteratureMap.put(lit.BIIB_Product_Catalog__c, litSO);
                }
            }
            
            for(Integer count = 1; count<=5 ; count++)
            {
                addOrderLineItem();
            }
            
            
        }
        
        
        
    }
    
    public pagereference toggleIsMail()
    {
        system.debug('testing.................'+selectedOptionIW);
        //system.debug('');
        if(blsIsMail)
            blsIsMail = false;
        else
        {
            blsIsMail = true;
            blsIsEmail = false;
            blsIsFax = false;
        }
        return null;
    }
    
    public pagereference toggleIsEmail()
    {
        system.debug('testing.................'+selectedOptionIW);
        //system.debug('');
        if(blsIsEmail)
            blsIsEmail = false;
        else
        {
            blsIsEmail = true;
            blsIsMail = false;
            blsIsFax = false;
        }
        return null;
    }
    
    public pagereference toggleIsFax()
    {
        system.debug('testing.................'+selectedOptionIW);
        //system.debug('');
        if(blsIsFax)
            blsIsFax = false;
        else
        {
            blsIsFax = true;
            blsIsEmail = false;
            blsIsMail = false;
        }
        return null;
    }
    
    public pagereference addOrderLineItem()
    {
        Literature_Order_Line_Item_Wrapper loliwrp = new Literature_Order_Line_Item_Wrapper();
        List<SelectOption> avlTherapyLst = new List<SelectOption>();
        for(Product_vod__c prd : prodCatLst)
        {
            if(prd.Literatures__r.size()>0)
                avlTherapyLst.add(new SelectOption(prd.id, prd.Name));
            //if(accPatient.Patient_Therapies__r[0].BIIB_Therapy__c == prd.id)
               // loliwrp.selectedTherapy = 
        }
        loliwrp.lineItem = new BIIB_Literature_Order_Line_Item__c();
        loliwrp.availableTherapy = avlTherapyLst;
        loliwrp.rowId = loliwLst.size()+1;
        
        
        
        //loliwrp.selectedTherapy = loliwrp.availableTherapy[0].getValue(); 
        loliwrp.selectedTherapy = accPatient.Patient_Therapies__r[0].BIIB_Therapy__c ;
        loliwLst.add(loliwrp);
        intRetRowId = loliwrp.rowId;
        getLiteratures();
        return null;
    }
    
    public pagereference getLiteratures()
    {
        List<SelectOption> avlLitLst = new List<SelectOption>();
        
        system.debug('productLiteratureMap......................'+productLiteratureMap);
        
        
        
        
        
        for(Literature_Order_Line_Item_Wrapper loliwrp : loliwLst)
        {
            if(intRetRowId == loliwrp.rowId)
            {
                system.debug('loliwrp.selectedTherapy......................'+loliwrp.selectedTherapy);
                //loliwrp.availableLiterature = new List<SelectOption>();
                system.debug('productLiteratureMap.get(loliwrp.selectedTherapy.................................'+productLiteratureMap.get(loliwrp.selectedTherapy));
                loliwrp.availableLiterature = new List<SelectOption>();
                List<SelectOption> availableProducts = productLiteratureMap.get(loliwrp.selectedTherapy)==null?new List<SelectOption>():productLiteratureMap.get(loliwrp.selectedTherapy);
                    
                for(SelectOption so : availableProducts)
                {
                    loliwrp.availableLiterature.add(so);    
                }
                if(loliwrp.availableLiterature.size()>0)
                    loliwrp.availableLiterature.add(0,new SelectOption('None', 'None'));
                else
                    loliwrp.availableLiterature.add(new SelectOption('None', 'None'));
                system.debug('productLiteratureMap.get(loliwrp.lineItem.BIIB_Therapy__c)......................'+loliwrp.selectedTherapy);
            }
        }
        
        return null;
    }
    
    public pagereference delOrderLineItem()
    {
        //Literature_Order_Line_Item_Wrapper loliwrp = new Literature_Order_Line_Item_Wrapper();
        for(Integer count = loliwLst.size()-1; count>=0 ; count--)
        {
            if(loliwLst[count].isSelected)
                loliwLst.remove(count);
        }
        return null;
        
    }
    
    public pagereference getAddress()
    {
        boolean blsMatch = false;
        system.debug('Get Address.........');
        if(accPatient!=null){
        
            if(selectedOptionAT==BIIB_PicklistValues__c.getValues('BIIB_AddressType_Home').Value__c)
            {
                if(selectedOptionST == BIIB_PicklistValues__c.getValues('BIIB_ShipTo_Patient').Value__c)
                {
                    for(Address_vod__c add : accPatient.Address_vod__r)
                    {
                        if(add.Home_vod__c)
                        {
                            strAddLine1 = add.Name;
                            strAddLine2 = add.Address_line_2_vod__c;
                            strCity = add.City_vod__c;
                            strState = add.State_vod__c;
                            strZip = add.Zip_vod__c;
                            blsMatch = true;
                            break;
                        }
                    }
                }
                else if(selectedOptionST == BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c)
                {
                    strAddLine1 = careGiverLst[0].MailingStreet;
                    
                    strCity = careGiverLst[0].MailingCity;
                    strState = careGiverLst[0].MailingState;
                    strZip = careGiverLst[0].MailingPostalCode;
                    blsMatch = true;
                    //break;
                    
                }
            }
        
            else if(selectedOptionAT==BIIB_PicklistValues__c.getValues('BIIB_AddressType_Office').Value__c)
            {
                if(selectedOptionST == BIIB_PicklistValues__c.getValues('BIIB_ShipTo_Patient').Value__c)
                {
                    for(Address_vod__c add : accPatient.Address_vod__r)
                    {
                        if(add.Business_vod__c)
                        {
                            strAddLine1 = add.Name;
                            strAddLine2 = add.Address_line_2_vod__c;
                            strCity = add.City_vod__c;
                            strState = add.State_vod__c;
                            strZip = add.Zip_vod__c;
                            blsMatch = true;
                            break;
                        }
                    }
                }
                else if(selectedOptionST == BIIB_PicklistValues__c.getValues('BIIB_ShipTo_CareGiver').Value__c)
                {
                    strAddLine1 = careGiverLst[0].OtherStreet;
                    
                    strCity = careGiverLst[0].OtherCity;
                    strState = careGiverLst[0].OtherState;
                    strZip = careGiverLst[0].OtherPostalCode;
                    blsMatch = true;
                    //break;
                    
                }
                
            }
            
            if(!blsMatch)
            {
                strAddLine1 = '';
                strAddLine2 = '';
                strCity = '';
                strState = '';
                strZip = '';
            }
        }
        system.debug('selectedOptionAT..................'+selectedOptionAT);
        system.debug('strAddLine1..................'+strAddLine1);
        return null;
    }
    
    public pagereference saveLiteratureOrder()
    {
        if(blsIsMail && (strAddLine1 == null || strAddLine1 == '' || strCity == null || strCity == '' || strState == null || strState == '' || strZip == null || strZip == ''))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,System.Label.BIIB_No_Mail_Address));
            return null;
        }
        else if(blsIsEmail && (strEmail == null || strEmail == ''))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,System.Label.BIIB_No_Email_Address));
            return null;
        }
        else if(blsIsFax && (strFax == null || strFax == ''))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,System.Label.BIIB_No_Fax_No));
            return null;
        }
        
        else if(!blsIsFax && !blsIsEmail && !blsIsMail)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,System.Label.BIIB_No_Information_Willing_Choice));
            return null;
        }
        
        BIIB_Literature_Request_Order__c litReqOrder = new BIIB_Literature_Request_Order__c();
        litReqOrder.BIIB_Information_Willing__c = (blsIsMail?'Mail':(blsIsEmail?'eMail':(blsIsFax?'Fax':null)));
        litReqOrder.BIIB_Ship_To__c = selectedOptionST;
        litReqOrder.BIIB_Address_Type__c = selectedOptionAT;
        
        if(blsIsMail)
        {
            litReqOrder.BIIB_Address_Line_1__c = strAddLine1;
            litReqOrder.BIIB_Address_Line_2__c = strAddLine2;
            litReqOrder.BIIB_Zip__c = strZip;
            litReqOrder.BIIB_City__c = strCity;
            litReqOrder.BIIB_State__c = strState;
            litReqOrder.BIIB_Email__c = '';
            litReqOrder.BIIB_Fax__c = '';
        }
        
        if(blsIsEmail)
        {
            litReqOrder.BIIB_Address_Line_1__c = '';
            litReqOrder.BIIB_Address_Line_2__c = '';
            litReqOrder.BIIB_Zip__c = '';
            litReqOrder.BIIB_City__c = '';
            litReqOrder.BIIB_State__c = '';
            litReqOrder.BIIB_Email__c = strEmail;
            litReqOrder.BIIB_Fax__c = '';
        }
        
        if(blsIsFax)
        {
            litReqOrder.BIIB_Address_Line_1__c = '';
            litReqOrder.BIIB_Address_Line_2__c = '';
            litReqOrder.BIIB_Zip__c = '';
            litReqOrder.BIIB_City__c = '';
            litReqOrder.BIIB_State__c = '';
            litReqOrder.BIIB_Email__c = '';
            litReqOrder.BIIB_Fax__c = strFax;
        }
        
        
        
        
        litReqOrder.BIIB_Account__c = accPatient.id;
        
        try{
            insert litReqOrder;
            
            List<BIIB_Literature_Order_Line_Item__c> lineItemLst = new List<BIIB_Literature_Order_Line_Item__c>();
            for(Literature_Order_Line_Item_Wrapper loliwrp : loliwLst)
            {    
                loliwrp.lineItem.BIIB_Literature_Request_Order__c = litReqOrder.id;
                loliwrp.lineItem.BIIB_Therapy__c = loliwrp.selectedTherapy;
                loliwrp.lineItem.BIIB_Literature__c = loliwrp.selectedLiterature;
                lineItemLst.add(loliwrp.lineItem);
            }
            
            system.debug('lineItemLst..........................'+lineItemLst);
            insert lineItemLst;
            
            List<RecordType> recTypes = new List<RecordType>([select id, Name, DeveloperName, SobjectType from RecordType where (SobjectType='Case' AND DeveloperName='BIIB_Literature') OR (SobjectType='BIIB_Work__c' AND DeveloperName='BIIB_Fulfilment')]);
            Case relatedSR = new Case(OwnerId=UserInfo.getUserId(),AccountId=ApexPages.CurrentPage().getParameters().get('id'),Status='Closed');
            BIIB_Work__c relatedWork = new BIIB_Work__c(OwnerId=UserInfo.getUserId(),BIIB_Account__c=ApexPages.CurrentPage().getParameters().get('id'),BIIB_Status__c='Done',BIIB_CanRoute__c=false);
            
            for(RecordType recType : recTypes)
            {
                if(recType.SobjectType=='Case')
                    relatedSR.RecordTypeId = recType.id;
                if(recType.SobjectType=='BIIB_Work__c')
                    relatedWork.RecordTypeId = recType.id;
            }
            
            insert relatedSR;
            
            relatedWork.BIIB_Case__c = relatedSR.id;
            relatedWork.Name = accPatient.Name + ' : ' +[select CaseNumber from Case where id=:relatedSR.id].CaseNumber;
            
            insert relatedWork;
            
            if(infWilLst.size()>0)
            {
                infWilLst[0].BIIB_Type__c = (blsIsMail?'Mail':(blsIsEmail?'Email':(blsIsFax?'Fax':null)));
                update infWilLst;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Confirm,System.Label.BIIB_Literature_Request_Generated));
            
        }catch(Exception e)
        {
            if(e.getMessage().contains('Invalid id: None'))
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,System.Label.BIIB_Not_Correct_Literature));
            else
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getMessage()));
        }
        
        return null;
    }

}