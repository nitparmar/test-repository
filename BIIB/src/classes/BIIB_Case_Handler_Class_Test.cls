/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/28/2014
 * Author : Saurabh Sertukde
 * Description : This Class is used for testing the functionality of  BIIB_Case_Handler_Class .
 * 
 */
 
 @isTest(SeeAllData = True)
 public class BIIB_Case_Handler_Class_Test{
 
  /*
         * 
         * Description : This method tests the desired functionality of BIIB_Case_Handler_Class
         * 
         */
 
     public static testMethod void case_test(){
     
     List<Account> test_list=new List<Account>();
     
     //Create test data
     Account tempAcc = BIIB_testClassHelper.createAccountPatient('casetest','testpatient');
     test_list.add(tempAcc);
     tempAcc = BIIB_testClassHelper.createAccountPatient('casetest1','testpatient1');
     test_list.add(tempAcc);
     tempAcc = BIIB_testClassHelper.createAccountPatient('casetest2','testpatient2');
     test_list.add(tempAcc);
     tempAcc = BIIB_testClassHelper.createAccountPatient('casetest3','testpatient3');
     test_list.add(tempAcc);
     tempAcc = BIIB_testClassHelper.createAccountPatient('casetest4','testpatient4');
     test_list.add(tempAcc);
     tempAcc = BIIB_testClassHelper.createAccountPatient('casetest5','testpatient5');
     test_list.add(tempAcc);
     
     insert test_list;
     
     system.debug('*******' + test_list);
     
     List<RecordType> case_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Case' AND Name  in ('12 Week Questionnaire' , 'Adverse Event' , 'Benefit Investigation' , 'Financial Assistance' , 'Medicare Outreach' , 'Patient Check-In' , 'Support')  ORDER BY Name];
     
     List<Case> test_case=new List<Case>();
     
     Case tempCase = new Case(AccountId = test_list[0].id,Status = 'New',recordTypeId = case_rectype[0].Id);
     test_case.add(tempCase);
     
     tempCase = new Case(AccountId = test_list[1].id,Status = 'Closed',recordTypeId = case_rectype[1].Id);
     test_case.add(tempCase);
     
     tempCase = new Case(AccountId = test_list[2].id,Status = 'New',recordTypeId = case_rectype[2].Id);
     test_case.add(tempCase);
     
     tempCase = new Case(AccountId = test_list[3].id,Status = 'New',recordTypeId = case_rectype[3].Id);
     test_case.add(tempCase);
     
     tempCase = new Case(AccountId = test_list[4].id,Status = 'Open',recordTypeId = case_rectype[4].Id);
     test_case.add(tempCase);
     
     tempCase = new Case(AccountId = test_list[4].id,Status = 'Open',recordTypeId = case_rectype[4].Id);
     test_case.add(tempCase);
     
     tempCase = new Case(AccountId = test_list[5].id,Status = 'New');
     test_case.add(tempCase);
     
    
     
     insert test_case;
     
     //BIIB_Case_Handler_Class.OnBeforeInsert(test_case); 
     //BIIB_Case_Handler_Class.OnBeforeUpdate(test_case);
     
     //test_case[5].Status='Closed';
     //test_case[4].Status='New';
          
     update test_case;
     }
 
 }