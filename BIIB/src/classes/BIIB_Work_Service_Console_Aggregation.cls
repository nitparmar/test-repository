/**
 *      @author Chandeep Singh
 *      @date   04/16/2014
        @description    US-0036: Contains the trigger functions for Tracker License Requests into this class.
         Modification Log:        
         ------------------------------------------------------------------------------------
                 Developer                       Date                Description
         ------------------------------------------------------------------------------------        
                Chandeep Singh                 4/16/2014            Original Version
 */
global with sharing class BIIB_Work_Service_Console_Aggregation {
    
    public static Map<String,List<BIIB_Work__c>> mapAccountWork {get;set;}
    
    public BIIB_Work_Service_Console_Aggregation(ApexPages.StandardController controller) {
      
    }   
    
    @RemoteAction
    global static String getcreateWorkAggregation(String strWorkAllocationGrouping){
        try{
            List<AggregateResult> lstAggregateAccounts = [Select 
                                                                    Work_Item_Activity__r.BIIB_Account__c WorkItemAccount
                                                          From 
                                                                    ININ_Work_Allocation__c 
                                                          Group By 
                                                                    Work_Item_Activity__r.BIIB_Account__c,
                                                                    ININ_Work_Allocation_Grouping__c 
                                                          Having 
                                                                    ININ_Work_Allocation_Grouping__c=:strWorkAllocationGrouping];
        
            if(!(lstAggregateAccounts.isempty())){
                Set<String> setUniqueAccountGrouping = new Set<String>();
                for(Integer i=0;i<lstAggregateAccounts.size();i++){
                    setUniqueAccountGrouping.add(String.valueOf(lstAggregateAccounts[i].get('WorkItemAccount')));
                }
                
                if(!(setUniqueAccountGrouping.isempty())){
                    List<BIIB_Work__c> lstAssignAccountWork;
                    mapAccountWork = new Map<String,List<BIIB_Work__c>>();
                    List<BIIB_Work__c> lstWork = new List<BIIB_Work__c>();
                    Map<Id,Account> mapAccountDetails = new Map<Id,Account>();
                    
                    for(BIIB_Work__c objWork : [Select   Id,
                                                    Name,
                                                    BIIB_Account__c,
                                                    BIIB_Account__r.Name,
                                                    BIIB_Case__c
                                                    //BIIB_AccountRecordType__c,
                                                    //BIIB_Assigned_to__c,
                                                    //BIIB_CanRoute__c,
                                                    //BIIB_Closed_Date_Time__c,
                                                    //BIIB_Dependency__c,
                                                    //BIIB_Due_Date__c,
                                                    //BIIB_ININ_ID__c,
                                                    //BIIB_Long_Delay_Reroute_Date__c,
                                                    //BIIB_Multi_Candidate__c,
                                                    //BIIB_Parent_Work__c,
                                                    //BIIB_Preferred_Language__c,
                                                    //BIIB_Preferred_Time__c,
                                                    //BIIB_Priority__c,
                                                    //BIIB_Region__c,
                                                    //BIIB_Required__c,
                                                    //BIIB_Routed__c,
                                                    //BIIB_Route_Date__c,
                                                    //BIIB_Routed_First_Date_Time__c,
                                                    //BIIB_Skills__c,
                                                    //BIIB_Status__c,
                                                    //BIIB_Time_Spent__c,
                                                    //BIIB_Work_Group__c,
                                                    //BIIB_Work_Item_Template__c,
                                                    //BIIB_Work_Still_Open_Long_Reroute__c
                                            From 
                                                    BIIB_Work__c 
                                            Where Id In (   Select
                                                                    Work_Item_Activity__c 
                                                            From 
                                                                    ININ_Work_Allocation__c 
                                                            Where   
                                                                    ININ_Work_Allocation_Grouping__c =:strWorkAllocationGrouping
                                                        )
                                            Order By
                                                    BIIB_Account__c,
                                                    BIIB_Account__r.Name]){
                            lstWork.add(objWork);                                                   
                    }
                    for(Account objAccount : [Select 
                                                        Id,
                                                        Name 
                                              From 
                                                        Account 
                                              Where 
                                                        Id In : setUniqueAccountGrouping]){
                        lstAssignAccountWork = new List<BIIB_Work__c>();
                        for(Integer i=0;i<lstWork.size();i++){
                            if(lstWork[i].BIIB_Account__c==objAccount.Id){
                                lstAssignAccountWork.add(lstWork[i]);           
                            }
                        }
                        if(!(lstAssignAccountWork.isempty())){
                            List<Account> lstAccount = new List<Account>();
                            lstAccount.add(objAccount);
                            mapAccountWork.put(JSON.serialize(lstAccount),lstAssignAccountWork);
                        }
                    }
                }
            }
            
            System.debug('Final Map-->'+JSON.serializePretty(mapAccountWork));
            return JSON.serializePretty(mapAccountWork);        
        }
        catch(Exception e){
            System.debug('Exception e-->'+e.getMessage());
            return null;
        }
    }
    
}