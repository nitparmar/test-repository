/**
*@author Aditya Ghosh
*@date 22/04/2014
@test class for BIIB_MedicareOutreachScanController
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Aditya       22/04/2014  OriginalVersion
*/

@isTest
public class BIIB_MedicareOutreachScanController_Test{
    
    public static List<Account> lstAccount;
      
    public static testmethod void testFunction(){
        test.startTest();
            lstAccount = new List<Account>();
            Account tempAcc = BIIB_testClassHelper.createAccountPatient('medOutreach','testpat');
            lstAccount.add(tempAcc);
            
            insert lstAccount;
            ApexPages.StandardSetController sc = new ApexPages.standardSetController(lstAccount);
            BIIB_MedicareOutreachScanController testInstance = new BIIB_MedicareOutreachScanController(sc);
            testInstance.scanPatients();
        test.stopTest(); 
    }

}