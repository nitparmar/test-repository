/**
*@author Kamal Mehta
*@date 12/04/2014
@description Handler class for Affiliation object all the validation and functioalities reltead to affiliation is present in this class
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Kamal       12/04/2014  OriginalVersion
*/
public with sharing class BIIB_Affiliation_Handler
{
    public Static Boolean isReverseAffiliationInserted = false;
    public Static Boolean isReverseAffiliationUpdate = false;
    /*
    * @author Kamal Mehta
    @description This method is used to call two methods ReverceAffiliation and assignRecordType
    @param List<BIIB_Affiliation__c> Newly created Affiliation
    @return void
    */
    public static void onBeforeInsert(List<BIIB_Affiliation__c> newList)
    {
        assignRecordType(newList);
        if(!isReverseAffiliationInserted)
            createReverseAffiliation(newList);
        //allowoneactivePatientSPP(newList);
    }
     /*
    * @author Kamal Mehta
    @description This method is used to call assignRecordType method. 
    @param Map<Id,BIIB_Affiliation__c> Newly created Affiliation
    @return void
    */
    public static void onBeforeUpdate(Map<Id,BIIB_Affiliation__c> mapNewAffiliations, Map<Id,BIIB_Affiliation__c> mapOldAffiliations)
    {
        assignRecordType(mapNewAffiliations.values());
        //allowoneactivePatientSPP(mapNewAffiliations.values());
    }
     public static void onAfterInsert(Map<Id,BIIB_Affiliation__c> mapNewAffiliations)
    {
        onAfterInsertAff(mapNewAffiliations);
    }
    
    public static void onAfterUpdate(Map<Id,BIIB_Affiliation__c> mapNewAffiliations, Map<Id,BIIB_Affiliation__c> mapOldAffiliations)
    {
        onAfterUpdateAffl(mapNewAffiliations,mapOldAffiliations);
    }
            
     /*
    * @author Kamal Mehta
    @description Create reverse Affiliation record
    @param List<BIIB_Affiliation__c> Newly created Affiliation
    @return void
    */
    public static void createReverseAffiliation(List<BIIB_Affiliation__c> lstAffiliation)
    {
        try
        {
            List<BIIB_Affiliation__c> lstRevAffiliation = new List<BIIB_Affiliation__c>();
            for(BIIB_Affiliation__c objAffiliation : lstAffiliation)
            {
                BIIB_Affiliation__c objAff = new BIIB_Affiliation__c();
                objAff = objAffiliation.clone();
                objAff.BIIB_To_Account__c = objAffiliation.BIIB_From_Account__c;
                objAff.BIIB_From_Account__c = objAffiliation.BIIB_To_Account__c;
                lstRevAffiliation.add(objAff);
            }
            
            if(lstRevAffiliation.size() > 0)
            {
                isReverseAffiliationInserted = true;
                insert lstRevAffiliation;
            }
        }
        catch(Exception e)
        {
            system.debug('------Exception----'+e);
            BIIB_Error_Log__c objErr = new BIIB_Error_Log__c(Component_Name__c='Affiliation Trigger', Error_Detail__c=e.getMessage(), Error_Type__c='Reverse Affiliation Creation method', BIIB_Stack_Trace__c = e.getStackTraceString(), BIIB_Timestamp__c= system.now());
            insert objErr;
        }
    }
    
    
    
     /*
    * @author Kamal Mehta
    @description Perform three task
                    -   Mapping the changes made on Affiliation record to its reverse record(Like Effective Date, End Date...)
                    -   If a new record is selected as primary than making other record's non primary
                    -   If the patient's Do Not Call is checked and we affiliate it to Professional_vod then open work items needs to be mirated to Professional_vod, SR's will continue to stay on Patient record only. 
    @param List<BIIB_Affiliation__c> Newly created Affiliation
    @return void
    */
    public static void makeReverseChangeToReverseAffiliation(List<BIIB_Affiliation__c> lstNewAffiliation, Map<Id, BIIB_Affiliation__c> mapOldAffiliation)
    {
        Set<Id> setFromAccId = new Set<Id>();
        Set<Id> setAffFromAccountId = new Set<Id>(); 
        Set<Id> setToAccId = new Set<Id>();
        Set<Id> setRecordTypeId = new Set<Id>();
        
        Set<Id> setAffiliationProcessed = new Set<Id>();
        List<BIIB_Affiliation__c> lstAffiliation = new List<BIIB_Affiliation__c>();
        Set<Id> setAffiliationId = new Set<Id>();
        Set<String> setFromAccountId = new Set<String>();
        Set<String> setAffRecordTypeId = new Set<String>();
        
        List<BIIB_Work__c> lstWorkToUpdate = new List<BIIB_Work__c>();
        Map<String,BIIB_Affiliation__c> mapToFromAccIdToAffiliation = new Map<String,BIIB_Affiliation__c>();
        Map<String,String> mapPatientIdToProfessional_vodId = new Map<String,String>();
        List<BIIB_Affiliation__c> lstAffiliationToUpdate = new List<BIIB_Affiliation__c>();
        String strPatientProfessional_vodRecTypeId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Patient_Professional);
        String strProfessional_vodInfusionSiteRecTypeId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Professional_Infusion_Site);
        
        for(BIIB_Affiliation__c objAff : lstNewAffiliation)
        {
            //Mapping the Affiliation record values to its reverse record in case of updation.
            if(mapOldAffiliation != null &&
            ((mapOldAffiliation.get(objAff.Id).BIIB_Primary__c != objAff.BIIB_Primary__c) ||
            (mapOldAffiliation.get(objAff.Id).BIIB_Effective_Date__c != objAff.BIIB_Effective_Date__c) ||
            (mapOldAffiliation.get(objAff.Id).BIIB_End_Date__c != objAff.BIIB_End_Date__c)||
            (mapOldAffiliation.get(objAff.Id).BIIB_Preferred__c != objAff.BIIB_Preferred__c)))
            {
                setFromAccId.add(objAff.BIIB_From_Account__c);
                setToAccId.add(objAff.BIIB_To_Account__c);
                mapToFromAccIdToAffiliation.put(objAff.BIIB_To_Account__c+'-'+objAff.BIIB_From_Account__c, objAff);
                setRecordTypeId.add(objAff.RecordTypeId);
            }
            
            if((mapOldAffiliation == null && objAff.BIIB_Primary__c) || (mapOldAffiliation != null && objAff.BIIB_Primary__c && 
            (mapOldAffiliation.get(objAff.Id).BIIB_Primary__c != objAff.BIIB_Primary__c)))
            {
                //If a new affiliation record is made primary then make the other affiliation record which is primary.
                if(objAff.BIIB_From_Account_Type__c == Label.BIIB_AccType_Patient || objAff.BIIB_From_Account_Type__c == Label.BIIB_AccType_Payer)
                    setAffFromAccountId.add(objAff.BIIB_From_Account__c);
                if(objAff.BIIB_To_Account_Type__c == Label.BIIB_AccType_Patient || objAff.BIIB_To_Account_Type__c == Label.BIIB_AccType_Payer)    
                    setAffFromAccountId.add(objAff.BIIB_To_Account__c);
                    
                setAffRecordTypeId.add(objAff.RecordTypeId);
                setAffiliationId.add(objAff.Id);
                
                //in case the patients Do not call is and if there is open work item migrate it to its related primary Professional_vod.
                if(objAff.BIIB_Is_Patient_DNC__c && objAff.BIIB_From_Account_Type__c == Label.BIIB_AccType_Patient && objAff.RecordTypeId == strPatientProfessional_vodRecTypeId)
                {
                    mapPatientIdToProfessional_vodId.put(objAff.BIIB_From_Account__c, objAff.BIIB_To_Account__c);
                }
                
                if(objAff.BIIB_Is_Patient_DNC__c && objAff.BIIB_To_Account_Type__c == Label.BIIB_AccType_Patient && objAff.RecordTypeId == strPatientProfessional_vodRecTypeId)
                {
                    mapPatientIdToProfessional_vodId.put(objAff.BIIB_To_Account__c, objAff.BIIB_From_Account__c);
                }
            }
            if((mapOldAffiliation == null && objAff.BIIB_Preferred__c) || (mapOldAffiliation != null && objAff.BIIB_Preferred__c && 
            (mapOldAffiliation.get(objAff.Id).BIIB_Preferred__c != objAff.BIIB_Preferred__c)))
            {
                if(objAff.BIIB_From_Account_Type__c == Label.BIIB_AccType_Professional_Vod || objAff.BIIB_From_Account_Type__c == Label.BIIB_AccType_Infusion_Site)
                    setAffFromAccountId.add(objAff.BIIB_From_Account__c);   
                if(objAff.BIIB_To_Account_Type__c == Label.BIIB_AccType_Professional_Vod || objAff.BIIB_To_Account_Type__c == Label.BIIB_AccType_Infusion_Site)    
                    setAffFromAccountId.add(objAff.BIIB_To_Account__c);
                
                setAffRecordTypeId.add(objAff.RecordTypeId);
                setAffiliationId.add(objAff.Id);
            }
        }
        
        //system.debug('mapPatientIdToProfessional_vodId.keyset()...................'+mapPatientIdToProfessional_vodId.keyset());
        //system.debug('......................'+[select id, BIIB_Status__c from BIIB_Work__c where id IN:(mapPatientIdToProfessional_vodId.keyset())]);
        
        for(BIIB_Work__c objWork : [SELECT Id, BIIB_Account__c FROM BIIB_Work__c WHERE BIIB_Account__c IN : mapPatientIdToProfessional_vodId.keyset() AND BIIB_Status__c != 'Complete'])
        {
            objWork.BIIB_Account__c = mapPatientIdToProfessional_vodId.get(objWork.BIIB_Account__c);
            lstWorkToUpdate.add(objWork);
        }
        
        for(BIIB_Affiliation__c objAff : [  SELECT Id, BIIB_Primary__c, BIIB_From_Account__c, BIIB_To_Account__c FROM BIIB_Affiliation__c 
                                            WHERE BIIB_From_Account__c IN : setToAccId AND BIIB_To_Account__c IN : setFromAccId AND RecordTypeId IN : setRecordTypeId])
        {
            if(mapToFromAccIdToAffiliation.containsKey(objAff.BIIB_From_Account__c+'-'+objAff.BIIB_To_Account__c))
            {
                objAff.BIIB_End_Date__c = mapToFromAccIdToAffiliation.get(objAff.BIIB_From_Account__c+'-'+objAff.BIIB_To_Account__c).BIIB_End_Date__c;
                objAff.BIIB_Primary__c = mapToFromAccIdToAffiliation.get(objAff.BIIB_From_Account__c+'-'+objAff.BIIB_To_Account__c).BIIB_Primary__c;
                objAff.BIIB_Effective_Date__c = mapToFromAccIdToAffiliation.get(objAff.BIIB_From_Account__c+'-'+objAff.BIIB_To_Account__c).BIIB_Effective_Date__c;
                objAff.BIIB_Preferred__c = mapToFromAccIdToAffiliation.get(objAff.BIIB_From_Account__c+'-'+objAff.BIIB_To_Account__c).BIIB_Preferred__c;
                lstAffiliationToUpdate.add(objAff);
                setAffiliationProcessed.add(objAff.Id);
            }
        }
        
        for(BIIB_Affiliation__c  objAff : [ SELECT Id, BIIB_Primary__c, BIIB_Preferred__c, BIIB_From_Account__c, BIIB_To_Account__c, RecordTypeId FROM BIIB_Affiliation__c 
                                            WHERE (BIIB_Primary__c = true OR BIIB_Preferred__c = true) AND Id Not IN : setAffiliationId AND (BIIB_From_Account__c IN : setAffFromAccountId OR BIIB_To_Account__c IN : setAffFromAccountId) AND RecordTypeId IN : setAffRecordTypeId])
        {
            if(!setAffiliationProcessed.contains(objAff.Id))
            {
                if(objAff.BIIB_Primary__c)
                    objAff.BIIB_Primary__c = false;
                if(objAff.BIIB_Preferred__c)
                    objAff.BIIB_Preferred__c = false;
                lstAffiliationToUpdate.add(objAff);
            }
        }
        
        if(lstAffiliationToUpdate.size() > 0)
        {
            isReverseAffiliationUpdate = true;
            update lstAffiliationToUpdate;
        }
        
        if(lstWorkToUpdate.size() > 0)
            update lstWorkToUpdate;
    }
    
     /*
    * @author Kamal Mehta
    @description This method is used to assign record type to the affiliation record based on the values selected From Account and To Account. 
    @param List<BIIB_Affiliation__c> Newly created Affiliation
    @return void
    */
    public static void assignRecordType(List<BIIB_Affiliation__c> lstAffiliation)
    {
        String strPatientProfessional_vodId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Patient_Professional);
        String strPatientSPPId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Patient_SPP);
        String strPatientInfusionSiteId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Patient_Infusion_Site);
        String strPayerInfusionSiteId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Payer_Infusion_Site);
        String strProfessional_vodInfusionSiteId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Professional_Infusion_Site);
        
        for(BIIB_Affiliation__c objAffiliation : lstAffiliation)
        {
            if((objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Patient || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Patient) 
            && (objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Professional_Vod || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Professional_Vod))
            {
                //patient-Professional_vod
                objAffiliation.RecordTypeId = strPatientProfessional_vodId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Patient || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Patient) 
            && (objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Infusion_Site || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Infusion_Site))
            {
                //patient-infusion site
                objAffiliation.RecordTypeId = strPatientInfusionSiteId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Patient || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Patient) 
            && (objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_SPP || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_SPP))
            {
                //patient-SPP
                objAffiliation.RecordTypeId = strPatientSPPId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Payer || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Payer) 
            && (objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Infusion_Site || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Infusion_Site))
            {
                //Payer-Infusion Site
                objAffiliation.RecordTypeId = strPayerInfusionSiteId;
            }
            else if((objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Professional_Vod || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Professional_Vod) 
            && (objAffiliation.BIIB_To_Account_Type__c == Label.BIIB_AccType_Infusion_Site || objAffiliation.BIIB_From_Account_Type__c == Label.BIIB_AccType_Infusion_Site))
            {
                //Professional_vod-Infusion Site
                objAffiliation.RecordTypeId = strProfessional_vodInfusionSiteId;
            }
            else
            {
                objAffiliation.addError('You can create Affiliation between Professional - Infusion Site, Patient - Professional, Patient - Infusion Sites, Patient - SPP and Payer - Infusion Site');
            }
        }
    }
    
     /*
    * @author Kamal Mehta
    @description This function is called on after insert of affiliation record which in turn calls the patient validation fucntion. 
    @param Map<Id,BIIB_Affiliation__c> mapNewAffiliations, Map<Id,BIIB_Affiliation__c> mapOldAffiliations
    @return void
    */
    public static void onAfterInsertAff(Map<Id,BIIB_Affiliation__c> mapNewAffiliations)
    {
        Set<Id> setToAccount = new Set<Id>();
        Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj = new Map<String,BIIB_Affiliation__c>();
        Set<Id> setValidateFromAccountId = new Set<Id>();
        Set<ID> setProfessional_vodAccIds = new Set<ID>();  //Set to collect Professional_vod account ids
        List<Account> lstProfessional_vodAcc = new List<Account>(); //Collect the List of Professional_vod Account to update
        
        String strPatientProfessional_vodRecTypeId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Patient_Professional);
        
        for(BIIB_Affiliation__c objAff : mapNewAffiliations.values())
        {
            if(objAff.BIIB_Primary__c && objAff.RecordTypeId == strPatientProfessional_vodRecTypeId)
            {
                setToAccount.add(objAff.BIIB_To_Account__c);
                mapAccToFromToAffiliationObj.put(objAff.BIIB_To_Account__c+'-'+objAff.BIIB_From_Account__c, objAff);
                setValidateFromAccountId.add(objAff.BIIB_From_Account__c);
            }
            if(objAff.BIIB_Affiliation_Type__c == Label.BIIB_Affl_Professional_Patient){
                setProfessional_vodAccIds.add(objAff.BIIB_From_Account__c);
            }
        }
        
        for(Account acc: [Select Id, BIIB_Account_Profile_Type__c, PersonDoNotCall from Account where BIIB_Account_Profile_Type__c =: Label.BIIB_AccType_Professional_Vod AND PersonDoNotCall = True]){
            lstProfessional_vodAcc.add(acc);        
        }
        if(lstProfessional_vodAcc.size()>0){
            BIIB_AccountTrigger_Handler.updateProfessionalDoNotCallOnPatient(lstProfessional_vodAcc);
        }
        
        if(setToAccount.size() > 0 && mapAccToFromToAffiliationObj.size() > 0)
            validatePrimaryAffiliation(mapAccToFromToAffiliationObj, setToAccount, setValidateFromAccountId);
            
        if(!isReverseAffiliationUpdate)
            makeReverseChangeToReverseAffiliation(mapNewAffiliations.values(), null);
    }
    
     /*
    * @author Kamal Mehta
    @description This function is called on after update of affiliation record which in turn calls the patient validation fucntion. 
    @param Map<Id,BIIB_Affiliation__c> mapNewAffiliations, Map<Id,BIIB_Affiliation__c> mapOldAffiliations
    @return void
    */
    public static void onAfterUpdateAffl(Map<Id,BIIB_Affiliation__c> mapNewAffiliations, Map<Id,BIIB_Affiliation__c> mapOldAffiliations)
    {
        Set<Id> setToAccount = new Set<Id>();
        Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj = new Map<String,BIIB_Affiliation__c>();
        Set<Id> setValidateFromAccountId = new Set<Id>();
        
        String strPatientProfessional_vodId = BIIB_Utility_Class.getRecordTypeId(Label.BIIB_Affiliation_Object, Label.BIIB_RecType_Patient_Professional);
        
        for(BIIB_Affiliation__c objAff : mapNewAffiliations.values())
        {
            //Validation related to primary checkbox needs to be done for Patient - Professional_vod record type only.
            if(objAff.BIIB_Primary__c != mapOldAffiliations.get(objAff.Id).BIIB_Primary__c && !objAff.BIIB_Primary__c
            && objAff.RecordTypeId == strPatientProfessional_vodId) 
            {
                setToAccount.add(objAff.BIIB_To_Account__c);
                mapAccToFromToAffiliationObj.put(objAff.BIIB_To_Account__c+'-'+objAff.BIIB_From_Account__c, objAff);
                setValidateFromAccountId.add(objAff.BIIB_From_Account__c);
            }
        }
        if(setToAccount.size() > 0 && mapAccToFromToAffiliationObj.size() > 0)
            validatePrimaryAffiliation(mapAccToFromToAffiliationObj, setToAccount, setValidateFromAccountId);
            
        if(!isReverseAffiliationUpdate)
            makeReverseChangeToReverseAffiliation(mapNewAffiliations.values(), mapOldAffiliations);
    }
    
    /*
    * @author Kamal Mehta
    @description This function does not allow the affiliation to mark as unprimary if there are open work item on the Professional_vod related to patient. 
    @param Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj, Set<Id> setToAccountId, Set<Id> setFromAccountId
    @return void
    */
    public static void validatePrimaryAffiliation(Map<String,BIIB_Affiliation__c> mapAccToFromToAffiliationObj, Set<Id> setToAccountId, Set<Id> setFromAccountId)
    {
        for(BIIB_Work__c objWork : [ SELECT Id, BIIB_Account__c, BIIB_Case__r.AccountId FROM BIIB_Work__c 
                                WHERE BIIB_Status__c != 'Completed' AND BIIB_Case__r.AccountId IN : setFromAccountId AND BIIB_Case__r.BIIB_Is_Patient_DND__c = true])
        {
            if(mapAccToFromToAffiliationObj.containsKey(objWork.BIIB_Account__c+'-'+objWork.BIIB_Case__r.AccountId))
            {
                mapAccToFromToAffiliationObj.get(objWork.BIIB_Account__c+'-'+objWork.BIIB_Case__r.AccountId).addError('You cannot mark Affiliation as non primary as there are open work pending with its related Professional');
            }
        }
    }
     /*
    * @author Rancy Fernandes
    @description This function does not allow more than one active affiliation between an SPP and Patient. 
    @param List<BIIB_Affiliation__c> newList
    @return void
    */
    //public static void allowoneactivePatientSPP(List<BIIB_Affiliation__c> newAfllList){
      /*Boolean bThrowError = False;
      Map<Id,List<BIIB_Affiliation__c>> mapLstOfAff = new Map<Id,List<BIIB_Affiliation__c>>();
      Map<Id,Account> mapSPP = new Map<Id,Account>();
      Map<Id,Account> mapPatient = new Map<Id,Account>();
      List<BIIB_Affiliation__c> listactiveSPPPatAff = new List<BIIB_Affiliation__c>();
      List<Account> listAccountSPP = new List<Account>();
      listAccountSPP = [Select id from Account where RecordType.DeveloperName = 'BIIB_SPP'];
      for(Account temp : listAccountSPP ){
          mapSPP.put(temp.Id,temp);
      }
      List<Account> listAccountPatient = new List<Account>();
      listAccountPatient = [Select id from Account where RecordType.DeveloperName = 'BIIB_Patient'];
      for(Account temp : listAccountPatient ){
          mapPatient.put(temp.Id,temp);
      }
      Set<Id> setPatientId = new Set<Id>();
      for (BIIB_Affiliation__c aff: newAfllList){
        if((( mapSPP.keySet().contains(aff.BIIB_From_Account__c) && mapPatient.keySet().contains(aff.BIIB_To_Account__c))|| ( mapSPP.keySet().contains(aff.BIIB_To_Account__c) && mapPatient.keySet().contains(aff.BIIB_From_Account__c))) && aff.BIIB_Active__c == True){
          listactiveSPPPatAff.add(aff);
          if(mapPatient.keySet().contains(aff.BIIB_To_Account__c)){
            setPatientId.add(aff.BIIB_To_Account__c);
          }else if(mapPatient.keySet().contains(aff.BIIB_From_Account__c)){
            setPatientId.add(aff.BIIB_From_Account__c);
          }
        }
      }
      for(BIIB_Affiliation__c aff : [Select Id, Name, BIIB_From_Account__c, BIIB_To_Account__c from BIIB_Affiliation__c where recordtype.DeveloperName ='BIIB_Patient_SPP' and BIIB_Active__c = True]){
            List<BIIB_Affiliation__c> lstAffiliations = new List<BIIB_Affiliation__c>();
            if(mapLstOfAff.keySet().contains(aff.BIIB_From_Account__c)){
                lstAffiliations = mapLstOfAff.get(aff.BIIB_From_Account__c);
                lstAffiliations.add(aff);
                mapLstOfAff.put(aff.BIIB_From_Account__c,lstAffiliations);
            }
            else{
                lstAffiliations.add(aff);
                mapLstOfAff.put(aff.BIIB_From_Account__c,lstAffiliations);
            }
      }
      System.debug('@@Map of Affiliations : ' +mapLstOfAff);
      for(Id temp : setPatientId){
          if(!mapLstOfAff.isEmpty()){
                if(mapLstOfAff.keyset().contains(temp)){
                    bThrowError = True;
                }
            }
      }
      for(BIIB_Affiliation__c aff: newAfllList){
          if(bThrowError){
              aff.addError('There can be only one active affiliation between an SPP and Patient');
          }
      }*/
    //}
}