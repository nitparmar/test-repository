/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/29/2014
 * Author : Avishek Roy
 * Description : This Class is used for testing the functionality of  BIIB_WorkItem_Disposition_ClassTest.
 * 
 */

@isTest(SeeAllData=true) 
private class BIIB_WorkItem_Disposition_ClassTest { 

     private static testMethod void testWorkItem_Disposition_BIIB_WorkItem_Disposition()
     {
         PageReference pageRef = Page.BIIB_WorkItem_Disposition;
         Test.setCurrentPage(pageRef);
         
         
         Account accRec = new Account(Name = 'newAccount');
         insert accRec;
         List<Case> lstServReq = new List<Case>();
         Case caseRec = new Case(AccountId = accRec.id, Status = 'New');
         lstServReq.add(caseRec);
         insert lstServReq;
         
         List<RecordType> lstRecType = new List<RecordType>([SELECT Id,Name,DeveloperName from RecordType where sObjectType = 'BIIB_Work__c']);
         
         BIIB_Work__c workRec1 = new BIIB_Work__c(Name = 'newWork', BIIB_Account__c = accRec.id, BIIB_Case__c = caseRec.id, RecordTypeId = lstRecType[0].id, BIIB_Reason_Code__c = System.Label.BIIB_Work_Reason_Code_Values);
         insert workRec1;
         
         List<BIIB_Work__c> lstWorkItem = new List<BIIB_Work__c>();
         BIIB_Work__c workRec = new BIIB_Work__c(Name = 'newWork', BIIB_Account__c = accRec.id, BIIB_Case__c = caseRec.id, /*BIIB_Parent_Disposition_Work__c=workRec1.id,*/ RecordTypeId = lstRecType[0].id, BIIB_Reason_Code__c = System.Label.BIIB_Work_Reason_Code_Values);
         lstWorkItem.add(workRec);
         insert lstWorkItem;
         
         //List<BIIB_Work__c> lstWorkItem = new List<BIIB_Work__c>();
         //BIIB_Work__c workRec1 = new BIIB_Work__c(Name = 'newWork1', BIIB_Account__c = accRec.id, BIIB_Case__c = caseRec.id, );
         //insert workRec;

         
         
         ApexPages.CurrentPage().getparameters().put('newid', lstWorkItem[0].id);
         ApexPages.StandardController stdCont = new ApexPages.StandardController(lstWorkItem[0]);
         
         BIIB_WorkItem_Disposition_Class ext = new BIIB_WorkItem_Disposition_Class(stdCont);
         Test.startTest();
         
         ext.redirectToWIPage();
         ext.redirectToFollowUpWI();
         ext.ValidateNoOfAttempts();
         ext.CreateFollowUpWorkItem();
         
         lstWorkItem[0].BIIB_Parent_Disposition_Work__c=workRec1.id;
         
         workRec = new BIIB_Work__c(Name = 'newWork1', BIIB_Account__c = accRec.id, BIIB_Case__c = caseRec.id, RecordTypeId = lstRecType[0].id, BIIB_Reason_Code__c = System.Label.BIIB_Work_Reason_Code_Values);
         lstWorkItem.add(workRec);
         workRec = new BIIB_Work__c(Name = 'newWork2', BIIB_Account__c = accRec.id, BIIB_Case__c = caseRec.id, RecordTypeId = lstRecType[0].id, BIIB_Reason_Code__c = System.Label.BIIB_Work_Reason_Code_Values);
         lstWorkItem.add(workRec);

         ext.ValidateNoOfAttempts();
         
         upsert lstWorkItem;
         ext.ValidateNoOfAttempts();
         
         lstWorkItem[0].BIIB_Reason_Code__c = null;
         update lstWorkItem[0];
         ext.ValidateNoOfAttempts();
         
         lstWorkItem[0].RecordTypeId = lstRecType[1].id;
         lstWorkItem[0].BIIB_Reason_Code__c = System.Label.BIIB_Work_Reason_Code_Values;
         update lstWorkItem[0];
         ext.ValidateNoOfAttempts();
         ext.CreateFollowUpWorkItem();
         
         lstWorkItem[0].BIIB_Reason_Code__c = null;
         update lstWorkItem[0];
         ext.CreateFollowUpWorkItem();
         
         Test.stopTest();
     }
     
     private static testMethod void testWorkItem_Disposition_BIIB_WI_Disposition_Popup()
     {
         PageReference pageRef = Page.BIIB_WI_Disposition_Popup;
         Test.setCurrentPage(pageRef);
         
         Account accRec = new Account(Name = 'newAccount');
         insert accRec;
         Case caseRec = new Case(AccountId = accRec.id, Status = 'New');
         insert caseRec;
         BIIB_Work__c workRec = new BIIB_Work__c(Name = 'newWork', BIIB_Account__c = accRec.id, BIIB_Case__c = caseRec.id);
         insert workRec;
         
         ApexPages.CurrentPage().getparameters().put('newid', workRec.id);
         ApexPages.StandardController stdCont = new ApexPages.StandardController(workRec);
         
         BIIB_WorkItem_Disposition_Class ext = new BIIB_WorkItem_Disposition_Class(stdCont);
         Test.startTest();
         
         ext.redirectToWIPage();
         ext.redirectToFollowUpWI();
         ext.strWorkItemRowId = workRec.id;
         ext.CreateFollowUpWorkItemFromPopUp();
         ext.CreateEscalateToFieldWI();
         
         Test.stopTest();
     }

}