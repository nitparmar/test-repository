/**
*@author Rancy Fernandes
*@date 08/04/2014
@description Handler class for BIIB_Case_Trigger
    1. User story: US-0115. To allow multiple Open Cases for recordtypes which are specified in the custom setting BIIB_Case_RecordTypes__c where Multiple is true before Insert.
    2. User story: US-0126. To change the record type from Adverse Event to Closed AE when the Adverse Event record type case is closed before insert and before update.
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rancy      08/04/2014  OriginalVersion
*/
public with sharing class BIIB_Case_Handler_Class 
{
    /*
    * @author Rancy Fernandes
    @description This method calls two methods - preventCaseDuplicates & ClosedAERecordtype 
    @param List<Case> Newly created Case List
    @return void
    */
    public static void OnBeforeInsert(List<Case> newCaseList) 
    {
        if(newCaseList != null && !newCaseList.isEmpty()) 
        {
            preventCaseDuplicates(newCaseList);
            ClosedAERecordtype(newCaseList);
        }
    }
    
    /*
    * @author Rancy Fernandes
    @description This method calls method ClosedAERecordtype 
    @param List<Case> Newly created Case List
    @return void
    */
    
    public static void OnBeforeUpdate(List<Case> newCaseList) 
    {
        if(newCaseList != null && !newCaseList.isEmpty()) 
        {
            ClosedAERecordtype(newCaseList);
        }
    }
    
    /*
    * @author Rancy Fernandes
    @description This method throws an error if the is an existing open case with the same type on the same patient, or else lets the new case to insert.
    @param List<Case> Newly created Case List
    @return void
    */
    
    static void preventCaseDuplicates(List<Case> triggerCaseList) 
    {
        //Query custom setting and create a map with reordtype name as key and permission(Not Allowed?) as value 
        Map<String,Boolean> mapCaseTypeToPermission = new Map<String,Boolean>();
        Map<String,Map<String,Integer>> mapAccountIdToCaseRecordTypeToCount = new Map<String,Map<String,Integer>>();
        Map<String,String> mapCaseRecordTypeToDeveloperName = new Map<String,String>();
        
        for(BIIB_Case_RecordTypes__c caseRecordtypeList : BIIB_Case_RecordTypes__c.getAll().values())
        {
            if(!caseRecordtypeList.BIIB_Multiple_Allowed__c)
                mapCaseTypeToPermission.put(caseRecordtypeList.Name,caseRecordtypeList.BIIB_Multiple_Allowed__c);
        }
        
        system.debug('--------------'+mapCaseTypeToPermission);
        //loop thorugh the case list and create a set of Account ids
        Set<Id> setAccountId = new Set<Id>();
        for(Case objCase : triggerCaseList)
        {
            if(objCase.AccountId != null) setAccountId.add(objCase.AccountId);
        }
        
        //Querying all Existing case records related to Account and framing a map of Account Id to Map of Case Record Type and Count
        for(Case objCase : [    SELECT RecordType.DeveloperName, RecordType.Id, RecordTypeId, Account.RecordTypeId, AccountId, Account.Name FROM Case 
                                WHERE Status!='Closed'])
        {
            mapCaseRecordTypeToDeveloperName.put(objCase.RecordType.Id,objCase.RecordType.DeveloperName);
            if(!mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId))
            {
                Map<String,Integer> mapCaseRecorTypeToCount = new Map<String,Integer>();
                mapCaseRecorTypeToCount.put(objCase.RecordTypeId,1);
                mapAccountIdToCaseRecordTypeToCount.put(objCase.AccountId,mapCaseRecorTypeToCount);
            }
            else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
            && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
            {
                Integer count = mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId);
                count++;
                mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,count);
            }
            else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
            && !mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
            {
                mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,1);
            } 
        }
        
        system.debug('------mapAccountIdToCaseRecordTypeToCount--------'+mapAccountIdToCaseRecordTypeToCount);
        
        for(Case objCase : triggerCaseList)
        {
            String strRecDeveloperName = mapCaseRecordTypeToDeveloperName.get(objCase.RecordTypeId);
            if(strRecDeveloperName != null && mapCaseTypeToPermission.containsKey(strRecDeveloperName))
            {
                system.debug('------objCase.RecordType.DeveloperName--------'+objCase.RecordType.DeveloperName);
                if(objCase.AccountId != Null && mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId)
                && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId) 
                && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId) > 0)
                {
                    objCase.addError('There is already open case with the same type on the same patient');
                }
                else if(objCase.AccountId != Null)
                {
                    if(!mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId))
                    {
                        Map<String,Integer> mapCaseRecorTypeToCount = new Map<String,Integer>();
                        mapCaseRecorTypeToCount.put(objCase.RecordTypeId,1);
                        mapAccountIdToCaseRecordTypeToCount.put(objCase.AccountId,mapCaseRecorTypeToCount);
                    }
                    else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                    && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                    {
                        Integer count = mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId);
                        count++;
                        mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,count);
                    }
                    else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                    && !mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                    {
                        mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,1);
                    }
                }
            }
            else if(objCase.AccountId != Null)
            {
                if(!mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId))
                {
                    Map<String,Integer> mapCaseRecorTypeToCount = new Map<String,Integer>();
                    mapCaseRecorTypeToCount.put(objCase.RecordTypeId,1);
                    mapAccountIdToCaseRecordTypeToCount.put(objCase.AccountId,mapCaseRecorTypeToCount);
                }
                else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                && mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                {
                    Integer count = mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).get(objCase.RecordTypeId);
                    count++;
                    mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,count);
                }
                else if(mapAccountIdToCaseRecordTypeToCount.containskey(objCase.AccountId) 
                && !mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).containsKey(objCase.RecordTypeId))
                {
                    mapAccountIdToCaseRecordTypeToCount.get(objCase.AccountId).put(objCase.RecordTypeId,1);
                }
            }
        }           
    }
     /*
    * @author Rancy Fernandes
    @description This method changes the case status 
    @param List<Case> Newly created Case List
    @return void
    */
     static void ClosedAERecordtype(List<Case> triggerCaseList) {
       List<Case> caseList = new List<Case>();
       
       Map<Id,String> caseRecordtypeMap = BIIB_Utility_Class.caseRTDevName('Case');
       Map<String,Id> caseRecordtypeNameMap = new Map<String,Id>();
       
       List<String> caseClosedStatusList = ((String)System.Label.BIIB_Case_Closed_Status_values).split(';');
       Set<String> caseClosedStatusSet= new Set<String>();
       caseClosedStatusSet.addAll(caseClosedStatusList); 
       
       if(!caseRecordtypeMap.isEmpty()){
         for (Id i: caseRecordtypeMap.keySet()){
             caseRecordtypeNameMap.put(caseRecordtypeMap.get(i), i);  
         }
       }
       
      for(Case objCase : triggerCaseList)
        {
          system.debug('-' + caseClosedStatusSet + '-' + objCase.Status + '-' +caseRecordtypeMap.get(objCase.RecordTypeId));
            if(caseClosedStatusSet.contains(objCase.Status) && caseRecordtypeMap.get(objCase.RecordTypeId)==System.label.BIIB_Adverse_Event) 
              caseList.add(objCase);
        }
        system.debug('caseList' + caseList);
        for (Case objCase: caseList){
          Case c = new Case();
          c= objCase;
          c.RecordtypeId=caseRecordtypeNameMap.get(System.label.BIIB_AE_Closed);
        }
        
    }
}