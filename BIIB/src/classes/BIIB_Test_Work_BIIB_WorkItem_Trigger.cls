@isTest(SeeAllData=true)
public class BIIB_Test_Work_BIIB_WorkItem_Trigger
{
    private static set<String> setWorkRecordTypes = new set<String>{'BIIB_Work_Item_1','BIIB_Work_Item_2','BIIB_Escalate_To_Field'};
     
     static testMethod void  BIIB_Test_Work_Item_Trigger()
     {
         // New Work
         
         system.debug('---intestmethod----');
        Map<String, Id> mapWorkRecordTypes = new Map<String, Id>();
         
         
         List <RecordType> lstWorkRecordTypes = [SELECT DeveloperName, Id from RecordType where DeveloperName IN : setWorkRecordTypes];
         system.debug('---lstWorkRecordTypes----'+lstWorkRecordTypes);
            for(RecordType objRecordType : lstWorkRecordTypes)
            {
                if(!mapWorkRecordTypes.containsKey(objRecordType.DeveloperName))
                {
                    mapWorkRecordTypes.put(objRecordType.DeveloperName,objRecordType.Id);
                }
                else
                {
                    mapWorkRecordTypes.put(objRecordType.DeveloperName,objRecordType.Id);
                }
            }
         system.debug('---mapWorkRecordTypes----'+mapWorkRecordTypes);
         test.startTest();
         
       
        //Creating Account Test data
        RecordType objAccRecordType = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName='BIIB_Patient' limit 1];
        System.debug('***objAccRecordType*****' +objAccRecordType.Id);
        Account objAccount = new Account(FirstName = 'Steve' , LastName = 'Denver' , RecordTypeId = objAccRecordType.Id , PersonDoNotCall = false);                                         
        insert  objAccount;
        system.debug('***objAccount*****' +objAccount.Id);
        
        //Creating Case Test data
        RecordType objCaseRecordType = [SELECT Id,Name FROM RecordType WHERE SObjectType='Case' AND DeveloperName='BIIB_Benefit_Investigation' limit 1];
        System.debug('***objCaseRecordType*****' +objCaseRecordType.Id);
        Case objCase = new Case(BIIB_Is_Pre_Created__c = true, BIIB_Comments__c = 'testcase', AccountId = objAccount.Id,RecordTypeId = objCaseRecordType.Id,Status = 'New');                                        
        insert  objCase;
        system.debug('***objCase*****' +objCase.Id);
        
        
         //Creating Work Item Test data
         BIIB_Work__c objWork = new BIIB_Work__c(Name = 'A001',
                                        BIIB_Account__c = objAccount.Id,
                                        BIIB_Assigned_to__c = '00511000001Hi3r',
                                        BIIB_ININ_ID__c = '666666',
                                        BIIB_Preferred_Language__c = 'English',
                                        BIIB_Preferred_Time__c = '8PM',
                                        BIIB_Priority__c = '85',
                                        BIIB_Region__c = 'UCAN',
                                        BIIB_Status__c = 'Closed-Failed',
                                        BIIB_Work_Group__c = 'Acquisition',
                                        RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_1'),
                                        BIIB_Case__c = objCase.Id
                                        
                                        );
                                        
            insert  objWork;                    
            system.debug('---objWork----'+objWork);
            system.debug('---objWorkId----'+objWork.Id);
            BIIB_Work__c objWorkQuery=[SELECT Id, Name from BIIB_Work__c where Name = 'A001'];
            system.debug('---objWorkQuery----'+objWorkQuery.Id);
            System.assertEquals(objWorkQuery.Id, objWork.Id);
            
            BIIB_Work__c objWork1 = new BIIB_Work__c(Name = 'A002',
                                        BIIB_Account__c = objAccount.Id,
                                        BIIB_Assigned_to__c = '00511000001Hi3r',
                                        BIIB_ININ_ID__c = '666666',
                                        BIIB_Preferred_Language__c = 'English',
                                        BIIB_Preferred_Time__c = '8PM',
                                        BIIB_Priority__c = '85',
                                        BIIB_Region__c = 'UCAN',
                                        BIIB_Status__c = 'Closed-Failed',
                                        BIIB_Work_Group__c = 'Acquisition',
                                        RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_1'),
                                        BIIB_Case__c = objCase.Id
                                        
                                        );
                                        
            insert  objWork1;                   
            system.debug('---objWork----'+objWork1);
            system.debug('---objWorkId----'+objWork1.Id);
            BIIB_Work__c objWorkQuery1=[SELECT Id, Name from BIIB_Work__c where Name = 'A002'];
            system.debug('---objWorkQuery----'+objWorkQuery1.Id);
            System.assertEquals(objWorkQuery1.Id, objWork1.Id);
            
            BIIB_Work__c objWork2 = new BIIB_Work__c(Name = 'A003',
                                        BIIB_Account__c = objAccount.Id,
                                        BIIB_Assigned_to__c = '00511000001Hi3r',
                                        BIIB_ININ_ID__c = '666666',
                                        BIIB_Preferred_Language__c = 'English',
                                        BIIB_Preferred_Time__c = '8PM',
                                        BIIB_Priority__c = '85',
                                        BIIB_Region__c = 'UCAN',
                                        BIIB_Status__c = 'Closed-Failed',
                                        BIIB_Work_Group__c = 'Acquisition',
                                        RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_1'),
                                        BIIB_Case__c = objCase.Id
                                        
                                        );
                                        
            insert  objWork2;                   
            system.debug('---objWork----'+objWork2);
            system.debug('---objWorkId----'+objWork2.Id);
            BIIB_Work__c objWorkQuery2=[SELECT Id, Name from BIIB_Work__c where Name = 'A003'];
            system.debug('---objWorkQuery----'+objWorkQuery2.Id);
            System.assertEquals(objWorkQuery2.Id, objWork2.Id);
            
            BIIB_Work__c objWork3 = new BIIB_Work__c(Name = 'A004',
                                        BIIB_Account__c = objAccount.Id,
                                        BIIB_Assigned_to__c = '00511000001Hi3r',
                                        BIIB_ININ_ID__c = '666666',
                                        BIIB_Preferred_Language__c = 'English',
                                        BIIB_Preferred_Time__c = '8PM',
                                        BIIB_Priority__c = '85',
                                        BIIB_Region__c = 'UCAN',
                                        BIIB_Status__c = 'Closed-Failed',
                                        BIIB_Work_Group__c = 'Acquisition',
                                        RecordTypeId = mapWorkRecordTypes.get('BIIB_Work_Item_1'),
                                        BIIB_Case__c = objCase.Id
                                        
                                        );
                                        
            insert  objWork3;                   
            system.debug('---objWork----'+objWork3);
            system.debug('---objWorkId----'+objWork3.Id);
            BIIB_Work__c objWorkQuery3=[SELECT Id, Name from BIIB_Work__c where Name = 'A004'];
            system.debug('---objWorkQuery----'+objWorkQuery3.Id);
            System.assertEquals(objWorkQuery3.Id, objWork3.Id);
                                    
            test.stopTest();
        
            
        
        
        
     }                                   
}