/**
*@author Divya Galani
*@date 22/04/2014
@description controller class BIIB_WIEdit_Override page
------------------------------------------------
Developer           Date            Description
------------------------------------------------
Divya Galani       22/04/2014      OriginalVersion
*/

public class BIIB_WI_Edit_Override_Class
{

    String strWorkItemId;
    private final BIIB_Work__c objWork;
    public String strInvokeEditRedirect{get;set;}
    
    /*
    * @author Divya Galani
    @description Standard Controller
    @param none
    @return Id
    */
    public BIIB_WI_Edit_Override_Class(ApexPages.StandardController controller) 
    {
        this.objWork = (BIIB_Work__c)controller.getRecord();
        strWorkItemId = objWork.Id;
        strInvokeEditRedirect = 'Yes';
        system.debug('----BIIB_WI_Edit_Override_Class_strWorkItemId----'+strWorkItemId);
    }

    /*
    * @author Divya Galani
    @description Redirect To Work Record
    @param WorkItemRowId
    @return pagereference
    */
    public PageReference editRedirectToWork()
    {
        system.debug('----InsideeditRedirectToWork----');
        strInvokeEditRedirect = 'No';
        PageReference pgRef = new PageReference('/'+strWorkItemId+'/e'+'?&nooverride=0'+'&saveURL='+'/apex/BIIB_WorkItem_Disposition');
        pgRef.setRedirect(true);
        return pgRef;

    }
   
}