/**
*@author Aditya Ghosh
*@date 28/04/2014
@test class for BIIB_Utility_Class
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Aditya       28/04/2014  OriginalVersion
*/
@isTest
public class BIIB_Utility_Class_Test{

    public static testmethod void testFunction(){
        test.startTest();
            BIIB_OperatorValidation__c newValidation = new BIIB_OperatorValidation__c();
            newValidation.Name = 'BIIB_testOperatorValidation';
            newValidation.BIIB_Operator_Value__c = '=';
            newValidation.BIIB_Field_Data_Type__c = 'PICKLIST';
            insert newValidation;
                        
            Boolean breturnValue = BIIB_Utility_Class.runOnce();
            //To Cover else condition
            breturnValue = BIIB_Utility_Class.runOnce();
            
            String objName = 'Account';
            String fieldAPIName = 'BIIB_Advocate_Status__c';
            String recordTypeLabel = 'Patient';
            String operatorValueFirst = '=';
            String operatorValueSecond = '!=';
            String ruleLogic = '((1(AND)2) OR 3)';
            Id returnRecordTypeId = BIIB_Utility_Class.getRecordTypeId(objName , recordTypeLabel );
            
            breturnValue  = BIIB_Utility_Class.validatefieldAPIName(objName , fieldAPIName);
            //To Cover else condition
            breturnValue  = BIIB_Utility_Class.validatefieldAPIName(objName , 'invalidField');
            
            breturnValue  = BIIB_Utility_Class.validateObjectAPIName(objName);
            //To Cover else condition
            breturnValue  = BIIB_Utility_Class.validateObjectAPIName('invalidObject');
            
            breturnValue  = BIIB_Utility_Class.validateFieldOperator(objName , fieldAPIName , operatorValueFirst );
            breturnValue  = BIIB_Utility_Class.validateFieldOperator(objName , fieldAPIName , operatorValueSecond );
            
            Map<Id,String> returnRecordTypeMap = new Map<Id,String>();
            returnRecordTypeMap = BIIB_Utility_Class.caseRTDevName(objName);
            
            breturnValue  = BIIB_Utility_Class.validateFirstLastChars(ruleLogic);
        test.stopTest();
    }
}