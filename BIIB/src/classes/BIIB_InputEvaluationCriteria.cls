public class BIIB_InputEvaluationCriteria 
{
    public List<BIIB_EvaluationCriteriaWrapperClass> lstCriteriaWrapper {get;set;}
    public BIIB_Evaluation_Criteria__c objEvalCriteria {get;set;}
    public Integer intSquenceToDel {get;set;}
    public String inputEvaluationCriteria {get;set;}
    public String strSelCaseRecordType {get;set;}
    String objectName = 'Case';
    
    set<String> setSingleQuotesReq = new set<String>();
    
    public String strRecordTypeName;
    String strProcessId;
    String strEvalId;
    String strObjectAPIName = 'BIIB_Object_API_Name__c';
    String objFieldCriteriaObject = 'BIIB_Field_Criteria__c';
    Decimal intSequence = 1;
    List<RecordType> lstCaseRecordType = new List<RecordType>();
    
    //Constructor
    public BIIB_InputEvaluationCriteria(ApexPages.StandardController controller)
    {
        objEvalCriteria = new BIIB_Evaluation_Criteria__c();
        strRecordTypeName = apexpages.currentpage().getparameters().get('RecordType');
        strEvalId = apexpages.currentpage().getparameters().get('Id');
        strProcessId = apexpages.currentpage().getparameters().get('processId');
        
        if(strEvalId == null || strEvalId == '')
        {
            //By default add 5 rows to the page
            lstCriteriaWrapper = new List<BIIB_EvaluationCriteriaWrapperClass>();
            lstCriteriaWrapper.add(new BIIB_EvaluationCriteriaWrapperClass(new BIIB_Field_Criteria__c(), intSequence++));
            lstCriteriaWrapper.add(new BIIB_EvaluationCriteriaWrapperClass(new BIIB_Field_Criteria__c(), intSequence++));
            lstCriteriaWrapper.add(new BIIB_EvaluationCriteriaWrapperClass(new BIIB_Field_Criteria__c(), intSequence++));
            lstCriteriaWrapper.add(new BIIB_EvaluationCriteriaWrapperClass(new BIIB_Field_Criteria__c(), intSequence++));
            lstCriteriaWrapper.add(new BIIB_EvaluationCriteriaWrapperClass(new BIIB_Field_Criteria__c(), intSequence++));
        }
        else
        {
            lstCriteriaWrapper = new List<BIIB_EvaluationCriteriaWrapperClass>();
            BIIB_Evaluation_Criteria__c objEC = [SELECT Id, BIIB_Criteria__c , BIIB_Description__c, BIIB_Business_Rules_Configuration__c 
                                                 FROM BIIB_Evaluation_Criteria__c WHERE Id=:strEvalId];
            inputEvaluationCriteria = objEC.BIIB_Criteria__c ;
            objEvalCriteria.BIIB_Description__c = objEC.BIIB_Description__c;
            
            strProcessId = objEC.BIIB_Business_Rules_Configuration__c ;
            
            for(BIIB_Field_Criteria__c objFC : [SELECT Id, BIIB_Field_API_Name__c, BIIB_Field_Value__c, BIIB_Object_API_Name__c, BIIB_Operator__c, BIIB_Sequence__c, BIIB_Evaluation_Criteria__c 
                                                FROM BIIB_Field_Criteria__c WHERE BIIB_Evaluation_Criteria__c =:objEC.Id order by BIIB_Sequence__c])
            {
                BIIB_EvaluationCriteriaWrapperClass objWC = new BIIB_EvaluationCriteriaWrapperClass(objFC, objFC.BIIB_Sequence__c);
                objWC.strSelFieldName = objFC.BIIB_Field_API_Name__c;
                lstCriteriaWrapper.add(objWC);
                intSequence = objFC.BIIB_Sequence__c + 1;
            }
        }
        
        //Fetch the picklist field value from a particuler object
        List<String> lstPickvals = new List<String>();
        Schema.SObjectType Res = Schema.getGlobalDescribe().get(objFieldCriteriaObject);
        // Describing Fields
        Map<String, Schema.SObjectField> fieldMap = Res.getDescribe().fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = fieldMap.get(strObjectAPIName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for(Schema.PicklistEntry a : pick_list_values) 
        {   
            //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        
        //Query All Case RecordType
        String strQuery = 'SELECT DeveloperName, Name FROM RecordType WHERE SobjectType =\'' + objectName +'\'';     
        lstCaseRecordType = database.query(strQuery);
    }
    
    public pagereference cancelEvaluationCriteria()
    {
        PageReference pgRef = new PageReference('/'+strProcessId);
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    public void onChangeObjectName()
    {
        //do nothing
    }
    
    //add row logic
    public void addRow()
    {
        lstCriteriaWrapper.add(new BIIB_EvaluationCriteriaWrapperClass(new BIIB_Field_Criteria__c(), intSequence++));
    }
    
    //Delete row logic
    public void delRow()
    {
        Integer i=0;
        Integer intSeqToDel;
        for(BIIB_EvaluationCriteriaWrapperClass objWrap : lstCriteriaWrapper)
        {
            if(objWrap.intSequenceNumber == intSquenceToDel)
            {
                intSeqToDel = i;
                break;
            }
            i++;
        }
        
        if(intSeqToDel != Null)
        {
            BIIB_EvaluationCriteriaWrapperClass  objWToRemove = lstCriteriaWrapper.remove(intSeqToDel);
            if(objWToRemove.objFieldCriteria.Id != null)
                delete objWToRemove.objFieldCriteria;
            intSequence = 1;
        }
        
        for(BIIB_EvaluationCriteriaWrapperClass objWrap : lstCriteriaWrapper)
        {
            objWrap.intSequenceNumber = intSequence;
            intSequence++;
        }
    }
    
    public List<SelectOption> getRecordTypes()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        for(RecordType objRT : lstCaseRecordType)
        {
            options.add(new SelectOption(objRT.DeveloperName,objRT.Name));
        }
        return options;
    }
    
    public pagereference saveEvaluationCriterias()
    {
        try
        {
            if(strProcessId !=  Null && strProcessId != '')
            {
                List<RecordType> lstRecType = new List<RecordType>();
                lstRecType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName='PRE' AND SobjectType='BIIB_Evaluation_Criteria__c'];
                system.debug('-----strProcessId------'+strProcessId);
                system.debug('-----strRecordTypeName------'+strRecordTypeName);
                system.debug('-----lstRecType.size()------'+lstRecType.size());
                system.debug('-----lstRecType------'+lstRecType);
                //if(lstRecType.size() > 0 && strRecordTypeName == 'PRE' && strProcessId != '')
                //{
                    system.debug('-----strRecordTypeName------'+strRecordTypeName);
                    List<BIIB_Field_Criteria__c> lstFilterCriteria = new List<BIIB_Field_Criteria__c>();
                    
                    
                    BIIB_Evaluation_Criteria__c insertEvaluationCriteria;
                    if(strEvalId != null)
                        insertEvaluationCriteria = new BIIB_Evaluation_Criteria__c(Id=strEvalId);
                    else
                        insertEvaluationCriteria = new BIIB_Evaluation_Criteria__c();
                    insertEvaluationCriteria.BIIB_Business_Rules_Configuration__c = strProcessId;
                    insertEvaluationCriteria.RecordTypeId = lstRecType[0].Id;
                    //insertEvaluationCriteria.BIIB_SR_Type__c = strSelCaseRecordType;
                    insertEvaluationCriteria.BIIB_Description__c = objEvalCriteria.BIIB_Description__c;
                    insertEvaluationCriteria.BIIB_Criteria__c = inputEvaluationCriteria;
                    //insertEvaluationCriteria.BIIB_API_Criteria__c = strEvalCriteriaQuery;
                    system.debug('insertEvaluationCriteria'+insertEvaluationCriteria);
                    upsert insertEvaluationCriteria;
                    
                    for(BIIB_EvaluationCriteriaWrapperClass objCW : lstCriteriaWrapper)
                    {
                        if(objCW != null && objCW.strSelFieldName != null && objCW.objFieldCriteria.BIIB_Operator__c != null && objCW.objFieldCriteria.BIIB_Object_API_Name__c != Null 
                        && objCW.intSequenceNumber!= null)
                        {
                            BIIB_Field_Criteria__c insertFieldCriteria;
                            if(objCW.objFieldCriteria.Id != null)
                            {
                                insertFieldCriteria = objCW.objFieldCriteria;
                            }
                            else
                            {
                                insertFieldCriteria = new BIIB_Field_Criteria__c();
                                insertFieldCriteria.BIIB_Evaluation_Criteria__c = insertEvaluationCriteria.Id;
                            }
                            insertFieldCriteria.BIIB_Sequence__c = objCW.intSequenceNumber;
                            insertFieldCriteria.BIIB_Operator__c = objCW.objFieldCriteria.BIIB_Operator__c;
                            insertFieldCriteria.BIIB_Object_API_Name__c = objCW.objFieldCriteria.BIIB_Object_API_Name__c; 
                            insertFieldCriteria.BIIB_Field_Value__c = objCW.objFieldCriteria.BIIB_Field_Value__c; 
                            insertFieldCriteria.BIIB_Field_API_Name__c = objCW.strSelFieldName;
                            system.debug('insertFieldCriteria'+insertFieldCriteria);
                            lstFilterCriteria.add(insertFieldCriteria);
                        }
                    }
                    
                    if(lstFilterCriteria.size() > 0)
                    {
                        upsert lstFilterCriteria;
                    }
                   
                    PageReference pgRef = new PageReference('/'+strProcessId);
                    pgRef.setRedirect(true);
                    return pgRef;
                //}
            }
            /*set<String> setSingleQuotesReq = new set<String>{'STRING','PICKLIST','PHONE','EMAIL','TEXTAREA','MULTIPICKLIST'};
            set<String> setOperator = new set<String>{'=','!=','<','>','<=','>='};
            
            //Create a map of sequence number to wrapper class.
            Map<String,BIIB_EvaluationCriteriaWrapperClass> mapSeqNoToFieldCrieria = new Map<String,BIIB_EvaluationCriteriaWrapperClass>();
            for(BIIB_EvaluationCriteriaWrapperClass objWrapCriterias : lstCriteriaWrapper)
            {
                mapSeqNoToFieldCrieria.put(String.valueOf(objWrapCriterias.intSequenceNumber), objWrapCriterias);
            }
            
            //Logic to convert the evaluation criteria in a way that it can be understood by soql later.
            String strEvalCriteriaQuery = '';
            if(inputEvaluationCriteria != Null)
            {
                List<String> lstEvalCriteria = inputEvaluationCriteria.split(' ');
                for(String strChar : lstEvalCriteria)
                {
                    strChar = strChar.trim();
                    BIIB_EvaluationCriteriaWrapperClass objWrapCriterias;
                    if(strChar != 'AND' && strChar != 'OR' && strChar != 'NOT' && strChar != '(' && strChar != ')')
                        objWrapCriterias = mapSeqNoToFieldCrieria.get(strChar);
                    if(objWrapCriterias != null && objWrapCriterias.strSelFieldName != null && objWrapCriterias.objFieldCriteria.BIIB_Operator__c != null && objWrapCriterias.objFieldCriteria.BIIB_Object_API_Name__c != Null)
                    {
                        String strFieldAPIDataType = String.ValueOf(Schema.getGlobalDescribe().get(objWrapCriterias.objFieldCriteria.BIIB_Object_API_Name__c).getDescribe().fields.getMap().get(objWrapCriterias.strSelFieldName).getDescribe().getType());
                        String strFieldAPIName = (objWrapCriterias.objFieldCriteria.BIIB_Object_API_Name__c != 'Case') ?
                                                objWrapCriterias.objFieldCriteria.BIIB_Object_API_Name__c+'.'+objWrapCriterias.strSelFieldName
                                                : objWrapCriterias.strSelFieldName;
                        
                        //The below if-else is to ensure that we are converting data into the format which can be understood by soql
                        if(setSingleQuotesReq.contains(strFieldAPIDataType))
                        {
                            objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c  = '\''+objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c+'\'';
                        }
                        else if(strFieldAPIDataType == 'DATETIME')
                        {
                            //User will input this YYYY-MM-DD HH:MM:SS format and we need to convert it into this YYYY-MM-DDThh:mm:ssZ format
                            objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c  = objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c.replace(' ','T')+'Z';
                        }
                        //End if-else to ensure that data is in the format which can be understood by soql.
                        
                        //Below if-else will decide on the operator
                        if(setOperator.contains(objWrapCriterias.objFieldCriteria.BIIB_Operator__c))
                        {
                            strEvalCriteriaQuery += strFieldAPIName+' '+objWrapCriterias.objFieldCriteria.BIIB_Operator__c+' '+objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c;
                        }
                        else if(objWrapCriterias.objFieldCriteria.BIIB_Operator__c == 'LIKE' || objWrapCriterias.objFieldCriteria.BIIB_Operator__c == 'CONTAINS')
                        {
                            strEvalCriteriaQuery += strFieldAPIName+' LIKE \'%'+objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c+'%\'';
                        }
                        else if(objWrapCriterias.objFieldCriteria.BIIB_Operator__c == 'STARTS WITH')
                        {
                            strEvalCriteriaQuery += strFieldAPIName+' LIKE '+objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c+'%\'';
                        }
                        else if(objWrapCriterias.objFieldCriteria.BIIB_Operator__c == 'ENDS WITH')
                        {
                            strEvalCriteriaQuery += strFieldAPIName+' LIKE \'%'+objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c;
                        }
                        else if(objWrapCriterias.objFieldCriteria.BIIB_Operator__c == 'DOES NOT CONTAINS')
                        {
                            strEvalCriteriaQuery += 'NOT '+strFieldAPIName+' LIKE \'%'+objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c+'%\'';
                        }
                        //End if-else to decide on operator
                    }
                    else if(strChar == 'AND' || strChar == 'OR' || strChar == 'NOT' || strChar == '(' || strChar == ')')
                    {
                        strEvalCriteriaQuery += ' '+strChar+' '; 
                    }
                }
            }
            
            system.debug('---------strEvalCriteriaQuery---------'+strEvalCriteriaQuery);
            List<Case> objCase = database.query('SELECT Id From Case WHERE '+strEvalCriteriaQuery);
            
            if(inputEvaluationCriteria != Null && strSelCaseRecordType != Null)
            {
                try
                {
                    // Create a record in  Evaluation Criteria Custom Object
                    BIIB_Evaluation_Criteria__c insertEvaluationCriteria = new BIIB_Evaluation_Criteria__c();
                    insertEvaluationCriteria.BIIB_SR_Type__c = strSelCaseRecordType;
                    insertEvaluationCriteria.BIIB_Criteria__c = inputEvaluationCriteria;
                    insertEvaluationCriteria.BIIB_API_Criteria__c = strEvalCriteriaQuery;
                    insert insertEvaluationCriteria;
                    
                    // This code will verify all the records which have been populated and save it in the evaluation criteria
                    for(BIIB_EvaluationCriteriaWrapperClass objWrapCriterias : lstCriteriaWrapper)
                    {
                       // Verify if each record has all the fields populated. If yes, insert record in FieldCriteria Custom Object
                        if(objWrapCriterias.objFieldCriteria.BIIB_Object_API_Name__c != null && objWrapCriterias.objFieldCriteria.BIIB_Operator__c != null 
                        && objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c != null && objWrapCriterias.strSelFieldName!= null && objWrapCriterias.intSequenceNumber!= null)
                        {                       
                            BIIB_Field_Criteria__c insertFieldCriteria = new BIIB_Field_Criteria__c();
                            insertFieldCriteria.BIIB_Sequence__c = objWrapCriterias.intSequenceNumber;
                            insertFieldCriteria.BIIB_Operator__c = objWrapCriterias.objFieldCriteria.BIIB_Operator__c;
                            insertFieldCriteria.BIIB_Object_API_Name__c = objWrapCriterias.objFieldCriteria.BIIB_Object_API_Name__c; 
                            insertFieldCriteria.BIIB_Field_Value__c = objWrapCriterias.objFieldCriteria.BIIB_Field_Value__c; 
                            insertFieldCriteria.BIIB_Field_API_Name__c = objWrapCriterias.strSelFieldName;
                            insertFieldCriteria.BIIB_Evaluation_Criteria__c = insertEvaluationCriteria.Id;
                            insert insertFieldCriteria;
                        }
                        else
                        {
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,' Please populate all the columns for each row ');
                            ApexPages.addMessage(myMsg);
                        }                    
                    }
                }
                catch(Exception e)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage());
                    ApexPages.addMessage(myMsg);
                }
            }
            else
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Evaluation Criteria and SR Type is required');
                ApexPages.addMessage(myMsg);
            }*/
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    //Wrapper class to bind the sequence no and API field column to the field criteria
    public class BIIB_EvaluationCriteriaWrapperClass 
    {
        public BIIB_Field_Criteria__c objFieldCriteria {get;set;}
        public String strSelFieldName {get;set;}
        public Decimal intSequenceNumber {get;set;}
        public String strFieldType {get
            {
                if(strSelFieldName != null && objFieldCriteria != null && objFieldCriteria.BIIB_Object_API_Name__c != null)
                {
                    strFieldType = String.valueOf(Schema.getGlobalDescribe().get(objFieldCriteria.BIIB_Object_API_Name__c).getDescribe().fields.getMap().get(strSelFieldName).getDescribe().getType());
                }
                return strFieldType;
            } set;}
        
        //dynamically fetch all the fields based on the object selected from the object select list
        public List<SelectOption> getFieldName()
        {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('','--None--'));
            //perform describe field to fetch all the fields related to the object select from its resoective row
            Map<String, Schema.SObjectField> mapFieldDesc = new Map<String, Schema.SObjectField>();
            if(objFieldCriteria.BIIB_Object_API_Name__c != null)
            {
                system.debug('----------------'+objFieldCriteria.BIIB_Object_API_Name__c);
                Schema.SObjectType objDesc = Schema.getGlobalDescribe().get(objFieldCriteria.BIIB_Object_API_Name__c);
                Map<String, Schema.SObjectField> objFieldMap = objDesc.getDescribe().fields.getMap();
                mapFieldDesc = objFieldMap;
            }
            //
            for(String strFieldName : mapFieldDesc.keyset())
            {
                options.add(new SelectOption(String.valueOf(mapFieldDesc.get(strFieldName).getDescribe().getName()) , String.valueOf(mapFieldDesc.get(strFieldName).getDescribe().getLabel())));
            }
            return options;
        } 
    
        public BIIB_EvaluationCriteriaWrapperClass(BIIB_Field_Criteria__c objFieldCriteria, Decimal intSeq)
        {
            this.objFieldCriteria = objFieldCriteria;
            this.intSequenceNumber =  intSeq;
        }
    }
}