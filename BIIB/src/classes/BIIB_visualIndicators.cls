Global without sharing class BIIB_visualIndicators{

    public string recType{get;set;}
    public string imagesrc{get;set;}
    public BIIB_visualIndicators(ApexPages.StandardController controller) { 
    
       string str = ApexPages.currentPage().getParameters().get('Id'); 
       
       Account docRecord =  [select recordtypeid  from Account where id =: str];
       
       recordtype rt = [select name, developername from recordtype where id =: docRecord.recordtypeid];
       
       if(rt.developername ==  'BIIB_Patient'){       
           recType = 'Patient Profile';           
       }
       else if(rt.developername ==  'BIIB_HCP')  {    
           recType = 'Health Care Professional';             
       }
       else if(rt.developername ==  'BIIB_HCO')  {    
           recType = 'Health Care Organization';             
       }
       else if(rt.developername ==  'BIIB_Infusion_Sites')  {    
           recType = 'Infusion Site';             
       }
       else if(rt.developername ==  'BIIB_Payer')  {    
           recType = 'Insurance / Payer';             
       }
       else if(rt.developername ==  'BIIB_SPP')  {    
           recType = 'Specialty Pharmacy';             
       }
    }
}