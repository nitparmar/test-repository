/**
*@author Aditya Ghosh
*@date 28/04/2014
@test class for BIIB_InfusionExtensionClass
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Aditya       28/04/2014  OriginalVersion
*/
@isTest(SeeAllData = true)
public class BIIB_InfusionExtensionClass_Test{

    public static testmethod void testFunction(){
        test.startTest();
            Case testInstanceCase = BIIB_testClassHelper.createCase();
            insert testInstanceCase;
            ApexPages.StandardController sc = new ApexPages.StandardController(testInstanceCase); 
            
            BIIB_InfusionExtensionClass testInstance = new BIIB_InfusionExtensionClass(sc); 
            Integer returnInt = testInstance.getPageSize();
            Boolean breturnValue = testInstance.getPreviousButtonEnabled();
            breturnValue  = testInstance.getNextButtonDisabled();
            //returnInt = testInstance.getTotalPageNumber();
            //returnPageRef = testInstance.nextBtnClick();  
            //PageReference returnPageRef = testInstance.previousBtnClick();
            String strReturnString = testInstance.getSortDirection();
            testInstance.setSortDirection('ASC');
            //testInstance.createAffiliation();
            List<SelectOption> returnList = new List<SelectOption>();
            returnList = testInstance.getItems();
            returnList = testInstance.getDistance();
            //testInstance.ShowResult();
            strReturnString = testInstance.getGeolocationFromZip('sampleZip');
            //testInstance.BindData(10);
        test.stopTest();
    }
}