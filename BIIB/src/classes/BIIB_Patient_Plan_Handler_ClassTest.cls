/*
 * ©Bio 2014, all rights reserved 
 * Created Date : 04/21/2014
 * Author : Pruthvi Raj
 * Description : This Class is used for testing the functionality of  BIIB_Patient_Plan_Handler_Class ( Class ) and BIIB_Patient_Plan_trigger ( Trigger ) .
 * 
 */
 
@isTest
private class BIIB_Patient_Plan_Handler_ClassTest {

    static testMethod void myUnitTest() {
        RecordType acc_rectype = [SELECT Id,Name FROM RecordType WHERE SObjectType='Account' AND DeveloperName  = 'BIIB_HCP'];
        Account acc_hcp_rec = new Account(FirstName = 'Test' , LastName = 'Name' , RecordTypeId = acc_rectype.id , PersonDoNotCall = true);
        insert acc_hcp_rec ;
        
        Test.startTest();
         BIIB_Patient_Plan__c obj = new BIIB_Patient_Plan__c ( BIIB_Patient_Lookup__c = acc_hcp_rec.id , BIIB_Primary__c = True , BIIB_Effective_Date__c = System.Today()-5, BIIB_End_Date__c = System.Today()+11);
         insert obj;   
         BIIB_Patient_Plan__c obj1 = new BIIB_Patient_Plan__c ( BIIB_Patient_Lookup__c = acc_hcp_rec.id , BIIB_Primary__c = True , BIIB_Effective_Date__c = System.Today()-10, BIIB_End_Date__c = System.Today()+1);
         insert obj1;
         obj1.BIIB_Effective_Date__c = date.newInstance(2014, 4, 19);
         update obj1;
         BIIB_Patient_Plan__c obj2 = new BIIB_Patient_Plan__c ( BIIB_Patient_Lookup__c = acc_hcp_rec.id , BIIB_Primary__c = True , BIIB_Effective_Date__c = System.Today(), BIIB_End_Date__c = System.Today()+5);
         insert obj2;
         //BIIB_Patient_Plan_Handler_Class.OnBeforeUpdate(obj, obj);
         Test.stopTest();
        }
}