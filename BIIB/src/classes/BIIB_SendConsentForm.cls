public with sharing class BIIB_SendConsentForm 
{
    public String strSelectedConsent {get;set;}
    Account objAccount = new Account();
    List<String> lstSelectedConsent = new List<String>();
    Map<String,String> mapConsentNameToValue = new Map<String,String>();
    
    public BIIB_SendConsentForm(ApexPages.StandardController controller)
    {
        objAccount = (Account) controller.getRecord();
       /* if(objAccount.BIIB_Information_Willing__c != null)
            lstSelectedConsent = objAccount.BIIB_Information_Willing__c.split(';');
       */
        for(BIIB_Send_Consent__c objSC : BIIB_Send_Consent__c.getall().values())
        {
            mapConsentNameToValue.put(objSC.Name,objSC.Consent_value__c);
        }
    }
    
    public List<SelectOption> getConsentName() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        for(String strConsent : lstSelectedConsent)
        {
            options.add(new SelectOption(mapConsentNameToValue.get(strConsent),mapConsentNameToValue.get(strConsent)));
        }
        
        return options;
    }
}