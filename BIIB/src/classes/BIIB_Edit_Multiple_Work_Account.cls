/**
*@author Divya Galani
*@date 09/05/2014
@description controller class for edit multiple work items.Invoked From VF Page: BIIB_Edit_Multiple_Work_Account
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Divya       14/04/2014  OriginalVersion
*/
public class BIIB_Edit_Multiple_Work_Account 
{
    public List<BIIB_Work__c> lstWork {get;set;}
    private Account objAccount = new Account();
    public List<BIIB_Work__c> lstWorkOld {get;set;}
    public Map<Id,BIIB_Work__c> mapWorkDetails = new Map<Id,BIIB_Work__c>();
    public String strWorkOpenStatus;
    public BIIB_Work__c objWorkInput {get;set;}
    public String strWorkType {get;set;}
    public String strWorkSubType {get;set;}
  
    public BIIB_Edit_Multiple_Work_Account(ApexPages.StandardController controller) 
    {
        lstWorkOld = new List<BIIB_Work__c>();
        system.debug('-----InsideClassBIIB_Edit_Multiple_Work_Account----');
        objAccount = (Account)controller.getRecord();
        system.debug('-----objAccountIs----'+objAccount.Id);
        
        strWorkOpenStatus = System.Label.BIIB_Work_New_Status;

        if(objAccount.Id != null)
        {

            lstWork  = [SELECT Name, BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Account__c=:objAccount.Id AND BIIB_Status__c != :System.Label.BIIB_Work_Completed_Status];
            lstWorkOld = [SELECT Name, BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Account__c=:objAccount.Id AND BIIB_Status__c != :System.Label.BIIB_Work_Completed_Status];
            for(BIIB_Work__c wrk : lstWorkOld)
            {
                
                if(!mapWorkDetails.containsKey(wrk.Id))
                {
                    mapWorkDetails.put(wrk.Id,wrk);
                }
                else
                {
                    // Do nothing
                }
            }
            system.debug('-----mapWorkDetails----'+mapWorkDetails);
        }
    }
    
    
     /*
    *@author Divya Galani
    @description This method is used to display the Work Type Picklist Values in VF Page
    @param null
    @return List<SelectOption>
    */

    
    public List<SelectOption> getWorkType() 
    {
     
     Schema.DescribeFieldResult workType = BIIB_Work__c.BIIB_Work_Type__c.getDescribe();
     List<Schema.PicklistEntry> workTypeValues = workType.getPicklistValues();
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('---None--', '---None---'));
     for(Schema.PicklistEntry ple : workTypeValues)
     {
            options.add(new SelectOption(ple.getValue(), ple.getLabel()));
     }
     
     return options;
    }
    
     /*
    *@author Divya Galani
    @description This method is used to display the Work Sub Type Picklist Values in VF Page
    @param null
    @return List<SelectOption>
    */
    public List<SelectOption> getWorkSubType() 
    {
     
     Schema.DescribeFieldResult workSubType = BIIB_Work__c.BIIB_Work_Sub_Type__c.getDescribe();
     List<Schema.PicklistEntry> workSubTypeValues = workSubType.getPicklistValues();
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('---None--', '---None---'));
     for(Schema.PicklistEntry ple : workSubTypeValues )
     {
            options.add(new SelectOption(ple.getValue(), ple.getLabel()));
     }
     
     return options;
    }
    
    /*
    *@author Divya Galani
    @description This method is used to filter the records in VF Page based on Work Type/Work Sub Type
    @param null
    @return null
    */
    
    public void FilteredWork()
    {

        // Filter on WorkType only
        if(strWorkType != '' && (strWorkSubType == '' || strWorkSubType.contains('None'))&& !strWorkType.contains('None'))
        {
            lstWork  = [SELECT Name, BIIB_Work_Type__c,BIIB_Work_Sub_Type__c,BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Account__c=:objAccount.Id AND BIIB_Status__c =: strWorkOpenStatus AND BIIB_Work_Type__c =:strWorkType];
        }
        
        // Filter on WorkType and SubType
        if(strWorkType != '' && strWorkSubType != '' && !strWorkType.contains('None') && !strWorkSubType.contains('None'))
        {
            lstWork  = [SELECT Name, BIIB_Work_Type__c,BIIB_Work_Sub_Type__c,BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Account__c=:objAccount.Id AND BIIB_Status__c =: strWorkOpenStatus AND BIIB_Work_Type__c =:strWorkType AND BIIB_Work_Sub_Type__c =:strWorkSubType];
        }
        // Filter on SubType Only
        if((strWorkType == '' || strWorkType.contains('None')) && strWorkSubType != '' && !strWorkSubType.contains('None'))
        {
            lstWork  = [SELECT Name, BIIB_Work_Type__c,BIIB_Work_Sub_Type__c,BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Account__c=:objAccount.Id AND BIIB_Status__c =: strWorkOpenStatus AND BIIB_Work_Sub_Type__c =:strWorkSubType];
        }
        
        // No Filter
        if((strWorkType.contains('None')) && strWorkSubType.contains('None'))
        {
            lstWork  = [SELECT Name, BIIB_Work_Type__c,BIIB_Work_Sub_Type__c,BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Account__c=:objAccount.Id AND BIIB_Status__c =: strWorkOpenStatus];
        }

    }
    
    /*
    *@author Divya Galani
    @description This methods updates all the work items modified by the user in the VF Page
    @param null
    @return pagereference
    */
    
    public pagereference Save()
    {
        try
        {
            Map<Id,BIIB_Work__c> mapUpdatedWorkRecords = new Map<Id,BIIB_Work__c>();

            
            for(BIIB_Work__c objUpdatedWork : lstWork)
            {
                
                SObject objGenericWork = objUpdatedWork;
                Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('BIIB_Work__c').getDescribe().getSObjectType();
                SObject objWorkUpdate = objectDef.newSObject();
                
                if(mapWorkDetails.containsKey(objUpdatedWork.Id))
                {
                    BIIB_Work__c objWork = mapWorkDetails.get(objUpdatedWork.Id);
                    system.debug('----objWork---'+objWork);
                    SObject objOldWork = objWork;
                    
                    
                    
                    for(BIIB_Work_Update_Multiple__c lstWorkFieldCustomSetting : [SELECT Name, BIIB_Work_Field_Name__c FROM BIIB_Work_Update_Multiple__c])
                    {
                        
                        if(objGenericWork.get(lstWorkFieldCustomSetting.BIIB_Work_Field_Name__c) == objOldWork.get(lstWorkFieldCustomSetting.BIIB_Work_Field_Name__c))
                        {
                            // Value has not changed, so  do nothing
                            system.debug('....FieldValue not changed.....');                        
                        }
                        else
                        {

                            if(!mapUpdatedWorkRecords.containsKey(objUpdatedWork.Id))
                            {
                                mapUpdatedWorkRecords.put(objUpdatedWork.Id,objUpdatedWork);
                            }
                        }
                    }
                      
                }
            }// End for(BIIB_Work__c objUpdatedWork : lstWork)
            
            for(String workKey: mapUpdatedWorkRecords.keySet())
            {

                BIIB_Work__c objUpdate = mapUpdatedWorkRecords.get(workKey);
                system.debug('....objUpdate.....'+objUpdate);
                update objUpdate;

            }
            
            
            PageReference pgRef = new PageReference('/'+objAccount.Id);
            pgRef.setRedirect(true);
            return pgRef;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            String strMyMesg = e.getMessage();
    
            if(strMyMesg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') )
            {
              ApexPages.Message strValidationMesg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BIIB_Multiple_Work_Edit_Error);
              ApexPages.addMessage(strValidationMesg);
                 
            }
            
            else
            {

                ApexPages.addMessage(myMsg);
            }
        }
        return null;
    }

}