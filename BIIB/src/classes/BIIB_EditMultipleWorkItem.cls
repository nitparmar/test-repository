/**
*@author Kamal Mehta
*@date 14/04/2014
@description controller class for edit multiple work items.
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Kamal       14/04/2014  OriginalVersion
Divya       13/05/2014  Chnaged Logic so that Last Updated Date is not changed for all records
*/
public class BIIB_EditMultipleWorkItem
{

    Public List<BIIB_Work__c> lstWork {get;set;}
    private Case objCase = new Case();
    public List<BIIB_Work__c> lstWorkOld {get;set;}
    public Map<Id,BIIB_Work__c> mapWorkDetails = new Map<Id,BIIB_Work__c>();
    public String strWorkOpenStatus;
    public BIIB_Work__c objWorkInput {get;set;}
    public String strWorkType {get;set;}
    public String strWorkSubType {get;set;}
    
    public BIIB_EditMultipleWorkItem(ApexPages.StandardController ctrl)
    {
        objCase  = (Case)ctrl.getRecord();
        strWorkOpenStatus = System.Label.BIIB_Work_New_Status;
        if(objCase.Id != null)
        {
            lstWork  = [SELECT Name, BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Case__c=:objCase.Id and BIIB_Status__c != :System.Label.BIIB_Work_Completed_Status];
            lstWorkOld = [SELECT Name, BIIB_Assigned_to__c, BIIB_Status__c, BIIB_Preferred_Language__c,BIIB_Result__c,BIIB_Reason_Code__c,BIIB_Preferred_Time__c,BIIB_Priority__c,BIIB_Route_Date__c,Description__c FROM BIIB_Work__c WHERE BIIB_Case__c=:objCase.Id and BIIB_Status__c != :System.Label.BIIB_Work_Completed_Status];
            
            for(BIIB_Work__c wrk : lstWorkOld)
            {
                
                if(!mapWorkDetails.containsKey(wrk.Id))
                {
                    mapWorkDetails.put(wrk.Id,wrk);
                }
                else
                {
                    // Do nothing
                }
            }
            system.debug('-----mapWorkDetails----'+mapWorkDetails);
        }
        
    }
     /*
    * @author Kamal Mehta
    @description This methods updates all the work items at one go.
    @param null
    @return pagereference
    */
    
    public pagereference Save()
    {
        try
        {
            Map<Id,BIIB_Work__c> mapUpdatedWorkRecords = new Map<Id,BIIB_Work__c>();

            
            for(BIIB_Work__c objUpdatedWork : lstWork)
            {
                
                SObject objGenericWork = objUpdatedWork;
                Schema.sObjectType objectDef = Schema.getGlobalDescribe().get('BIIB_Work__c').getDescribe().getSObjectType();
                SObject objWorkUpdate = objectDef.newSObject();
                
                if(mapWorkDetails.containsKey(objUpdatedWork.Id))
                {
                    BIIB_Work__c objWork = mapWorkDetails.get(objUpdatedWork.Id);
                    system.debug('----objWork---'+objWork);
                    SObject objOldWork = objWork;
                    
                    
                    
                    for(BIIB_Work_Update_Multiple__c lstWorkFieldCustomSetting : [SELECT Name, BIIB_Work_Field_Name__c FROM BIIB_Work_Update_Multiple__c])
                    {
                        
                        if(objGenericWork.get(lstWorkFieldCustomSetting.BIIB_Work_Field_Name__c) == objOldWork.get(lstWorkFieldCustomSetting.BIIB_Work_Field_Name__c))
                        {
                            // Value has not changed, so  do nothing
                            system.debug('....FieldValue not changed.....');                        
                        }
                        else
                        {

                            if(!mapUpdatedWorkRecords.containsKey(objUpdatedWork.Id))
                            {
                                mapUpdatedWorkRecords.put(objUpdatedWork.Id,objUpdatedWork);
                            }
                        }
                    }
                      
                }
            }// End for(BIIB_Work__c objUpdatedWork : lstWork)
            
            for(String workKey: mapUpdatedWorkRecords.keySet())
            {

                BIIB_Work__c objUpdate = mapUpdatedWorkRecords.get(workKey);
                system.debug('....objUpdate.....'+objUpdate);
                update objUpdate;

            }
            PageReference pgRef = new PageReference('/'+objCase.Id);
            pgRef.setRedirect(true);
            return pgRef;
        }
        catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            String strMyMesg = e.getMessage();
    
            if(strMyMesg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') )
            {
              ApexPages.Message strValidationMesg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BIIB_Multiple_Work_Edit_Error);
              ApexPages.addMessage(strValidationMesg);
                 
            }
            
            else
            {

                ApexPages.addMessage(myMsg);
            }
        }
        return null;
    }
    
}