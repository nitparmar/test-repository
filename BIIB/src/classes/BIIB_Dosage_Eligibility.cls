public class BIIB_Dosage_Eligibility {

    public String selectedProgram {get;set;}
    public BIIB_Patient_Therapy__c ptTherapy {get;set;}
    public Map<BIIB_Questionnaire__c, List<wQuestionnaire>> questionnaireMap {get;set;}
    public list<wQuestionnaire> allResponses {get;set;}
    public List<BIIB_Questionnaire__c> qList{get; set;}
    public Id patientTherapyId {get;set;}
    
    public BIIB_Dosage_Eligibility(ApexPages.StandardController controller){
        
        ptTherapy = [Select Id, BIIB_Patient__c, BIIB_Therapy__c, BIIB_Eligible_Dosage__c from BIIB_Patient_Therapy__c WHERE id =: ApexPages.currentPage().getParameters().get('id')];
         
        allResponses = new list<wQuestionnaire>();
        String[] programs = new String[]{'Quick Start','Sure Start'};
        this.programOptions = new SelectOption[]{};
        patientTherapyId = controller.getId();
        for (String c: programs ) {
            this.programOptions.add(new SelectOption(c,c));
        }
        
        //String str = ApexPages.currentPage().getParameters().get('pgm');
        String str = 'Sure Start';
        questionnaireMap = new map<BIIB_Questionnaire__c, List<wQuestionnaire>>();
        qlist = new list<BIIB_Questionnaire__c>();
        System.debug('hello'+str);
        if(str != NULL){
            for(BIIB_Questionnaire__c q:[Select ID, BIIB_Question__c, BIIB_Program__c,BIIB_Serial_Number__c,(Select ID, BIIB_Response__c, BIIB_Question__c, BIIB_Question_Number__c, BIIB_Serial_Number__c from BIIB_Responses__r Order by BIIB_Serial_Number__c asc) from BIIB_Questionnaire__c where BIIB_Program__c =:str Order By BIIB_Serial_Number__c asc]){
                List<BIIB_Response__c> ansLst =  q.BIIB_Responses__r ;
                list<wQuestionnaire> qAnLst = new list<wQuestionnaire>();
                for(BIIB_Response__c an : ansLst) {
                 wQuestionnaire qAn = new wQuestionnaire();
                 qAn.listAns = an ;
                 qAn.selected = false ;
                 qAnLst.add(qAn);
                 allResponses.add(qAn);
             }
             questionnaireMap.put(q,qAnLst ); 
             qList.add(q); 
            }
            
        }
    }
    
    public SelectOption[] programOptions { //this is where we're going to pull the list
        public get;
        private set;
    }
    
    public PageReference next() {
    
      system.debug('Surely this one'+this.selectedProgram );
      PageReference pageRef = Page.BIIB_Questionnaire;      
      pageRef.getParameters().put('pgm',selectedProgram);
      pageRef.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
      return PageRef.setRedirect(True);  
    }
    
    public PageReference cancel() {
      PageReference ptPage = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
      ptPage.setRedirect(true);
      return ptPage; 
    }
   
      public PageReference processSelected() {
      
        List<BIIB_Response__c> selectedResponses = new List<BIIB_Response__c>();
        Map<String, Integer> responseMatrix = new Map<String, Integer>();
        
        for(wQuestionnaire selectedValue: allResponses ) {
            if(selectedValue.selected == true) {
                selectedResponses.add(selectedValue.listAns);
            }
        }

        for(BIIB_Response__c res: selectedResponses) {
            system.debug(res);
            if(res.BIIB_Question_Number__c == 1 && res.BIIB_Serial_Number__c == 1){
                responseMatrix.put('Question1', 1);
            }else if(res.BIIB_Question_Number__c == 2 && res.BIIB_Serial_Number__c == 2){
                responseMatrix.put('Question2', 2);
            }else if(res.BIIB_Question_Number__c == 3 && res.BIIB_Serial_Number__c == 2){
                responseMatrix.put('Question3', 2);
            }else if(res.BIIB_Question_Number__c == 4 && res.BIIB_Serial_Number__c == 4){
                responseMatrix.put('Question4', 4);
            }else if(res.BIIB_Question_Number__c == 4 && (res.BIIB_Serial_Number__c == 2 || res.BIIB_Serial_Number__c == 3 )){
                responseMatrix.put('Question4', 23);
            }else if(res.BIIB_Question_Number__c == 4 && (res.BIIB_Serial_Number__c == 1 || res.BIIB_Serial_Number__c == 2 || res.BIIB_Serial_Number__c == 3 )){
                responseMatrix.put('Question4', 123);
            }else if(res.BIIB_Question_Number__c == 5 && res.BIIB_Serial_Number__c == 2){
                responseMatrix.put('Question5', 2);
            }else if(res.BIIB_Question_Number__c == 5 && res.BIIB_Serial_Number__c == 1){
                responseMatrix.put('Question5', 1);
            }   
        }
        
        if(responseMatrix.get('Question3') == 1){
            ptTherapy.Consent_on_File__c  = True;  
        }
        if(responseMatrix.get('Question5') == 1){
            ptTherapy.BIIB_Titration__c  = True;  
        }
        if(responseMatrix.get('Question1') == 1 && responseMatrix.get('Question2') == 2 && responseMatrix.get('Question3') == 2 && responseMatrix.get('Question4') == 4 ){
            ptTherapy.BIIB_Eligible_Dosage__c  = 0;
            update ptTherapy;
        }else if(responseMatrix.get('Question4') == 23 && responseMatrix.get('Question5') == 2 ){
            ptTherapy.BIIB_Eligible_Dosage__c  = 2;
            update ptTherapy;            
            //Insert SR
            Case closedSR = new Case(OwnerId=UserInfo.getUserId(),AccountId=ptTherapy.BIIB_Patient__c,Status='Closed',BIIB_SR_Type__c='No Charge Order',BIIB_SR_Sub_Type__c='Sure Start',RecordTypeId='012110000000JIL',BIIB_Therapy__c = ptTherapy.BIIB_Therapy__c);
            insert closedSR;
        }else if(responseMatrix.get('Question4') == 123 && responseMatrix.get('Question5') == 1 ){
            ptTherapy.BIIB_Eligible_Dosage__c  = 4;
            update ptTherapy;
            //Insert SR
            Case closedSR = new Case(OwnerId=UserInfo.getUserId(),AccountId=ptTherapy.BIIB_Patient__c,Status='Closed',BIIB_SR_Type__c='No Charge Order',BIIB_SR_Sub_Type__c='Sure Start',RecordTypeId='012110000000JIL',BIIB_Therapy__c = ptTherapy.BIIB_Therapy__c);
            insert closedSR;
        }
        
        
        
        PageReference ptPage = new PageReference('/' + ptTherapy.id);
        ptPage.setRedirect(true);
        return ptPage;
    }
    
   public class wQuestionnaire {
        
        public BIIB_Response__c listAns {get; set;}
        public Boolean selected {get; set;}

        //This is the contructor method. When we create a new wQuestionnaire object we pass a Contact that is set to the listAns property. We also set the selected value to false
        public wQuestionnaire (){}
        public wQuestionnaire (BIIB_Response__c an) {
            
            listAns = an;
            selected = false;
        }
    }

}