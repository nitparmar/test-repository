/**
*@author Rancy Fernandes
*@date 08/04/2014
@description SPP Payer Matrix class
    1. User story: US-230. To allow Agents to choose the SPP for the patient based on the Patient Plan and Preferred SPP by Product
------------------------------------------------
Developer   Date        Description
------------------------------------------------
Rancy      08/05/2014  OriginalVersion
*/

public class BIIB_SPP_PayerMatrix {

    public String strHealthCareAccount {get;set;}
    public Boolean isValidPatient {get;set;}
    private BIIB_work__c Owork;
    String patientId = '';
    public String strPatientPrdId {get;set;}
    public String strPlanName {get;set;}
    public String strPayerName {get;set;}
    public String strIsPbm {get;set;} 
    public Set<Id> setAffSPPId = new Set<Id>(); 
    Set<ID> stPlanId = new Set<Id>();
    public Account acc {get;set;}

    List<BIIB_Affiliation__c> lstAffiliated = new List<BIIB_Affiliation__c>();
    public case c = new case();
    private List<BIIB_PreferredSPPPlan__c> lstPlan = new List<BIIB_PreferredSPPPlan__c>();

    /*
    * @author Rancy Fernandes
    @description This is constructor of the class to intialize variables on load
    @param StandardController
    */
    public BIIB_SPP_PayerMatrix(ApexPages.StandardController controller) { 
        Owork =(BIIB_work__c)controller.getRecord();
        getDisplay();   
    }
   
    /*
    * @author Rancy Fernandes
    @description This method sets the variables for the different fields to be displayed on the visualforce page 
    @param none
    @return void
    */
    public void getDisplay()
    {
        BIIB_work__c w = [Select Id, Name,BIIB_Case__c from BIIB_work__c where Id= :Owork.Id];
        if (w!=null && w.BIIB_Case__c!= null ){
            c = [Select Id, AccountId,Account.RecordTypeId from Case where Id=:w.BIIB_Case__c];
        }
        if(c.Account != null) 
        patientId = c.AccountId;
        if(patientId != NULL && patientId !=''){
            acc = [Select Id, Name,BIIB_Product__c,BIIB_CMID__c,FirstName, LastName, BIIB_SSN__c, PersonBirthdate, BIIB_TOUCH_ID__c from Account where Id = :patientId];
            
            List<BIIB_Patient_Therapy__c> listPTherapy = new List<BIIB_Patient_Therapy__c>(); 
            listPTherapy = [Select Id, Name , BIIB_Therapy__c,BIIB_Therapy__r.Name ,BIIB_Patient__c from BIIB_Patient_Therapy__c where BIIB_Patient__c =: patientId and BIIB_Active__c = True];
            if(!listPTherapy.isEmpty()){
                List<BIIB_Patient_Plan__c> listPatientPlan = [Select Id, Name, BIIB_Plan__r.BIIB_Payer__r.Name, BIIB_Plan__r.BIIB_Is_PBM__c, BIIB_Patient_lookup__c,BIIB_Plan__r.Name from BIIB_Patient_Plan__c where BIIB_Patient_lookup__c = :patientId and BIIB_Active__c = true];
                if(listPatientPlan != null && !listPatientPlan.isEmpty()){
                    if(listPatientPlan[0].BIIB_Plan__c !=null){
                        strPlanName=listPatientPlan[0].BIIB_Plan__r.Name;
                        if(listPatientPlan[0].BIIB_Plan__r.BIIB_Payer__c !=null)
                        strPayerName=listPatientPlan[0].BIIB_Plan__r.BIIB_Payer__r.Name;
                        if(listPatientPlan[0].BIIB_Plan__r.BIIB_Is_PBM__c){
                            strIsPbm = System.Label.BIIB_SPP_PBM_True;
                        }else{
                            strIsPbm = System.Label.BIIB_SPP_PBM_False;
                        }
                    }
                    for(BIIB_patient_plan__c opp: listPatientPlan)
                    {
                        if(opp.BIIB_plan__c != null)
                        stPlanId.add(opp.BIIB_plan__c);
                    }
                    system.debug('strPatientPrdId--' + strPatientPrdId);
                    Map<String,SObject> recordTypeMap = BIIB_Utility_Class.getMapOfRecordTypeId(System.label.BIIB_Account_API_Name);
                    if(recordTypeMap != null){  
                        if(recordTypeMap.containsKey(System.label.BIIB_Patient_DeveloperName)){  
                            SObject rec = recordTypeMap.get(System.label.BIIB_Patient_DeveloperName);     
                        if(c.Account.RecordTypeId == rec.id )
                            {
                                isValidPatient = true;      
                            }
                        }
                    }
                    strPatientPrdId = acc.BIIB_Product__c;
                }else{
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.BIIB_No_Active_Patient_Plan);
                    ApexPages.addMessage(myMsg);
                }
            
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BIIB_No_Active_Therapy);
                ApexPages.addMessage(myMsg);
            }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BIIB_No_Patient_to_SR);
            ApexPages.addMessage(myMsg);
        } 
        
        lstAffiliated = [SELECT Id, BIIB_End_Date__c,BIIB_To_Account__c FROM BIIB_Affiliation__c WHERE BIIB_From_Account__c =:patientId and BIIB_Active__c = true and BIIB_Affiliation_Type__c= :System.label.BIIB_Affl_Patient_SPP];
        system.debug('**'+lstAffiliated);
        if(lstAffiliated.size() > 0){
            strHealthCareAccount = lstAffiliated[0].BIIB_To_Account__c;
            for(BIIB_Affiliation__c af: lstAffiliated){
                setAffSPPId.add(af.BIIB_To_Account__c);
            }
        }
        system.debug('*'+stPlanId); 
        system.debug('*'+lstPlan);
        
    }
    
    /*
    * @author Rancy Fernandes
    @description This method is used when the Product is not REMS and creates an affiliation in the BIIB_Association__c entity 
    @param none
    @return void
    */
    
    public void Save()
    {
        if(strHealthCareAccount != Null)
        {   
            if(!setAffSPPId.contains(strHealthCareAccount)){
                if(lstAffiliated.size() > 0)
                {   
                    Integer i=0;
                    for(BIIB_Affiliation__c af : lstAffiliated){
                        lstAffiliated[i].BIIB_End_Date__c = system.today()-1;
                        i++;
                    }
                    try{
                        update lstAffiliated;
                    }catch(Exception e){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                        ApexPages.addMessage(myMsg);
                    }
                }
           
                //create a new record with patient id and healthcare id and effective date as today.
           
                BIIB_Affiliation__c objAff=new BIIB_Affiliation__c(BIIB_From_Account__c =patientId,BIIB_Effective_Date__c=system.today(),BIIB_To_Account__c=strHealthCareAccount);
                try{
                    insert objAff;
                }catch(Exception e){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                    ApexPages.addMessage(myMsg);
                }
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,System.label.BIIB_Association_Creation_Success);
                ApexPages.addMessage(myMsg);
                getDisplay();
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.BIIB_Aff_Selected_SPP);
                ApexPages.addMessage(myMsg);
            }
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.label.BIIB_Affl_Creation_SPP_Error);
            ApexPages.addMessage(myMsg);
        }
    }
    
    /*
    * @author Rancy Fernandes
    @description This method sets the boolean variable to true for REMS product else set it to false  
    @param none
    @return boolean
    */
    public boolean getrems(){
        Product_vod__c p = new Product_vod__c();
        if(strPatientPrdId != null)
            p = [Select Id, Name , BIIB_Is_REMS__c from Product_vod__c where Id =:strPatientPrdId limit 1];
        if(p != null){
            Boolean boolIsRems = p.BIIB_Is_REMS__c;
            system.debug('boolIsRems--' + boolIsRems);
            return boolIsRems;
        }else{
            return false;
        }
    }
    /*
    * @author Rancy Fernandes
    @description This method returns the active products associated to the patient 
    @param None
    @return List<SelectOption>
    */
    public List<SelectOption> getTherapy() {
        List<BIIB_Patient_Therapy__c> listPatientTherapy = new List<BIIB_Patient_Therapy__c>(); 

        if(patientId != NULL && patientId !=''){
            listPatientTherapy = [Select Id, Name , BIIB_Therapy__c,BIIB_Therapy__r.Name ,BIIB_Patient__c from BIIB_Patient_Therapy__c where BIIB_Patient__c =: patientId and BIIB_Active__c = True];
        }
        
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('',System.label.BIIB_None_option));  
        if(!listPatientTherapy.isEmpty()){
            for(BIIB_Patient_Therapy__c pt : listPatientTherapy){
                if(pt.BIIB_Therapy__c != null)
                options.add(new SelectOption(pt.BIIB_Therapy__c,String.valueOf(pt.BIIB_Therapy__r.Name)));
            }
            system.debug('optionsTherapy -- ' + options);
        }       
        return options;
    }
    /*
    * @author Rancy Fernandes
    @description This method returns the SPP in the order of Preferred SPP1, SPP2 and other SPPS 
    @param None
    @return List<SelectOption>
    */
    public List<SelectOption> getItems() {
        lstPlan = [SELECT Id, name, BIIB_Plan__c, BIIB_spp1__c,BIIB_spp1__r.Name,BIIB_spp2__c,BIIB_spp2__r.Name,BIIB_Product__c FROM BIIB_PreferredSPPPlan__c WHERE BIIB_Plan__c IN : stPlanId and BIIB_Product__c = :strPatientPrdId order by BIIB_spp1__r.Name, BIIB_spp2__r.Name];
 
        Set<Id> SPPId = new Set<Id>();
        List<SelectOption> options = new List<SelectOption>(); 
        for(BIIB_PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPID.contains(objPlan.BIIB_spp1__c) && objPlan.BIIB_spp1__c != null ){
                options.add(new SelectOption(String.valueOf(objPlan.BIIB_spp1__c),String.valueOf(objPlan.BIIB_spp1__r.Name) + System.label.BIIB_SPP1));
                SPPId.add(objPlan.BIIB_spp1__c);
            }
        } 
        for(BIIB_PreferredSPPPlan__c objPlan : lstPlan){
            if(!SPPId.contains(objPlan.BIIB_spp2__c) && objPlan.BIIB_spp2__c != null){
               options.add(new SelectOption(String.valueOf(objPlan.BIIB_spp2__c),String.valueOf(objPlan.BIIB_spp2__r.Name) + System.label.BIIB_SPP2));
               SPPId.add(objPlan.BIIB_spp2__c);
            }
        } 
        for (Account a : [Select Id, Name from Account where Recordtype.DeveloperName = :System.label.BIIB_SPP_DeveloperName and Id NOT in :SPPId]){
            options.add(new SelectOption(a.Id, a.Name));
            SPPId.add(a.Id);
        }        
        system.debug('options -- ' + options);
        return options;
    }     
}